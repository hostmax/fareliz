﻿using log4net;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Service.LiveUpdate
{
    /// <summary>
    /// This class provides GUI for LiveUpdate (used in the main app)
    /// The next version content must also contain the Updater
    /// Workflow: Download package to local folder -> Start updater inside that local folder (Kill process and overwrite the file inside the application folder)
    /// </summary>
    public partial class LiveUpdateClientForm : SmartForm
    {
        private readonly UpdateRequest _request;
        private readonly LiveUpdateClient _client;

        public LiveUpdateClientForm(UpdateRequest request, LiveUpdateClient client, Image logo)
        {
            InitializeComponent();
            _request = request;
            _client = client;
            lblProduct.Text = lblProduct.Text.Replace("%PRODUCT_NAME%", request.ProductName);
            txtCurrentVersion.Text = request.FromVersion.VersionNumber.ToString();
            txtNewVersion.Text = request.ToVersion.VersionNumber.ToString();
            imgLogo.Image = logo;
        }

        /// <summary>
        /// On event: User click on "Install Updates" button
        /// </summary>
        private void btnInstallUpdates_Click(object sender, EventArgs e)
        {
            btnInstallUpdates.Enabled = false;
            _client.RunUpdate(_request);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void LiveUpdateClientForm_Shown(object sender, EventArgs e)
        {
            NativeMethods.ShowToFront(Handle);
        }
    }
}
