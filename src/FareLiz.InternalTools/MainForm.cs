﻿using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace FareLiz.InternalTools
{
    public partial class MainForm : SmartForm
    {
        private bool _isChanging = false;
        private DataGrep _activeGrep = null;

        public MainForm()
        {
            InitializeComponent();
            InitializeGreps();
        }

        private void InitializeGreps()
        {
            var curLoc = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var files = Directory.GetFiles(curLoc, "*.dll");
            var greps = new List<FieldInfo>();
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.NonPublic;

            foreach (var fi in files)
            {
                try
                {
                    var asm = Assembly.LoadFile(fi);
                    var types = asm.GetTypes();
                    foreach (var t in types)
                    {
                        var fields = t.GetFieldsRecursively(flags);
                        foreach (var f in fields)
                        {
                            if (f.FieldType == typeof(DataGrep))
                            {
                                var defConstructor = t.GetConstructor(Type.EmptyTypes);
                                if (defConstructor != null)
                                    greps.Add(f);
                            }
                        }
                    }
                }
                catch { }
            }

            cbGrep.DisplayMember = "DeclaringType";
            cbGrep.DataSource = greps;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            var txt = (TextBox)sender;
            if (e.Control && e.KeyCode == Keys.A)
                txt.SelectAll();
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            if (_isChanging)
                return;

            _isChanging = true;
            GenerateData(sender);
            _isChanging = false;
        }

        private void cbGrep_SelectedIndexChanged(object sender, EventArgs e)
        {
            var f = cbGrep.SelectedItem as FieldInfo;
            if (f != null)
            {
                var type = f.DeclaringType;
                if (_activeGrep == null || _activeGrep.GetType() != type)
                {
                    var decInstance = Activator.CreateInstance(type);
                    _activeGrep = f.GetValue(decInstance) as DataGrep;
                }

                GenerateData(sender);
            }
        }

        private void GenerateData(object sender)
        {
            if (sender == txtRaw)
                txtEncoded.Text = GenerateBytesString(txtRaw.Text);
            else if (sender == txtEncoded)
            {
                if (String.IsNullOrEmpty(txtEncoded.Text))
                    txtRaw.Text = null;
                else
                {
                    try
                    {
                        var arr = txtEncoded.Text.Split(',');
                        var bytesArr = new List<byte>();
                        foreach (var item in arr)
                        {
                            var trimmedItem = item.Trim();
                            if (trimmedItem.Length > 2)
                            {
                                var bytePart = trimmedItem.Substring(2);
                                bytesArr.Add(Convert.ToByte(bytePart, 16));
                            }
                        }

                        txtRaw.Text = _activeGrep.Convert(bytesArr.ToArray());
                    }
                    catch { txtRaw.Text = "<Invalid Hex>"; }
                }
            }
        }

        private string GenerateBytesString(string input)
        {
            var bytes = _activeGrep.Convert(input);

            var sb = new StringBuilder();
            foreach (byte b in bytes)
                sb.AppendFormat("0x{0:x2}, ", b);
            var result = sb.ToString().TrimEnd(", ".ToCharArray());
            return result;
        }
    }
}
