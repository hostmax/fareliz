﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.WinForm;
using SkyDean.FareLiz.WinForm.Config;
using System;
using System.IO;

namespace FareLiz.Tests.FareLiz.WinForm
{
    [TestClass]
    public class ObjectIniConfigTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            string fileName = "UnitTestLogger";
            var logger = log4net.LogManager.GetLogger(fileName);
            var target = new ObjectIniConfig("UnitTest.ini", logger);
            string dept = "Departure Test Location String";
            string dest = "Destination Test Location String";
            bool isMin = true;
            DateTime deptDate = DateTime.Now;
            DateTime retDate = DateTime.Now.AddDays(7);

            var obj = new ExecutionInfo
            {
                Departure = Airport.FromIATA("HEL"),
                Destination = Airport.FromIATA("SGN"),
                IsMinimized = isMin,
                DepartureDate = deptDate,
                ReturnDate = retDate
            };
            target.SaveData(obj);
            bool actual = File.Exists(fileName);
            Assert.AreEqual(true, actual, "INI configuration file should be created");

            var fi = new FileInfo(fileName);
            var length = fi.Length;
            Assert.AreNotEqual(0, length, "INI Configuration file should not be empty");

            target.RemoveAllSections();
            target.Load(fileName);

            var newObj = new ExecutionInfo();  // Initialize empty object
            target.Load(fileName);
            target.ApplyData(newObj);

            Assert.AreEqual(newObj.Departure, dept);
            Assert.AreEqual(newObj.Destination, dest);
            Assert.AreEqual(newObj.IsMinimized, isMin);
            Assert.AreEqual(newObj.DepartureDate, deptDate);
            Assert.AreEqual(newObj.ReturnDate, retDate);
        }
    }
}
