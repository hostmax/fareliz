﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkyDean.FareLiz.Service;
using System.Windows.Forms;
using SkyDean.FareLiz.Core.Utils;

namespace FareLiz.Tests.FareLiz.Service
{
    [TestClass]
    public class ExceptionHelperTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var logger = log4net.LogManager.GetLogger("Test");
            var target = new ExceptionHelper(logger, true, false);
            Application.ThreadException += target.UnhandledThreadExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException += target.UnhandledExceptionHandler;

            throw new ApplicationException("TEST");
        }
    }
}
