﻿using ProtoBuf;
using SkyDean.FareLiz.Core.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace SkyDean.FareLiz.Core.Data
{
    /// <summary>
    /// Information on the airport
    /// </summary>
    [Serializable, ProtoContract]
    public class Airport
    {
        /// <summary>
        /// IATA code of the airport (3 letter)
        /// </summary>
        [ProtoMember(1)]
        public string IATA { get; set; }

        /// <summary>
        /// Airport name
        /// </summary>
        [XmlIgnore, ProtoIgnore]
        public string Name { get; set; }

        /// <summary>
        /// City of the airport
        /// </summary>
        [XmlIgnore, ProtoIgnore]
        public string City { get; set; }

        /// <summary>
        /// Country of the airport
        /// </summary>
        [XmlIgnore, ProtoIgnore]
        public string Country { get; set; }

        /// <summary>
        /// Airport's latitude co-ordinate
        /// </summary>
        [XmlIgnore, ProtoIgnore]
        public float Latitude { get; set; }

        /// <summary>
        /// Airport's longitude co-ordinate
        /// </summary>
        [XmlIgnore, ProtoIgnore]
        public float Longitude { get; set; }

        public static ReadOnlyCollection<Airport> Data { get; private set; }

        static Airport()
        {
            var data = new List<Airport>();
            using (var txtReader = new StringReader(Resources.Airports))
            using (var reader = new CsvReader(txtReader, false) { DefaultParseErrorAction = ParseErrorAction.AdvanceToNextLine, SkipEmptyLines = true })
            {
                while (reader.ReadNextRecord())
                {
                    string iata = reader[4];
                    if (String.IsNullOrEmpty(iata))
                        continue;

                    string name = reader[1],
                           city = reader[2],
                           country = reader[3];
                    float latitude = float.Parse(reader[6], NamingRule.NumberCulture),
                          longitude = float.Parse(reader[7], NamingRule.NumberCulture);
                    var airport = new Airport(name, city, country, iata, latitude, longitude);
                    data.Add(airport);
                }
            }

            data.Sort((a, b) => a.ToString().CompareTo(b.ToString()));
            Data = data.AsReadOnly();
        }

        // This constructor is needed for serialization
        protected Airport() { }

        protected Airport(string name, string city, string country, string iata, float latitude, float longitude)
        {
            Name = name;
            City = city;
            Country = country;
            IATA = iata;
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        /// Create a new Airport object from the IATA code
        /// </summary>
        /// <param name="iata"></param>
        /// <returns></returns>
        public static Airport FromIATA(string iata)
        {
            if (!String.IsNullOrEmpty(iata))
            {
                foreach (var a in Data)
                {
                    if (String.Equals(iata, a.IATA, StringComparison.OrdinalIgnoreCase))
                        return a;
                }
            }

            return null;
        }

        public override string ToString()
        {
            return String.Format("{0}, {1} ({2}) {3}", City, Name, Country, IATA);
        }
    }
}
