﻿using SkyDean.FareLiz.Core.Data;
using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FareLiz.Core")]
[assembly: AssemblyDescription("Core Library for SkyDean FareLiz")]
[assembly: AssemblyConfiguration("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d62193a3-e3cc-44e9-a21b-88f5411747b5")]
[assembly: AssemblyPublisher("http://www.skydean.com", "support@skydean.com")]