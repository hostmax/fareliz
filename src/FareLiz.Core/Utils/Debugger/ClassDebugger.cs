﻿#if DEBUG
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Utils.Debugger
{
    public static class ClassDebugger
    {
        public static void PrintClassFields(object target, bool recursive)
        {
            var sb = new StringBuilder();
            GatherClassFields(sb, target, recursive, new List<object>());
            var content = sb.ToString();
            Debug.WriteLine(content);
            Clipboard.SetText(content, TextDataFormat.UnicodeText);
        }

        public static void GatherClassFields(StringBuilder sb, object target, bool recursive, List<object> parentCalls)
        {
            var type = target.GetType();
            var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var fields = (recursive
                              ? type.GetFieldsRecursively(flags)
                              : type.GetFields(flags));

            sb.AppendLine("Class type: " + type);
            foreach (var f in fields)
            {
                var val = f.GetValue(target);
                var fType = f.FieldType;
                if (fType.IsValueType || fType.IsPrimitive || fType == typeof(string) || fType == typeof(DateTime) || val == null)
                {
                    string rep = String.Format(CultureInfo.InvariantCulture, "{0} [{1}] = {2}", f.Name, f.FieldType, val);
                    sb.AppendLine(rep);
                }
                else
                {
                    if (!parentCalls.Contains(target))
                    {
                        parentCalls.Add(val);
                        GatherClassFields(sb, val, recursive, parentCalls);
                    }
                }
            }

            var content = sb.ToString();
            Debug.WriteLine(content);
            Clipboard.SetText(content, TextDataFormat.UnicodeText);
        }
    }
}
#endif