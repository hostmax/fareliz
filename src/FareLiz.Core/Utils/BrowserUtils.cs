﻿using System.Diagnostics;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Utils
{
    public class BrowserUtils
    {
        public static void Open(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                try
                {
                    // How about IE?
                    Process.Start("iexplore", url);
                }
                catch
                {
                    var browserForm = new Form { Text = "Popup Browser", Icon = Properties.Resources.SkyDeanBlackIcon, WindowState = FormWindowState.Maximized };
                    var browser = new WebBrowser { Dock = DockStyle.Fill };
                    browser.Navigate(url);
                    browserForm.Controls.Add(browser);
                    browserForm.Show();
                }

            }
        }
    }
}
