﻿using log4net;
using System;
using System.Threading;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Utils
{
    public static class BackgroundThread
    {
        public static ThreadResult DoWork(Action action, int timeoutInSeconds, ILog _logger)
        {
            ThreadResult result = null;
            Thread workerThread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    action();
                    result = ThreadResult.Succeed();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    MessageBox.Show(ex.Message, "Application Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    result = ThreadResult.Fail(ex);
                }
            }));

            workerThread.Start();
            workerThread.Join(TimeSpan.FromSeconds(timeoutInSeconds));

            if (result == null) // The action did not complete
            {
                try { workerThread.Abort(); }
                catch { }

                result = ThreadResult.Timeout(timeoutInSeconds);
            }

            return result;
        }
    }

    public class ThreadResult
    {
        public Exception ExceptionObject { get; private set; }
        public bool Succeeded { get; private set; }
        public bool IsTimedout
        {
            get { return !Succeeded && ExceptionObject is TimeoutException; }
        }

        protected ThreadResult(bool succeeded, Exception ex)
        {
            if (!succeeded && ex == null)
                throw new ArgumentException("Exception cannot be nulled for failed result");
            Succeeded = succeeded;
            ExceptionObject = ex;
        }

        public static ThreadResult Timeout(int timeoutInSeconds)
        {
            return new ThreadResult(false, new TimeoutException(String.Format("Task did not finish within {0}s", timeoutInSeconds)));
        }

        public static ThreadResult Succeed()
        {
            return new ThreadResult(true, null);
        }

        public static ThreadResult Fail(Exception ex)
        {
            return new ThreadResult(false, ex);
        }
    }
}
