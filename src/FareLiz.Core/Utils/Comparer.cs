﻿using SkyDean.FareLiz.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SkyDean.FareLiz.Core.Utils
{
    public static class CompareExtensions
    {
        public static IEnumerable<T> DistinctBy<T, TIdentity>(this IEnumerable<T> source,
                                                              Func<T, TIdentity> identitySelector)
        {
            return source.Distinct(By(identitySelector));
        }

        public static IEqualityComparer<TSource> By<TSource, TIdentity>(Func<TSource, TIdentity> identitySelector)
        {
            return new DelegateComparer<TSource, TIdentity>(identitySelector);
        }

        private class DelegateComparer<T, TIdentity> : IEqualityComparer<T>
        {
            private readonly Func<T, TIdentity> identitySelector;

            public DelegateComparer(Func<T, TIdentity> identitySelector)
            {
                this.identitySelector = identitySelector;
            }

            public bool Equals(T x, T y)
            {
                return Equals(identitySelector(x), identitySelector(y));
            }

            public int GetHashCode(T obj)
            {
                return identitySelector(obj).GetHashCode();
            }
        }
    }

    public class JourneyDateEqualityComparer : IEqualityComparer<Journey>
    {
        public bool Equals(Journey x, Journey y)
        {
            return (x.DepartureDate.Date == y.DepartureDate.Date) && (x.ReturnDate.Date == y.ReturnDate.Date);
        }

        public int GetHashCode(Journey obj)
        {
            return obj.DepartureDate.GetHashCode() + obj.ReturnDate.GetHashCode();
        }
    }

    public class FlightPriceQualityComparer : IEqualityComparer<Flight>
    {
        public bool Equals(Flight x, Flight y)
        {
            return (x.InboundLeg.Departure == y.InboundLeg.Departure) &&
                   (x.OutboundLeg.Departure == y.OutboundLeg.Departure) && (x.Price == y.Price) &&
                   (x.Operator == y.Operator);
        }

        public int GetHashCode(Flight obj)
        {
            return obj.Operator.GetHashCode() + obj.OutboundLeg.Departure.GetHashCode() + obj.Price.GetHashCode();
        }
    }

    public class FlightPriceComparer : IComparer<Flight>
    {
        public int Compare(Flight x, Flight y)
        {
            if (x == null)
            {
                if (y == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (y == null)
                    return 1;
                else
                    return x.Price.CompareTo(y.Price);
            }
        }

        public static FlightPriceComparer Instance = new FlightPriceComparer();
    }
}