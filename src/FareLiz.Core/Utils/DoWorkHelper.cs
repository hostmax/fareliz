﻿using System;
using System.Globalization;
using System.Threading;

namespace SkyDean.FareLiz.Core
{
    public static class WorkerThread
    {
        public static DoWorkResult DoWork(Action action, int timeoutInSeconds)
        {
            DoWorkResult result = null;
            Thread doWorkThread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    action();
                    result = DoWorkResult.Succeed();
                }
                catch (Exception ex)
                {
                    result = DoWorkResult.Fail(ex);
                }
            }));

            doWorkThread.Start();
            doWorkThread.Join(TimeSpan.FromSeconds(timeoutInSeconds));

            if (result == null) // The action did not complete
            {
                try { doWorkThread.Abort(); }
                catch { }

                result = DoWorkResult.Timeout(timeoutInSeconds);
            }

            return result;
        }
    }

    public class DoWorkResult
    {
        public Exception Exception { get; private set; }
        public bool Succeeded { get; private set; }
        public bool IsTimedout
        {
            get { return !Succeeded && Exception is TimeoutException; }
        }

        protected DoWorkResult(bool succeeded, Exception ex)
        {
            if (!succeeded && ex == null)
                throw new ArgumentException("Exception cannot be nulled for failed result");
            Succeeded = succeeded;
            Exception = ex;
        }

        public static DoWorkResult Timeout(int timeoutInSeconds)
        {
            return new DoWorkResult(false, new TimeoutException(String.Format(CultureInfo.InvariantCulture, "Task did not finish within {0}s", timeoutInSeconds)));
        }

        public static DoWorkResult Succeed()
        {
            return new DoWorkResult(true, null);
        }

        public static DoWorkResult Fail(Exception ex)
        {
            return new DoWorkResult(false, ex);
        }
    }
}
