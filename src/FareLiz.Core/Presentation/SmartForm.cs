﻿using System;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Presentation
{
    /// <summary>
    /// Helper class for properly center-aligned form
    /// </summary>
    public class SmartForm : Form
    {
        protected override void OnLoad(EventArgs e)
        {
            // If the start location of the form is set to be centered align and or the parent form is missing
            if (StartPosition == FormStartPosition.CenterScreen
                || (StartPosition == FormStartPosition.CenterParent && ParentForm == null))
            {
                var screen = Screen.FromPoint(Cursor.Position);
                Left = screen.Bounds.Left + screen.Bounds.Width / 2 - Width / 2;
                Top = screen.Bounds.Top + screen.Bounds.Height / 2 - Height / 2;
            }

            base.OnLoad(e);
        }
    }
}
