﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Presentation
{
    public static class ControlExtensions
    {
        /// <summary>
        /// Measure the size of the text assosiated with the control
        /// </summary>
        public static Size GetTextSize(this Control target)
        {
            return String.IsNullOrEmpty(target.Text) ? Size.Empty : TextRenderer.MeasureText(target.Text, target.Font);
        }

        /// <summary>
        /// Asynchnorously invoke with validation check
        /// </summary>
        public static IAsyncResult BeginSafeInvoke(this Control target, Delegate method, params object[] args)
        {
            if (CanInvoke(target))
                return target.BeginInvoke(method, args);
            return null;
        }

        /// <summary>
        /// Asynchnorously invoke with validation check
        /// </summary>
        public static IAsyncResult BeginSafeInvoke(this Control target, Delegate method)
        {
            return BeginSafeInvoke(target, method, null);
        }

        /// <summary>
        /// Invoke with disposed check
        /// </summary>
        public static object SafeInvoke(this Control target, Delegate method, params object[] args)
        {
            if (target.CanInvoke())
                return target.Invoke(method, args);
            return null;
        }

        /// <summary>
        /// Invoke with null and disposed check
        /// </summary>
        public static object SafeInvoke(this Control target, Delegate method)
        {
            return target.SafeInvoke(method, null);
        }

        /// <summary>
        /// Check if the control can be invoked (check if the control handled is created and not yet disposed)
        /// </summary>
        public static bool CanInvoke(this Control target)
        {
            return !target.IsUnusable();
        }

        /// <summary>
        /// Check if the control is no longer usable (check if the control handled is created and not yet disposed)
        /// </summary>
        public static bool IsUnusable(this Control target)
        {
            return (target.IsDestructed() || !target.IsHandleCreated);
        }

        /// <summary>
        /// Check if the control is disposed or being disposed
        /// </summary>
        public static bool IsDestructed(this Control target)
        {
            return target.IsDisposed || target.Disposing;
        }
    }
}
