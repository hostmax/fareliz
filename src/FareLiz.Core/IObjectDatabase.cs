﻿using System;
using System.Collections.Generic;

namespace SkyDean.FareLiz.Core
{
    /// <summary>
    /// Interface for object-storage database
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObjectDatabase<T>
    {
        /// <summary>
        /// Add list of data into the database
        /// </summary>
        /// <param name="data">List of data</param>
        void AddData(IList<T> data);

        /// <summary>
        /// Create the data (any existing data will be replaced)
        /// </summary>
        /// <param name="data">List of data</param>
        void ResetData(IList<T> data);

        /// <summary>
        /// Reset and empty the database
        /// </summary>
        void Reset();

        /// <summary>
        /// Repair the database structure/data
        /// </summary>
        /// <returns>Success</returns>
        void RepairDatabase();

        /// <summary>
        /// Validate the database structure/data
        /// </summary>
        /// <returns>Validation result</returns>
        ValidateResult ValidateDatabase();
    }
}
