﻿using SkyDean.FareLiz.Core.Data;
using System;

namespace SkyDean.FareLiz.WinForm.Config
{
    internal static class IniConfigHandler
    {
        internal static bool FromString(string data, Type targetType, out object output)
        {
            bool supported = true;
            if (targetType == typeof(Airport))
                output = Airport.FromIATA(data);
            else
            {
                supported = false;
                output = null;
            }

            return supported;
        }

        internal static bool ToStorageString(object data, out string output)
        {
            bool supported = true;
            if (data != null)
            {
                var test = data as Airport;
                if (test != null)
                    output = test.IATA;
                else
                {
                    supported = false;
                    output = null;
                }
            }
            else
            {
                supported = false;
                output = null;
            }

            return supported;
        }
    }
}
