﻿using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.Service.Utils;
using SkyDean.FareLiz.WinForm.Controls;
using SkyDean.FareLiz.WinForm.Monitoring;
using SkyDean.FareLiz.WinForm.Properties;
using SkyDean.FareLiz.WinForm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Presentation
{
    /// <summary>
    /// Windows Form used for checking flexible fare data
    /// </summary>
    internal partial class CheckFareForm : SmartForm
    {
        private readonly string _liveFareDir = Path.Combine(ProcessUtils.CurrentProcessDirectory, "LiveFare");
        private readonly LiveFareFileStorage _liveFareStorage;
        private readonly TaskbarTextNotifier notifier = new TaskbarTextNotifier();
        private readonly ToolStripControl<Windows7ProgressBar> loadProgress = new ToolStripControl<Windows7ProgressBar>() { Visible = false };
        private readonly ToolStripStatusLabel springLabelStrip = new ToolStripStatusLabel() { Spring = true };
        private readonly ToolStripControl<CheckBox> chkAutoFocusTabStrip = new ToolStripControl<CheckBox>();
        private readonly ToolStripControl<CheckBox> chkAutoSyncStrip = new ToolStripControl<CheckBox>();
        private readonly ToolStripControl<CheckBox> chkExitAfterDoneStrip = new ToolStripControl<CheckBox>();
        private readonly AggeratingFareRequestMonitor _monitors = new AggeratingFareRequestMonitor();
        private bool _isChangingButtonState = false;

        private readonly Color BrowserStartingColor = Color.FromArgb(255, 255, 245),
                               BrowserSuccessColor = Color.FromArgb(237, 255, 237),
                               BrowserFailedColor = Color.FromArgb(255, 240, 240);

        private bool _firstCloseForLiveMonitor = true;
        private ExecutionParam _executionParam;

        public CheckFareForm(ExecutionParam param)
        {
            _liveFareStorage = new LiveFareFileStorage(_liveFareDir);
            InitializeComponent();
            Text = AppUtil.ProductName + " " + Text;

            Initialize(param);
            trayIcon.Icon = Icon;
            GUIBuilder.AttachMenuToTrayIcon(this, trayIcon, true);
            fareBrowserTabs.ImageList = new ImageList { ImageSize = new Size(6, 6) }; // Get extra empty space on tab header
        }

        private void Initialize(ExecutionParam param)
        {
            // Update the view based on Execution parameters
            _executionParam = param.ReflectionDeepClone(GlobalContext.Logger);

            txtDeparture.SelectedAirport = _executionParam.Departure;
            txtDestination.SelectedAirport = _executionParam.Destination;
            if (_executionParam.MinStayDuration == Int32.MinValue || _executionParam.MinStayDuration == Int32.MaxValue)
                _executionParam.MinStayDuration = Settings.Default.DefaultDurationMin;
            if (_executionParam.MaxStayDuration == Int32.MinValue || _executionParam.MaxStayDuration == Int32.MaxValue)
                _executionParam.MaxStayDuration = Settings.Default.DefaultDurationMax;
            if (_executionParam.DepartureDate.IsUndefined() || _executionParam.DepartureDate < DateTime.Now)
                _executionParam.DepartureDate = DateTime.Now.AddDays(1);
            if (_executionParam.ReturnDate.IsUndefined() || _executionParam.ReturnDate < _executionParam.DepartureDate)
                _executionParam.ReturnDate = _executionParam.DepartureDate.AddDays(_executionParam.MaxStayDuration);
            departureDatePicker.MinDate = returnDatePicker.MinDate = DateTime.Now.Date;
            departureDatePicker.Value = _executionParam.DepartureDate;
            returnDatePicker.Value = _executionParam.ReturnDate;
            numDepartDateRangePlus.Value = _executionParam.DepartureDateRange.Plus;
            numDepartDateRangeMinus.Value = _executionParam.DepartureDateRange.Minus;
            numReturnDateRangePlus.Value = _executionParam.ReturnDateRange.Plus;
            numReturnDateRangeMinus.Value = _executionParam.ReturnDateRange.Minus;
            numMinDuration.Value = _executionParam.MinStayDuration;
            numMaxDuration.Value = _executionParam.MaxStayDuration;
            numPriceLimit.Value = _executionParam.PriceLimit > 0 ? _executionParam.PriceLimit : 1;

            loadProgress.ControlItem.ContainerControl = this;
            loadProgress.ControlItem.ShowInTaskbar = false;
            statusStrip.Items.Add(loadProgress);

            statusStrip.Items.Add(springLabelStrip);

            chkAutoFocusTabStrip.Control.Text = "Auto-focus completed tab";
            chkAutoFocusTabStrip.Alignment = ToolStripItemAlignment.Right;
            statusStrip.Items.Add(chkAutoFocusTabStrip);

            chkAutoSyncStrip.Control.Text = "Auto-sync exported data";
            chkAutoSyncStrip.Alignment = ToolStripItemAlignment.Right;
            chkAutoSyncStrip.ControlItem.DataBindings.Clear();
            chkAutoSyncStrip.ControlItem.DataBindings.Add("Checked", _executionParam, "AutoSync");
            statusStrip.Items.Add(chkAutoSyncStrip);

            chkExitAfterDoneStrip.ControlItem.Text = "Exit after done";
            chkExitAfterDoneStrip.ControlItem.DataBindings.Clear();
            chkExitAfterDoneStrip.ControlItem.DataBindings.Add("Checked", _executionParam, "ExitAfterDone");
            chkExitAfterDoneStrip.Alignment = ToolStripItemAlignment.Right;
            statusStrip.Items.Add(chkExitAfterDoneStrip);
            foreach (ToolStripItem item in statusStrip.Items)
                item.Margin = new Padding(3, 1, 3, 1);

            UpdateViewForDuration();
            ResizeStatusStrip();
        }

        private void UpdateViewForDuration()
        {
            bool skipDurationConstraint = numDepartDateRangePlus.Value == 0 && numDepartDateRangeMinus.Value == 0
                                          && numReturnDateRangePlus.Value == 0 && numReturnDateRangeMinus.Value == 0;
            numMinDuration.Enabled = numMaxDuration.Enabled = !skipDurationConstraint;
        }

        /// <summary>
        /// Get list of fare scanning requests based on the condition specified on the view
        /// </summary>
        private List<FareBrowserRequest> GetRequests()
        {
            int minDuration = (numMinDuration.Enabled ? Decimal.ToInt32(numMinDuration.Value) : int.MinValue);
            int maxDuration = (numMaxDuration.Enabled ? Decimal.ToInt32(numMaxDuration.Value) : int.MaxValue);
            var result = new List<FareBrowserRequest>();
            bool roundTrip = chkReturnDate.Checked;

            for (int i = -Convert.ToInt32(numDepartDateRangeMinus.Value); i <= numDepartDateRangePlus.Value; i++)
            {
                int retStart = roundTrip ? -Convert.ToInt32(numReturnDateRangeMinus.Value) : 0;
                int retEnd = roundTrip ? Convert.ToInt32(numReturnDateRangePlus.Value) : 0;
                for (int j = retStart; j <= retEnd; j++)
                {
                    DateTime dept = departureDatePicker.Value.Date.AddDays(i);
                    DateTime ret = roundTrip ? returnDatePicker.Value.Date.AddDays(j) : DateTime.MinValue;
                    int stayDuration = (int)(ret - dept).TotalDays;
                    if (roundTrip && (stayDuration < minDuration || stayDuration > maxDuration))
                        continue;

                    if (dept.Date >= DateTime.Now.Date && (!roundTrip || ret.Date >= dept.Date)) // Make sure that the travel date range is valid
                        result.Add(new FareBrowserRequest(txtDeparture.SelectedAirport, txtDestination.SelectedAirport, dept, ret));
                }
            }

            return result;
        }

        /// <summary>
        /// Create new monitor for requests depending on the operation mode
        /// </summary>
        private FareRequestMonitor CreateMonitor(List<FareBrowserRequest> requests, OperationMode operationMode, bool autoSyncData)
        {
            if (requests == null || requests.Count < 1)
                return null;

            FareRequestMonitor newMon = null;
            if (operationMode == OperationMode.LiveMonitor)
            {
                newMon = new LiveFareMonitor(_liveFareStorage);
                newMon.RequestStarting += LiveFare_RequestStarting;
                newMon.MonitorStopping += LiveFareMonitorStopping;
                newMon.Enqueue(requests);
            }
            else
            {
                if (operationMode == OperationMode.ShowFare)
                {
                    newMon = new FareRequestMonitor();
                    newMon.RequestCompleted += ShowFare_RequestCompleted;
                }
                else if (operationMode == OperationMode.GetFareAndSave)
                {
                    newMon = new FareExportMonitor(IsAutoSyncFunc);
                    newMon.RequestCompleted += CloseAndExport_RequestCompleted;
                }
                else
                    throw new ApplicationException("Unsupported opearation mode: " + operationMode);

                newMon.Enqueue(requests);
                newMon.RequestStarting += FareMonitor_RequestStarting;
                newMon.RequestCompleted += FareMonitor_RequestCompleted;
                newMon.RequestStopping += FareMonitor_RequestCompleted;
            }

            return newMon;
        }

        private bool IsAutoSyncFunc()
        {
            if (InvokeRequired)
                return (bool)this.SafeInvoke(new Func<bool>(() => IsAutoSyncFunc()));
            return chkAutoSyncStrip.ControlItem.Checked;
        }

        /// <summary>
        /// Start new fare scan and return the monitor responsible for the new requests
        /// </summary>
        private FareRequestMonitor BeginFareScan(OperationMode mode)
        {
            btnShowFare.Enabled = btnLiveMonitor.Enabled = btnGetFareAndSave.Enabled = false;
            loadProgress.ControlItem.Value = loadProgress.ControlItem.Maximum = 0;
            loadProgress.ControlItem.ShowInTaskbar = false;
            if (mode == OperationMode.ShowFare)
                btnSummary.Enabled = btnSave.Enabled = btnUploadPackages.Enabled = false;

            FareRequestMonitor newMon = null;
            try
            {
                // Validate the location of the journey
                if (txtDeparture.Text == txtDestination.Text)
                {
                    MessageBox.Show(this, "Departure location and destination cannot be the same:" + Environment.NewLine + txtDestination.Text, "Invalid Route", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return null;
                }

                // Get list of new requests for the monitor
                var newRequests = GetRequests();
                if (newRequests.Count > 0)
                {
                    newMon = CreateMonitor(newRequests, mode, IsAutoSyncFunc());

                    if (mode == OperationMode.LiveMonitor)
                    {
                        trayIcon.Visible = true;
                    }
                    else
                    {
                        _monitors.Clear(newMon.GetType());
                        ClearBrowserTabs();
                        loadProgress.ControlItem.Maximum = newRequests.Count;
                        loadProgress.ControlItem.Style = (newRequests.Count == 1 ? ProgressBarStyle.Marquee : ProgressBarStyle.Continuous);
                        loadProgress.Visible = loadProgress.ControlItem.ShowInTaskbar = true;
                    }

                    GlobalContext.Logger.InfoFormat("{0}: Starting monitor for {1} new requests", GetType().Name, newRequests.Count);
                    _monitors.Start(newMon);
                }
                else
                {
                    if (_executionParam.ExitAfterDone)
                        Environment.Exit(1);
                    else
                    {
                        string period = (numMinDuration.Value == numMaxDuration.Value
                            ? numMinDuration.Value.ToString(CultureInfo.InvariantCulture)
                            : numMinDuration.Value.ToString(CultureInfo.InvariantCulture) + " and " + numMaxDuration.Value.ToString(CultureInfo.InvariantCulture));

                        string message = String.Format(@"There is no travel date which satisfies the filter condition for the stay duration between {0} days (You selected travel period {1})!

Double-check the filter conditions and make sure that not all travel dates are in the past", period, StringUtil.GetPeriodString(departureDatePicker.Value, returnDatePicker.Value));
                        GlobalContext.Logger.Debug(message);
                        MessageBox.Show(message, "Invalid parameters", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Failed to start monitors: " + ex.Message, "Check Fares", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GlobalContext.Logger.Error("Failed to start monitors: " + ex.ToString());
            }
            finally
            {
                btnShowFare.Enabled = btnLiveMonitor.Enabled = btnGetFareAndSave.Enabled = true;
            }

            return newMon;
        }

        private void ClearBrowserTabs()
        {
            fareBrowserTabs.SuspendLayout();
            foreach (TabPage t in fareBrowserTabs.TabPages)
            {
                fareBrowserTabs.TabPages.Remove(t);
                t.Dispose();
            }
            fareBrowserTabs.ResumeLayout();
        }

        private IList<TravelRoute> ExtractTabData()
        {
            GlobalContext.Logger.Debug("Generating journey from all tabs...");
            var result = new List<TravelRoute>();

            foreach (TabPage tab in fareBrowserTabs.TabPages)
            {
                try
                {
                    var request = tab.Tag as FareBrowserRequest;
                    if (request != null && request.BrowserControl != null)
                    {
                        TravelRoute route = request.BrowserControl.LastRetrievedRoute;
                        if (route != null)
                        {
                            TravelRoute existRoute = null;
                            foreach (var r in result)
                            {
                                if (r.IsSameRoute(route))
                                {
                                    existRoute = r;
                                    break;
                                }
                            }

                            if (existRoute == null)
                            {
                                existRoute = new TravelRoute(route);
                                result.Add(route);
                            }

                            existRoute.AddJourney(route.Journeys, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GlobalContext.Logger.ErrorFormat("Failed to get data from tab: " + ex);
                }
            }

            return result;
        }

        private void SetStatus(string text, Image image)
        {
            if (InvokeRequired)
                this.SafeInvoke(new MethodInvoker(() => SetStatus(text, image)));
            else
            {
                lblStatus.Text = text;
                if (lblStatus.Image != image)
                    lblStatus.Image = image;
                ResizeStatusStrip();
            }
        }

        private void SetStatus(FareBrowserRequest request)
        {
            string action = (request.BrowserControl.RequestState == DataRequestState.Pending ? "Getting" : "Waiting");
            bool oneWayTrip = request.ReturnDate.IsUndefined();
            string trip = String.Format("{0} trip {1}{2}", oneWayTrip ? "one-way" : "round", request.DepartureDate.ToShortDayAndDateString(), oneWayTrip ? "" : " - " + request.ReturnDate.ToShortDayAndDateString());
            var img = (request.BrowserControl.RequestState == DataRequestState.Pending ? Properties.Resources.Loading : null);
            string message = String.Format("{0} fare data for {1} ({2}/{3})...",
                                           action,
                                           trip,
                                           loadProgress.ControlItem.Value,
                                           loadProgress.ControlItem.Maximum);
            GlobalContext.Logger.Debug(message);
            SetStatus(message, img);
        }

        private void ResizeStatusStrip()
        {
            int minusWidth = (statusStrip.SizingGrip ? statusStrip.SizeGripBounds.Width : 0) + 5 + 2 * SystemInformation.BorderSize.Width + loadProgress.Margin.Left + loadProgress.Margin.Right;
            foreach (ToolStripItem item in statusStrip.Items)
                if (item != loadProgress && item != springLabelStrip && item.Visible)
                    minusWidth += item.Width + item.Margin.Left + item.Margin.Right;

            int newWidth = statusStrip.Width - minusWidth;
            if (newWidth > 0)
                loadProgress.Size = new Size(newWidth, statusStrip.Height / 2);
        }

        private void CheckProgress()
        {
            if (loadProgress.ControlItem.Value == loadProgress.ControlItem.Maximum)
            {
                if (_executionParam.ExitAfterDone)
                    Environment.Exit(0);

                loadProgress.ControlItem.Value = loadProgress.ControlItem.Maximum = 0;
                loadProgress.Visible = false;
                loadProgress.ControlItem.ShowInTaskbar = false;
                SetStatus("Idle", null);
            }
        }

        private void IncreaseProgress()
        {
            if (loadProgress.ControlItem.Value < loadProgress.ControlItem.Maximum)
                loadProgress.ControlItem.Value++;
        }

        private void CheckFareForm_Load(object sender, EventArgs e)
        {
            // Click the appropriate button after everything is fully loaded
            if (_executionParam.OperationMode != OperationMode.Unspecified)
            {
                var opMode = _executionParam.OperationMode;
                if (opMode == OperationMode.ShowFare)
                    btnShowFare.PerformClick();
                else if (opMode == OperationMode.GetFareAndSave)
                    btnGetFareAndSave.PerformClick();
                else if (opMode == OperationMode.LiveMonitor)
                    btnLiveMonitor.PerformClick();

                WindowState = _executionParam.IsMinimized ? FormWindowState.Minimized : FormWindowState.Normal;
                ShowInTaskbar = (WindowState != FormWindowState.Minimized);
            }
        }

        void FareMonitor_RequestStarting(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            if (InvokeRequired)
                this.SafeInvoke(new Action(() => FareMonitor_RequestStarting(sender, args)));
            else
            {
                FareBrowserRequest request = args.Request;
                request.Initialize();   // Initialize request here: So that the control is created on the UI thread
                var browserTabPage = new TabPage(request.DepartureDate.ToString(NamingRule.DATE_FORMAT)
                + (request.IsRoundTrip ? " - " + request.ReturnDate.ToString(NamingRule.DATE_FORMAT) : String.Empty));

                FareBrowserControl browser = request.BrowserControl;
                browser.Dock = DockStyle.Fill;
                browserTabPage.Controls.Add(browser);
                browserTabPage.BackColor = BrowserStartingColor;
                browserTabPage.ImageIndex = 0;
                browserTabPage.Tag = request;
                browserTabPage.ToolTipText = request.Detail;
                fareBrowserTabs.TabPages.Add(browserTabPage);
                SetStatus(request);
            }
        }

        void CloseAndExport_RequestCompleted(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            this.SafeInvoke(new Action(() =>
            {
                var tabPageObj = args.Request.BrowserControl.Parent as TabPage;
                if (tabPageObj != null)
                {
                    fareBrowserTabs.TabPages.Remove(tabPageObj);
                    tabPageObj.Dispose();
                }

                if (sender.State == MonitorState.Stopped && chkExitAfterDoneStrip.ControlItem.Checked)
                    Environment.Exit(0);
            }));
        }

        void ShowFare_RequestCompleted(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            if (InvokeRequired)
                this.SafeInvoke(new Action(() => ShowFare_RequestCompleted(sender, args)));
            else
            {
                FareBrowserControl fareBrowserObj = args.Request.BrowserControl;
                if (fareBrowserObj.IsDestructed()
                    || fareBrowserObj.LastRequestInitiatedDate != args.RequestInitiatedDate || fareBrowserObj.RequestState == DataRequestState.Pending)
                    return;

                var browserTabPage = fareBrowserObj.Parent as TabPage;
                if (browserTabPage == null)
                    return;

                if (fareBrowserObj.RequestState == DataRequestState.Pending || fareBrowserObj.RequestState == DataRequestState.Requested)
                    browserTabPage.BackColor = BrowserStartingColor;
                else if (fareBrowserObj.RequestState == DataRequestState.Ok)
                    browserTabPage.BackColor = BrowserSuccessColor;
                else
                    browserTabPage.BackColor = BrowserFailedColor;

                if (chkAutoFocusTabStrip.ControlItem.Checked)
                    fareBrowserTabs.SelectedTab = browserTabPage;
            }
        }

        void FareMonitor_RequestCompleted(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            // This method should be triggered both before stopping the request or after the request is completed
            var browser = args.Request.BrowserControl;  // The browser might be nulled
            this.SafeInvoke(new Action(() =>
                {
                    SwitchButton senderBtn = null;
                    var monType = sender.GetType();
                    var resultRoute = (browser == null ? null : browser.LastRetrievedRoute);

                    if (monType == typeof(FareRequestMonitor))
                    {
                        // Browser can be nulled if the request was aborted before it is event started
                        if (browser != null && resultRoute != null && !btnSummary.Enabled)
                            btnSummary.Enabled = btnSave.Enabled = btnUploadPackages.Enabled = true;
                        senderBtn = btnShowFare;
                    }
                    else if (monType == typeof(FareExportMonitor))
                        senderBtn = btnGetFareAndSave;

                    IncreaseProgress();
                    CheckProgress();

                    if (!loadProgress.Visible && senderBtn != null)
                    {
                        var senderMonitor = senderBtn.Tag as FareRequestMonitor;
                        if (senderMonitor == sender && senderBtn.IsSecondState)
                            senderBtn.IsSecondState = false;    // Reset the button after everything is done!
                    }
                }));
        }

        void LiveFareMonitorStopping(FareRequestMonitor sender)
        {
            btnLiveMonitor.IsSecondState = false;
        }

        void LiveFare_RequestStarting(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            args.Request.Initialize();
            var monitor = sender as LiveFareMonitor;
            monitor.PriceLimit = (double)numPriceLimit.Value;
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            try
            {
                var db = GlobalContext.MonitorEnvironment.FareDatabase;
                GlobalContext.Logger.Info("Export current fare data on all tabs");
                var tabRoutes = ExtractTabData();
                db.AddData(tabRoutes);
            }
            finally
            {
                btnSave.Enabled = true;
            }
        }

        private void btnUploadPackages_Click(object sender, EventArgs e)
        {
            var syncDb = GlobalContext.MonitorEnvironment.FareDatabase as ISyncableDatabase;
            if (syncDb == null)
                MessageBox.Show(this, "You have not selected a database type which supports data synchronization", "Unsupported Operation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            else
            {
                var journeys = ExtractTabData();
                if (journeys.Count == 0)
                    MessageBox.Show(this, "There is no journey available for synchronizing! Get fare data first!", "Unsupported Operation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                else
                {
                    string pkgId = null;
                    ProgressDialog.ExecuteTask(null, "Data Package Sending", "Please wait while the new data packages are being sent...", GetType().Name + "UploadPackages", ProgressBarStyle.Marquee, GlobalContext.Logger, callback =>
                    {
                        callback.Begin();
                        pkgId = syncDb.SendData(journeys);

                        if (pkgId != null)
                            callback.ShowDialog("A new package with ID " + pkgId + " has been successfully sent using the configured synchronizer", "Package Sending", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    });
                }
            }
        }

        private void StartMonitorButton_Click(object sender, EventArgs e)
        {
            _executionParam.ExitAfterDone = false;
            var btn = sender as SwitchButton;
            if (btn != null)
            {
                if (btn.IsSecondState) // The monitor is actually running
                {
                    var existMon = ((Control)sender).Tag as FareRequestMonitor;
                    if (existMon != null)
                    {
                        existMon.Stop();
                        _monitors.Clear(existMon.GetType());
                    }

                    if (sender == btnLiveMonitor)
                    {
                        if (trayIcon.Icon != null)
                            trayIcon.Visible = false;

                        if (!Visible)
                            Show();
                    }
                }
                else
                {
                    btn.Enabled = false;
                    try
                    {

                        OperationMode mode = OperationMode.Unspecified;
                        if (sender == btnShowFare)
                            mode = OperationMode.ShowFare;
                        else if (sender == btnGetFareAndSave)
                            mode = OperationMode.GetFareAndSave;
                        else if (sender == btnLiveMonitor)
                            mode = OperationMode.LiveMonitor;
                        else
                            throw new NotImplementedException("Unsupported fare monitor mode!");

                        var newMon = BeginFareScan(mode);
                        if (newMon != null)
                        {
                            if (mode == OperationMode.LiveMonitor)
                                notifier.Show("Live Fare Monitor", "Live Fare Monitor will run in background" + Environment.NewLine + "You will receive a notification whenever the flight fare has been changed",
                                              7000, NotificationType.Success, false);
                            btn.IsSecondState = true;
                        }
                        btn.Tag = newMon;
                    }
                    finally
                    {
                        btn.Enabled = true;
                    }
                }
            }
        }

        void btnSummary_Click(object sender, EventArgs e)
        {
            string err = null;
            IList<TravelRoute> tabData = null;
            var tabPagesCount = fareBrowserTabs.TabPages.Count;

            if (tabPagesCount < 1)
                err = "There is no loaded flight data. Select to show the fare first before viewing the summary!";
            else
            {
                tabData = ExtractTabData();
                // Remove empty data set
                for (int i = 0; i < tabData.Count; i++)
                {
                    var route = tabData[i];
                    if (route.Journeys.Count < 1)
                        tabData.RemoveAt(i--);
                }

                // Set error message if there is not sufficient data for summary view
                if (tabData != null)
                {
                    if (tabData.Count < 1)
                        err = "There is no available data. Please wait until the fare browser has finished loading data!";
                    else if (tabData.Count == 1)
                    {
                        if (tabPagesCount == 1)
                            err = "There is only one loaded journey. The summary view is only helpful when you have loaded multiple journeys. Try changing the travel date offsets and try again!";
                        else if (tabData[0].Journeys.Count == 1)
                            err = "Only one journey has completed loading. The summary view is only helpful when you have multiple loaded journeys. Please wait until the application has finished loading another journey and try again!";
                    }
                }
            }

            if (err != null)
            {
                MessageBox.Show(this, err, "There is not enough data", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            var newParam = _executionParam.ReflectionDeepClone(GlobalContext.Logger);
            int minStay = 0, maxStay = 0;
            double maxPrice = 0;
            foreach (var r in tabData)
                foreach (var j in r.Journeys)
                {
                    if (j.StayDuration < minStay)
                        minStay = j.StayDuration;
                    else if (j.StayDuration > maxStay)
                        maxStay = j.StayDuration;
                    var jPrice = j.Data.Max(d => d.Flights.Max(f => f.Price));
                    if (jPrice > maxPrice)
                        maxPrice = jPrice;
                }

            var priceLim = (int)Math.Ceiling(maxPrice);
            newParam.MinStayDuration = minStay;
            newParam.MaxStayDuration = maxStay;
            newParam.PriceLimit = priceLim;
            var summaryForm = new FlightStatisticForm(tabData, newParam, false);
            summaryForm.Show(this);
        }

        private void datePicker_ValueChanged(object sender, CheckDateEventArgs e)
        {
            if (sender == departureDatePicker)
                returnDatePicker.MinDate = departureDatePicker.Value;

            if (departureDatePicker.Value > returnDatePicker.Value)
                returnDatePicker.Value = departureDatePicker.Value.AddDays(7);
        }

        private void CheckFareForm_Resize(object sender, EventArgs e)
        {
            ResizeStatusStrip();
        }

        private void lblStatus_TextChanged(object sender, EventArgs e)
        {
            ResizeStatusStrip();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            ShowInTaskbar = true;
            WindowState = FormWindowState.Normal;
        }

        private void btnLiveFareData_Click(object sender, EventArgs e)
        {
            new LiveFareDataForm(_liveFareStorage).ShowDialog(this);
        }

        private void CheckFareForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnLiveMonitor.IsSecondState) // If LiveMonitor is Running
            {
                e.Cancel = true;
                if (_firstCloseForLiveMonitor)
                {
                    trayIcon.ShowBalloonTip(500, "Run in tray", "Fare Monitor will continue monitoring in System tray", ToolTipIcon.Info);
                    _firstCloseForLiveMonitor = false;
                }

                Hide();
            }
            else
            {
                if (loadProgress.Visible)   // Something is on-going
                {
                    var closeDialog = MessageBox.Show(this, "Fare scanning is in progress. Are you sure you want to close this window and abort the active operations?", "Operations in progress", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (closeDialog == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                _monitors.Clear();
                Dispose();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            // Close window if user press Esc and Live Monitor is running
            if (btnLiveMonitor.IsSecondState && keyData == Keys.Escape)
                Close();
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnNoDepartRange_Click(object sender, EventArgs e)
        {
            numDepartDateRangeMinus.Value = numDepartDateRangePlus.Value = 0;
        }

        private void btnNoReturnRange_Click(object sender, EventArgs e)
        {
            numReturnDateRangeMinus.Value = numReturnDateRangePlus.Value = 0;
        }

        private void fareBrowserTabs_TabPageRefreshing(object sender, TabControlEventArgs e)
        {
            var browserTabPage = e.TabPage;
            var request = browserTabPage.Tag as FareBrowserRequest;
            if (request != null)
            {
                request.Start();
                browserTabPage.BackColor = BrowserStartingColor;
                browserTabPage.ImageIndex = 0;
            }
        }

        private void fareBrowserTabs_TabPageClosing(object sender, TabControlCancelEventArgs e)
        {
            var tagPage = e.TabPage;
            var request = tagPage.Tag as FareBrowserRequest;
            if (request != null)
            {
                request.Stop();
                request.OwnerMonitor.FinalizeRequest(request);
                if (!(request.BrowserControl.RequestState > DataRequestState.Requested) && loadProgress.ControlItem.Maximum > 0) // If the request was not completed: Increase the progress
                {
                    IncreaseProgress();
                    CheckProgress();
                }

                if (fareBrowserTabs.TabPages.Count == 1)    // This is the last tab: Disable unneeded buttons
                    btnSave.Enabled = btnUploadPackages.Enabled = btnSummary.Enabled = false;
            }
        }

        private void chkReturnDate_CheckedChanged(object sender, EventArgs e)
        {
            lblDuration.Enabled = numMinDuration.Enabled = numMaxDuration.Enabled =
                returnDatePicker.Enabled = numReturnDateRangeMinus.Enabled = numReturnDateRangePlus.Enabled = lblReturnPM.Enabled = btnNoReturnRange.Enabled
                = chkReturnDate.Checked;
        }

        private void GetFareButtons_StateChanging(object sender, CancelEventArgs e)
        {
            if (_isChangingButtonState)
                return;

            _isChangingButtonState = true;
            var btn = sender as SwitchButton;
            if (btn != null)
            {
                if (btn == btnShowFare)
                    btnGetFareAndSave.Enabled = btn.IsSecondState;
                else if (btn == btnGetFareAndSave)
                    btnShowFare.Enabled = btn.IsSecondState;
            }
            _isChangingButtonState = false;
        }

        private void DateRange_ValueChanged(object sender, EventArgs e)
        {
            UpdateViewForDuration();
        }
    }
}