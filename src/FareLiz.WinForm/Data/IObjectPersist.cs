﻿using log4net;

namespace SkyDean.FareLiz.WinForm.Data
{
    internal interface IObjectPersist
    {
        ILog Logger { get; set; }
        void ApplyData(object targetObject);
        void SaveData(object targetObject);
    }
}
