﻿using log4net;
using log4net.Repository.Hierarchy;
using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Config;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Data
{
    [DisplayName("File Archive Manager")]
    [Description("Data Archive Manager using persistent file system storage")]
    public class FileArchiveManager : IArchiveManager
    {
        public IFareDataProvider FareDataProvider { get; set; }
        public IFareDatabase FareDatabase { get; set; }
        public List<string> AcceptedExtensions = new List<string> { ".htm", ".html", ".xml", ".fle" };

        public ILog Logger { get; set; }

        public IConfig DefaultConfig
        {
            get
            {
                return new FileArchiveManagerConfig
                {
                    ArchivePath = "Archives",
                    ProcessBatchSize = 500
                };
            }
        }


        public IConfigBuilder CustomConfigBuilder
        {
            get { return null; }
        }

        private const string BIN_EXTENSION = ".fle";
        private const string XML_EXTENSION = ".xml";

        public FileArchiveManager() { }

        public FileArchiveManager(IFareDataProvider dataProvider, IFareDatabase fareDatabase, ILog logger)
            : this()
        {
            FareDataProvider = dataProvider;
            FareDatabase = fareDatabase;
            Logger = logger;
        }

        FileArchiveManagerConfig _config;
        public IConfig Configuration
        {
            get { return _config; }
            set { _config = value as FileArchiveManagerConfig; }
        }
        public bool IsConfigurable { get { return false; } }

        public void Initialize()
        {
            if (Configuration == null)
                Configuration = DefaultConfig;

            if (!String.IsNullOrEmpty(_config.ArchivePath))
                Directory.CreateDirectory(_config.ArchivePath);
        }

        public IList<TravelRoute> ImportData(string path, DataOptions options)
        {
            if (String.IsNullOrEmpty(path))
                path = ".\\";

            if (!Directory.Exists(path))
                throw new ArgumentException("Target path does not exist: " + path);

            IList<TravelRoute> result = null;
            ProgressDialog.ExecuteTask(null, "Load journey data from local files", "Please wait...", "ArchiveImport",
                                                    ProgressBarStyle.Continuous, Logger, callback =>
                        {
                            result = DoImportData(path, options, callback);
                        }, null, null, options.ShowProgressDialog, options.ShowProgressDialog);

            return result;
        }

        private List<TravelRoute> DoImportData(string path, DataOptions options, IProgressCallback callback)
        {
            var result = new List<TravelRoute>();
            callback.Begin(); 
            callback.Text = "Getting file list...";
            string[] fileList = Directory.GetFiles(path, "*");
            var acceptedFiles = new List<string>();
            foreach (var file in fileList)
            {
                string fileExt = Path.GetExtension(file);
                foreach (var ext in AcceptedExtensions)
                    if (String.Equals(fileExt, ext, StringComparison.OrdinalIgnoreCase))
                    {
                        acceptedFiles.Add(file);
                        break;
                    }
            }

            callback.Title = String.Format(CultureInfo.InvariantCulture, "Importing {0} files...", acceptedFiles.Count);
            callback.SetRange(0, acceptedFiles.Count);

            int stackCount = 0, maxStack = _config.ProcessBatchSize;
            long stackSizeBytes = 0, maxStackSizeBytes = 1024 * 1024 * 20;  // If data size reaches the limit: Start processing file content
            var processRoutes = new List<TravelRoute>();
            var formatter = new ProtoBufTransfer(Logger);
            var processFiles = new List<string>();

            for (int i = 0; i < acceptedFiles.Count; i++)
            {
                if (callback.IsAborting)
                    return result;

                string file = acceptedFiles[i];
                string fileName = Path.GetFileName(file);
                string ext = Path.GetExtension(fileName);   // Get file extension in order to determine the data type

                Logger.InfoFormat("Importing file [{0}]", fileName);
                callback.Text = fileName;
                var fileSize = new FileInfo(file).Length;
                stackSizeBytes += fileSize;

                // Handle Binary data
                if (String.Equals(ext, BIN_EXTENSION, StringComparison.OrdinalIgnoreCase))
                {
                    var newRoutes = formatter.FromRaw<List<TravelRoute>>(file);
                    if (newRoutes == null)
                    {
                        var singleRoute = formatter.FromRaw<TravelRoute>(file);
                        if (singleRoute != null && singleRoute.Journeys.Count > 0)
                        {
                            processRoutes.Add(singleRoute);
                            stackCount += singleRoute.Journeys.Count;
                        }
                    }
                    else if (newRoutes.Count > 0)
                    {
                        foreach (var r in newRoutes)
                        {
                            if (r.Journeys.Count > 0)
                            {
                                processRoutes.AddRange(newRoutes);
                                stackCount += r.Journeys.Count;
                            }
                        }
                    }
                }
                else
                {
                    // Handle the old data using the Fare Provider
                    var newRoute = FareDataProvider.ReadData(File.ReadAllText(file, Encoding.Default));
                    if (newRoute != null && newRoute.Journeys.Count > 0)
                    {
                        var existRoute = processRoutes.FirstOrDefault(r => r.IsSameRoute(newRoute));
                        if (existRoute == null)
                            processRoutes.Add(newRoute);
                        else
                            existRoute.AddJourney(newRoute.Journeys, true); // Merge the journeys into the same Route

                        foreach (var j in newRoute.Journeys)
                            foreach (var d in j.Data)
                                if (d.DataDate.IsUndefined())
                                    d.DataDate = DateTime.Now;

                        stackCount += newRoute.Journeys.Count;
                        stackSizeBytes += 20 * fileSize;    // XML file needs much more resource for processing: Add "padding" to the file size boundary
                    }
                }

                processFiles.Add(file);

                if (stackCount >= maxStack || stackSizeBytes >= maxStackSizeBytes || i == acceptedFiles.Count - 1)
                {
                    FareDatabase.AddData(processRoutes);
                    result.AddRange(processRoutes);
                    if (options.ArchiveDataFiles)
                    {
                        callback.Text = String.Format("Archiving {0} data entries...", processFiles.Count);
                        ExportData(processRoutes, _config.ArchivePath, DataFormat.Binary);

                        foreach (var f in processFiles)
                            File.Delete(f);
                    }

                    processRoutes.Clear();
                    stackCount = 0;
                    stackSizeBytes = 0;
                }

                callback.Increment(1);
            }

            return result;
        }

        public string ExportData(IList<TravelRoute> data, string destinationPath, DataFormat format)
        {
            if (data == null || data.Count < 1)
                return null;

            Logger.InfoFormat("Export {0} travel route", data.Count);
            if (String.IsNullOrEmpty(destinationPath))
                destinationPath = ".\\";

            Directory.CreateDirectory(destinationPath);
            string targetFile = null;

            if (format == DataFormat.XML)
            {
                foreach (var route in data)
                {
                    targetFile = Path.Combine(destinationPath, GenerateFileName(route) + XML_EXTENSION);
                    using (var fs = File.Open(targetFile, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                    {
                        FareDataProvider.ExportData(fs, route);
                    }
                }
            }
            else
            {
                targetFile = Path.Combine(destinationPath, String.Format(CultureInfo.InvariantCulture, "Exp_{0}_{1}{2}", Guid.NewGuid(), data.Count, BIN_EXTENSION));
                var formatter = new ProtoBufTransfer(Logger);
                formatter.ToRaw(data, targetFile);
            }

            Logger.InfoFormat("{0} travel route was exported", data.Count);
            return targetFile;
        }

        public string ExportData(TravelRoute data, string destinationPath, DataFormat format)
        {
            return ExportData(new List<TravelRoute> { data }, destinationPath, format);
        }

        string GenerateFileName(TravelRoute route)
        {
            string origin = route.Departure.IATA;
            string dest = route.Destination.IATA;
            long minId = int.MaxValue, maxId = 0;

            foreach (var j in route.Journeys)
            {
                var id = j.Id;
                if (id > maxId)
                    maxId = id;
                if (id < minId)
                    minId = id;
            }

            var fileName = String.Format(CultureInfo.InvariantCulture, "{0}-{1}.[{2}]-[{3}]-({4}-{5}){6}({7})",
                                 FareDataProvider.ServiceName, route.Id, origin, dest,
                                 minId, maxId,
                                 DateTime.Now.ToString(NamingRule.DATE_FORMAT, CultureInfo.InvariantCulture),
                                 route.Journeys.Count);
            var processedName = PathUtil.RemoveInvalidFileNameChars(fileName);
            return processedName;
        }
    }
}
