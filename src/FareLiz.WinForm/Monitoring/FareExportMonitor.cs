﻿using log4net;
using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.WinForm.Data;
using System;
using System.Collections.Generic;
using System.IO;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    /// <summary>
    /// Monitor used for exporting fare
    /// </summary>
    internal class FareExportMonitor : FareRequestMonitor
    {
        private readonly FileArchiveManager _archiveManager;
        private readonly IFareDatabase _fareDatabase;
        private readonly string _dataPath;
        private readonly List<TravelRoute> _cachedRoutes = new List<TravelRoute>();
        private readonly Func<bool> _autoSync;
        private readonly ILog _logger;
        private readonly object _lockObj = new object();
        private Stream _lockStream;
        private const int CACHE_AMOUNT = 100;
        private static readonly string LOCK_EXT = ".{2559a1f2-21d7-11d4-bdaf-00c04f60b9f0}";

        internal FareExportMonitor(bool autoSync)
            : this(() => autoSync) { }

        internal FareExportMonitor(Func<bool> autoSync)
        {
            var env = GlobalContext.MonitorEnvironment;
            _archiveManager = new FileArchiveManager(env.FareDataProvider, env.FareDatabase, env.Logger);
            _archiveManager.Initialize();
            _fareDatabase = env.FareDatabase;
            _autoSync = autoSync;
            _logger = env.Logger;
            var guid = Guid.NewGuid().ToString();
            _dataPath = AppUtil.GetLocalDataPath("Temp") + "\\" + guid + LOCK_EXT;
            Directory.CreateDirectory(_dataPath);
            _lockStream = File.Open(Path.Combine(_dataPath, "_" + guid), FileMode.Create, FileAccess.ReadWrite, FileShare.None);

            RequestCompleted += FareExportMonitor_OnRequestCompleted;
        }

        public override void Dispose()
        {
            _logger.Debug("Disposing " + GetType());
            FinalizeData();
            base.Dispose();
        }

        private void ReleaseLock()
        {
            _logger.Debug("Releasing lock...");
            lock (_lockObj)
            {
                if (_lockStream == null)
                    return;

                if (_lockStream != null)
                {
                    _lockStream.Dispose();
                    _lockStream = null;
                }

                if (Directory.Exists(_dataPath))
                    Directory.Delete(_dataPath, true);
            }
        }

        private void SaveDataToDatabase()
        {
            IList<TravelRoute> newRoutes;
            lock (_lockObj)
            {
                _logger.Info("Trying to save data...");
                if (!Directory.Exists(_dataPath))
                {
                    _logger.DebugFormat("Data path [{0}] no longer exists... Exiting...", _dataPath);
                    return;
                }

                newRoutes = _archiveManager.ImportData(_dataPath, DataOptions.Default);
            }

            if (newRoutes == null || newRoutes.Count < 1)
                return;

            var syncDb = _fareDatabase as ISyncableDatabase;
            if (_autoSync() && syncDb != null)
            {
                var newPkgId = syncDb.SendData(newRoutes);
                syncDb.AddPackage(newPkgId, null);
            }
        }

        private void FareExportMonitor_OnRequestCompleted(FareRequestMonitor sender, FareBrowserRequestArg args)
        {
            var browser = args.Request.BrowserControl;
            if (browser == null)
            {
                _logger.Warn("There is no browser control associated with the request");
                return;
            }

            TravelRoute route = browser.LastRetrievedRoute;
            lock (_lockObj)
            {
                var pendingCount = sender.PendingRequests.Count;
                var processCount = sender.ProcessingRequests.Count;
                _logger.DebugFormat("There are {0} pending requests, {1} processing requests", pendingCount, processCount);
                bool isLastRequest = pendingCount == 0 && processCount == 0;
                if (browser.RequestState == DataRequestState.Ok && route != null && route.Journeys.Count > 0)
                {
                    _cachedRoutes.Add(route);   // Cache journeys

                    if (_cachedRoutes.Count >= CACHE_AMOUNT || isLastRequest) // Export journeys to file to reduce memory load
                    {
                        FlushCache();
                    }
                }

                if (isLastRequest)
                {
                    _logger.Debug("There is no more active request for the monitor");
                    FinalizeData();
                }
            }
        }

        private void FlushCache()
        {
            if (_cachedRoutes.Count == 0)
                _logger.Info("There is no cached journey data... Nothing to flush");
            else
            {
                // For now, the number of routes is also the number of journey data
                _logger.InfoFormat("There are {0} journey in cache... Exporting...", _cachedRoutes.Count);
                TravelRoute.Merge(_cachedRoutes);
                _archiveManager.ExportData(_cachedRoutes, _dataPath, DataFormat.Binary);
                _cachedRoutes.Clear();
            }
        }

        private void FinalizeData()
        {
            FlushCache();
            SaveDataToDatabase();
            ReleaseLock();
        }
    }
}
