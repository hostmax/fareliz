﻿using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.WinForm.Controls;
using System;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    /// <summary>
    /// A request to retrieve fare data using FareBrowserControl
    /// </summary>
    internal class FareBrowserRequest : FlightFareRequest
    {
        private FareRequestMonitor _ownerMonitor = null;
        internal FareRequestMonitor OwnerMonitor
        {
            get { return _ownerMonitor; }
            set
            {
                if (_ownerMonitor != null && _ownerMonitor != value)
                    throw new ArgumentException("Cannot assign the request to another monitor");
                _ownerMonitor = value;
            }
        }

        internal FareBrowserControl BrowserControl { get; private set; }

        internal FareBrowserRequest(FareBrowserRequest request) :
            this(request.Departure, request.Destination, request.DepartureDate, request.ReturnDate) { }

        internal FareBrowserRequest(Airport origin, Airport destination, DateTime departureDate, DateTime returnDate)
            : base(origin, destination, departureDate, returnDate)
        {
        }

        internal void Initialize()
        {
            if (BrowserControl == null)
                BrowserControl = new FareBrowserControl(GlobalContext.MonitorEnvironment.FareDataProvider);
        }

        internal void Start()
        {
            StartedDate = DateTime.Now;
            BrowserControl.RequestDataAsync(this);
        }

        internal void Stop()
        {
            if (BrowserControl != null)
                BrowserControl.Stop();
        }

        internal void Reset()
        {
            StartedDate = DateTime.MinValue;
            if (BrowserControl != null)
                BrowserControl.Reset();
        }
    }
}
