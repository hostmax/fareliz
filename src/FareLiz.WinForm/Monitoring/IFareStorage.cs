﻿using SkyDean.FareLiz.Core.Data;
using System.Collections.Generic;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    interface IFareStorage
    {
        List<StorageRoute> GetRoutes();
        List<DatePeriod> GetTravelDates(StorageRoute route);
        List<DatePeriod> GetDataDates(StorageRoute route, DatePeriod travelDate);
        void SaveLiveFare(IEnumerable<Flight> flights);
    }
}
