﻿using System;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    /// <summary>
    /// This class is a streamlined version for Route class, which is used for the sole purpose of storing live fare data metadata
    /// </summary>
    internal sealed class StorageRoute
    {
        internal string Departure { get; set; }
        internal string Destination { get; set; }

        internal StorageRoute(string origin, string destination)
        {
            Departure = origin;
            Destination = destination;
        }

        public override string ToString()
        {
            return Departure + " - " + Destination;
        }

        public override bool Equals(object obj)
        {
            var other = obj as StorageRoute;
            if (other == null)
                return false;

            return String.Equals(Departure, other.Departure, StringComparison.OrdinalIgnoreCase)
                && String.Equals(Destination, other.Destination, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }

    /// <summary>
    /// Contains StorageRoute object with an ID
    /// </summary>
    internal class RouteInfo
    {
        internal int Id { get; set; }
        internal StorageRoute Route { get; set; }

        internal RouteInfo(int id, StorageRoute route)
        {
            Id = id;
            Route = route;
        }

        public override string ToString()
        {
            if (Route != null)
                return Route.Departure + " - " + Route.Destination;
            return base.ToString();
        }
    }
}
