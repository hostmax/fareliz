﻿using log4net;
using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.WinForm.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    internal enum MonitorState { Stopped, Stopping, Running }

    /// <summary>
    /// Monitor for fare browser requests
    /// </summary>
    internal class FareRequestMonitor : IDisposable
    {
        private BackgroundWorker _backgroundWorker = null;
        private readonly ILog _logger;
        private readonly DataQueue<FareBrowserRequest> _pendingQueue = new DataQueue<FareBrowserRequest>();
        private readonly DataQueue<FareBrowserRequest> _processQueue = new DataQueue<FareBrowserRequest>();
        private readonly object _lockObj = new object();

        internal int BatchProcessSize
        {
            get
            {
                var result = GlobalContext.MonitorEnvironment.FareDataProvider.SimultaneousRequests;
                return result > 0 ? result : 1;
            }
        }

        internal MonitorState State { get; private set; }
        internal MonitorEnvironment MonitorEnvironment { get; private set; }
        internal TimeSpan RequestTimeout { get; set; }

        internal DataQueue<FareBrowserRequest> PendingRequests { get { return _pendingQueue; } }
        internal DataQueue<FareBrowserRequest> ProcessingRequests { get { return _processQueue; } }

        internal event FareRequestHandler RequestStarting;
        internal event FareRequestHandler RequestStopping;
        internal event FareRequestHandler RequestCompleted;
        internal event FareMonitorHandler MonitorStarting;
        internal event FareMonitorHandler MonitorStopping;

        internal FareRequestMonitor()
        {
            RequestTimeout = TimeSpan.FromMinutes(5);
            _logger = GlobalContext.Logger;
        }

        internal void Enqueue(Airport origin, Airport destination, DateTime departureDate, DateTime returnDate)
        {
            var newRequest = new FareBrowserRequest(origin, destination, departureDate, returnDate);
            Enqueue(newRequest);
        }

        internal void Enqueue(IEnumerable<FareBrowserRequest> requests)
        {
            foreach (var r in requests)
            {
                Enqueue(r);
            }
        }

        internal void Enqueue(FareBrowserRequest request)
        {
            request.OwnerMonitor = this;
            _pendingQueue.Enqueue(request);
        }

        internal void Clear()
        {
            _pendingQueue.Clear(true);
            _processQueue.Clear(true);
        }

        internal void Start()
        {
            lock (_lockObj)
            {
                GlobalContext.Logger.DebugFormat("Start {0}...", GetType());
                DateTime startDate = DateTime.Now;

                // If monitor is still being stopped: Wait for it within a timeout limit
                while (State == MonitorState.Stopping && (DateTime.Now - startDate) < RequestTimeout)
                    Thread.Sleep(500);

                if (State != MonitorState.Stopped)  // If the monitor is still not stopped: Throw exception
                    throw new TimeoutException("Could not restart monitor. Timed out waiting for the monitor to stop");

                if (MonitorStarting != null)
                    MonitorStarting(this);

                State = MonitorState.Running;
                if (_backgroundWorker == null)
                {
                    _backgroundWorker = new BackgroundWorker();
                    _backgroundWorker.DoWork += backgroundWorker_DoWork;
                    _backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
                }

                // If we can reach here: The background worker should not be doing anything on background
                if (!_backgroundWorker.IsBusy)
                    _backgroundWorker.RunWorkerAsync();
                GlobalContext.Logger.DebugFormat("{0} started", GetType());
            }
        }

        internal virtual void Stop()
        {
            lock (_lockObj)
            {
                GlobalContext.Logger.DebugFormat("Stopping {0}...", GetType());
                State = (_backgroundWorker.IsBusy ? MonitorState.Stopping : MonitorState.Stopped);
                if (_pendingQueue.Count > 0)
                {
                    _pendingQueue.ToList().ForEach(Stop);
                    _pendingQueue.Clear(true);
                }

                if (_processQueue.Count > 0)
                {
                    _processQueue.ToList().ForEach(Stop);
                    _processQueue.Clear(true);
                }
                GlobalContext.Logger.DebugFormat("{0} stopped", GetType());
            }
        }

        internal void Stop(FareBrowserRequest request)
        {
            _logger.Info("Stopping " + GetType().Name);
            if (RequestStopping != null)
            {
                FareBrowserRequestArg args = new FareBrowserRequestArg(request);
                OnRequestStopping(args);
                if (args.Cancel)
                    return;
            }

            request.Stop();
        }

        public virtual void Dispose()
        {
            Stop();
            Clear();
        }

        protected virtual void OnRequestStarting(FareBrowserRequestArg args)
        {
            _logger.Debug("Starting request for journey: " + args.Request.ShortDetail);
            if (RequestStarting != null)
            {
                RequestStarting(this, args);
            }

            var request = args.Request;
            if (request.BrowserControl == null)
            {
                request.Initialize();
            }
            else
            {
                if (request.BrowserControl.RequestState == DataRequestState.Pending)
                {
                    // Assign the event handler for completed request when the request is starting
                    JourneyBrowserDelegate _requestCompletedHandler = null;
                    _requestCompletedHandler = (b, j) =>
                        {
                            try
                            {
                                if (State != MonitorState.Running) return;

                                lock (_lockObj)
                                {
                                    OnRequestCompleted(new FareBrowserRequestArg(request));
                                    FinalizeRequest(request);
                                }
                            }
                            finally
                            {
                                request.BrowserControl.GetJourneyCompleted -= _requestCompletedHandler;
                            }
                        };
                    request.BrowserControl.GetJourneyCompleted += _requestCompletedHandler;
                }
            }
        }

        protected virtual void OnRequestStopping(FareBrowserRequestArg args)
        {
            _logger.Debug("Stopping request: " + args.Request.ShortDetail);
            if (RequestStopping != null)
            {
                RequestStopping(this, args);
            }

            FinalizeRequest(args.Request);
        }

        protected virtual void OnRequestCompleted(FareBrowserRequestArg args)
        {
            _logger.Info("Fare request completed: " + args.Request.ShortDetail);
            if (args.Request.BrowserControl != null
                && args.Request.BrowserControl.RequestState == DataRequestState.NoData)
            {
                _logger.Info("No data was found!");
            }

            if (RequestCompleted != null)
            {
                RequestCompleted(this, args);
            }
        }

        internal void FinalizeRequest(FareBrowserRequest request)
        {
            _processQueue.Remove(request);
            if (_pendingQueue.Count == 0 && _processQueue.Count == 0)
            {
                Stop();
            }
        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            AppUtil.NameCurrentThread(GetType().Name);
            Thread.CurrentThread.Priority = ThreadPriority.Lowest;
            while (State == MonitorState.Running)
            {
                int canProcessCount = BatchProcessSize - _processQueue.Count;
                if (canProcessCount > _pendingQueue.Count)
                    canProcessCount = _pendingQueue.Count;

                if (canProcessCount > 0)
                {
                    Console.WriteLine("Starting " + canProcessCount + " requests...");
                    var itemList = _pendingQueue.Dequeue(canProcessCount);
                    foreach (FareBrowserRequest request in itemList)
                    {
                        var args = new FareBrowserRequestArg(request);
                        OnRequestStarting(args);
                        if (args.Cancel)
                            continue;

                        request.Start();    // Start the request and move it to the appropriate queue
                        _pendingQueue.Remove(request);
                        _processQueue.Enqueue(request);
                    }
                }

                Thread.Sleep(500);
            }
        }

        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (MonitorStopping != null)
                MonitorStopping(this);
            bool noRunningReq = (_pendingQueue.Count == 0 && _processQueue.Count == 0);
            State = (noRunningReq ? MonitorState.Stopped : MonitorState.Stopping);
        }
    }

    internal delegate void FareMonitorHandler(FareRequestMonitor sender);
    internal delegate void FareRequestHandler(FareRequestMonitor sender, FareBrowserRequestArg args);
    internal class FareBrowserRequestArg : CancelEventArgs
    {
        internal readonly FareBrowserRequest Request;
        internal readonly DateTime RequestInitiatedDate;

        internal FareBrowserRequestArg(FareBrowserRequest request)
        {
            Request = request;
            if (request != null && request.BrowserControl != null)
                RequestInitiatedDate = request.BrowserControl.LastRequestInitiatedDate;
        }

        internal static readonly new FareBrowserRequestArg Empty = new FareBrowserRequestArg(null);
    }
}
