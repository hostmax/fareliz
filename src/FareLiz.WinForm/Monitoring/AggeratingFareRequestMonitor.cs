﻿using System;
using SkyDean.FareLiz.Core.Utils;

namespace SkyDean.FareLiz.WinForm.Monitoring
{
    /// <summary>
    /// Aggerating monitor which keeps track of multiple monitors
    /// </summary>
    internal class AggeratingFareRequestMonitor : IDisposable
    {
        private readonly DataQueue<FareRequestMonitor> _startedMonitors = new DataQueue<FareRequestMonitor>();
        private readonly object _syncObj = new object();

        public void Stop()
        {
            var startedMons = _startedMonitors.ToList();
            foreach (var mon in startedMons)
                mon.Stop();
        }

        internal void Start(FareRequestMonitor monitor)
        {
            monitor.Start();
            _startedMonitors.Enqueue(monitor);
        }

        internal void Clear()
        {
            Stop();
            _startedMonitors.Clear(true);
        }

        internal void Clear(Type monitorType)
        {
            if (_startedMonitors.Count < 1)
                return;

            var allMons = _startedMonitors.ToList();
            foreach (var mon in allMons)
            {
                if (mon.GetType() == monitorType)
                {
                    mon.Stop();
                    _startedMonitors.Remove(mon);
                    mon.Dispose();
                }
            }
        }

        public void Dispose()
        {
            lock (_syncObj)
            {
                Stop();
                Clear();
            }
        }
    }
}
