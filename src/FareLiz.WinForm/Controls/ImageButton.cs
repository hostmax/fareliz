﻿using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public class ImageButton : Button
    {
        [Category("Appearance"), DefaultValue(true)]
        public bool AutoAlign { get { return _autoAlign; } set { _autoAlign = value; } }
        private bool _autoAlign = true;
        private bool _isAligning = false;

        public new Image Image
        {
            get { return base.Image; }
            set
            {
                base.Image = value;
                if (AutoAlign)
                    AutoPadding();
            }
        }

        public new string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                if (AutoAlign)
                    AutoPadding();
            }
        }

        public new Padding Padding
        {
            get { return base.Padding; }
            set
            {
                base.Padding = value;
                if (AutoAlign)
                    AutoPadding();
            }
        }

        public new Size Size
        {
            get { return base.Size; }
            set
            {
                base.Size = value;
                if (AutoAlign)
                    AutoPadding();
            }
        }

        public override ContentAlignment TextAlign
        {
            get { return (AutoAlign ? ContentAlignment.MiddleRight : base.TextAlign); }
            set { base.TextAlign = (AutoAlign ? ContentAlignment.MiddleRight : value); }
        }

        public void AutoPadding()
        {
            if (!_isAligning && Image != null)
            {
                _isAligning = true;
                var imgWidth = Image.Width + Padding.Left + 6;
                var txtAreaSize = new Size(Width - imgWidth, Height - 6);
                var txtSize = TextRenderer.MeasureText(base.Text, Font, txtAreaSize);
                if (txtSize.Width > txtAreaSize.Width)
                    txtSize.Width = txtAreaSize.Width;

                base.Padding = new Padding(Padding.Left, Padding.Top, (txtAreaSize.Width - txtSize.Width) / 2, Padding.Bottom);
                _isAligning = false;
            }
        }
    }
}
