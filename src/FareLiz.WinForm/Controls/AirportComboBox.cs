﻿using SkyDean.FareLiz.Core.Data;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal class AirportComboBox : ComboBox
    {
        private int _lastSelectedIndex = -1;
        private object _lastSelectedItem = null;
        private bool _suppressSelectedIndexChange = false;
        private bool _suppressSelectedItemChange = false;

        public new object DataSource
        {
            get { return base.DataSource; }
            set
            {
                SetDataSource(value);
                MeasureDimensions();
            }
        }

        public Airport SelectedAirport
        {
            get { return SelectedItem as Airport; }
            set { SelectedItem = value; }
        }

        public string SelectedAirportCode
        {
            get { return SelectedValue == null ? null : SelectedValue.ToString(); }
            set
            {
                if (value == null)
                    SelectedItem = null;
                else
                    SelectedValue = value;
            }
        }

        public AirportComboBox()
        {
            DrawMode = DrawMode.OwnerDrawVariable;
            MaxDropDownItems = 10;            
            ValueMember = "IATA";
            DisplayMember = "";
            DrawItem += OnDrawItem;
            MeasureItem += OnMeasureItem;
            LostFocus += OnLostFocus;
        }

        private void SetDataSource(object dataSource)
        {
            _suppressSelectedIndexChange = _suppressSelectedItemChange = true;
            bool triggerSelItemChange = true, triggerSelIndexChange = true;
            try
            {
                var prevSelIndex = _lastSelectedIndex;
                var prevSelItem = _lastSelectedItem;
                var prevText = Text;
                base.DataSource = dataSource;
                foreach (var item in Items)
                {
                    var itemText = GetItemText(item);
                    if (prevText == itemText)
                    {
                        SelectedItem = item;
                        break;
                    }
                }

                triggerSelIndexChange = (prevSelIndex != SelectedIndex);
                triggerSelItemChange = (prevSelItem != SelectedItem);
            }
            finally
            {
                _suppressSelectedIndexChange = _suppressSelectedItemChange = false;
                if (triggerSelItemChange)
                    OnSelectedItemChanged(EventArgs.Empty);
                if (triggerSelIndexChange)
                    OnSelectedIndexChanged(EventArgs.Empty);
            }
        }

        public int DefaultItemHeight
        {
            get { return 2 * Font.Height + 10; }
        }

        protected override void OnClick(EventArgs e)
        {
            if (!DroppedDown)
                DroppedDown = true;
            base.OnClick(e);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (!_suppressSelectedIndexChange)
                base.OnSelectedIndexChanged(e);

            _lastSelectedIndex = SelectedIndex;
        }

        protected override void OnSelectedItemChanged(EventArgs e)
        {
            if (!_suppressSelectedItemChange)
                base.OnSelectedItemChanged(e);

            _lastSelectedItem = SelectedItem;
        }

        private void OnLostFocus(object sender, EventArgs eventArgs)
        {
            Select(0, 0);
            if (Items.Count < 1)
                return;

            if (SelectedIndex > -1)
                base.Text = SelectedItem.ToString(); // set the Text to the selected item
            else
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    var item = Items[i].ToString();
                    if (Text == item)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }

                if (SelectedIndex < 0)
                {
                    SelectedItem = _lastSelectedItem;
                    if (SelectedIndex < 0)
                    {
                        base.Text = Items[0].ToString();
                        SelectedIndex = 0;
                    }
                }
            }
        }

        internal void MeasureDimensions()
        {
            int visibleItems = (MaxDropDownItems > Items.Count ? Items.Count : MaxDropDownItems);

            if (visibleItems == 0)
                DropDownHeight = 1;
            else
                DropDownHeight = visibleItems * (DefaultItemHeight + 2 * SystemInformation.BorderSize.Height);
        }

        private void OnMeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = DefaultItemHeight;
        }

        private void OnDrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            var bounds = e.Bounds;
            string text = Items[e.Index].ToString();
            bool isHighlighted = (e.State & DrawItemState.Focus) == DrawItemState.Focus;
            using (var txtBrush = new SolidBrush(isHighlighted ? SystemColors.HighlightText : e.ForeColor))
            {
                var textSize = TextRenderer.MeasureText(text, e.Font, new Size(bounds.Width, int.MaxValue), TextFormatFlags.WordBreak);
                int y = bounds.Y + (bounds.Height - textSize.Height) / 2;
                e.Graphics.DrawString(text, e.Font, txtBrush, new Rectangle(2, y, bounds.Width, textSize.Height), StringFormat.GenericDefault);
            }

            e.Graphics.DrawLine(Pens.Gainsboro, 0, bounds.Y, bounds.X + bounds.Width, bounds.Y);

            e.DrawFocusRectangle();
        }
    }
}
