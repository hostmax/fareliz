using SkyDean.FareLiz.Core.Utils.WinNative;
using SkyDean.FareLiz.WinForm.Properties;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Presentation
{
    public enum NotificationType { Info, Success, Warning, Error }
    public partial class TaskbarNotifierBase
    {
        const int CORNER_RADIUS = 15;
        private void ApplyTheme(NotificationType type)
        {
            switch (type)
            {
                case NotificationType.Info:
                    SetThemeColor(Color.FromArgb(74, 114, 237));
                    imgIcon.Image = Resources.NotifierInfo;
                    break;
                case NotificationType.Success:
                    SetThemeColor(Color.FromArgb(0x7F, 0xBA, 0));
                    imgIcon.Image = Resources.NotifierSuccess;
                    break;
                case NotificationType.Warning:
                    SetThemeColor(Color.FromArgb(201, 189, 0));
                    imgIcon.Image = Resources.NotifierWarning;
                    break;
                case NotificationType.Error:
                    imgIcon.Image = Resources.NotifierError;
                    SetThemeColor(Color.FromArgb(237, 95, 74));
                    break;
            }
            this.Region = Region.FromHrgn(NativeMethods.CreateRoundRectRgn(0, 0, Width, Height, CORNER_RADIUS, CORNER_RADIUS));
        }

        private void SetThemeColor(Color targetColor)
        {
            this.BackColor = targetColor;
        }
    }

    public static class NotificationTypeExtensions
    {
        public static NotificationType ConvertToNotificationType(this ToolTipIcon iconType)
        {
            switch (iconType)
            {
                case ToolTipIcon.Warning:
                    return NotificationType.Warning;
                case ToolTipIcon.Error:
                    return NotificationType.Error;
                default:
                    return NotificationType.Info;
            }
        }

        public static ToolTipIcon ConvertToTooltipIcon(this NotificationType type)
        {
            switch (type)
            {
                case NotificationType.Warning:
                    return ToolTipIcon.Warning;
                case NotificationType.Error:
                    return ToolTipIcon.Error;
                default:
                    return ToolTipIcon.Info;
            }
        }
    }
}
