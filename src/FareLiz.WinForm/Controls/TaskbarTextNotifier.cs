using System;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.Core.Presentation
{
    /// <summary>
    /// TaskbarNotifier allows to display styled popups
    /// </summary>
    public sealed partial class TaskbarTextNotifier : TaskbarNotifierBase
    {
        public Font ContentFont
        {
            get { return txtContent.Font; }
            set { txtContent.Font = value; }
        }

        protected override void InitializeContentPanel(Panel contentPanel)
        {
            InitializeComponent();
            contentPanel.Controls.Add(txtContent);
        }

        protected override Size MeasureContentSize(int maxWidth, int maxHeight)
        {
            var result = new Size(maxWidth, maxHeight);
            result = TextRenderer.MeasureText(txtContent.Text, txtContent.Font, result, TextFormatFlags.LeftAndRightPadding);
            bool ver = result.Height > maxHeight;
            bool hor = result.Width > maxWidth;

            RichTextBoxScrollBars scroll = (hor && ver)
               ? RichTextBoxScrollBars.Both
               : (hor
                       ? RichTextBoxScrollBars.Horizontal
                       : (ver
                           ? RichTextBoxScrollBars.Vertical
                           : RichTextBoxScrollBars.None));

            txtContent.ScrollBars = scroll;

            return result;
        }

        /// <summary>
        /// Displays the popup for a certain amount of time
        /// </summary>
        /// <param name="title">Popup title</param>
        /// <param name="content">Popup content</param>
        /// <param name="timeToStay">Time to stay in milliseconds</param>
        /// <param name="type">Notification type</param>
        /// <param name="isRTF">Is the content Rich Text Format</param>
        public void Show(string title, string content, int timeToStay, NotificationType type, bool isRTF)
        {
            if (InvokeRequired)
                this.SafeInvoke(new Action(() => Show(title, content, timeToStay, type, isRTF)));
            else
            {
                SetTitle(title);
                if (isRTF)
                    txtContent.Rtf = content;
                else
                    txtContent.Text = content;
                Display(timeToStay, type);
            }
        }

        public void Show(string title, string content, int timeToStay, NotificationType type)
        {
            Show(title, content, timeToStay, type, false);
        }
    }
}
