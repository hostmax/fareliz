﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace SkyDean.FareLiz.WinForm.Controls
{
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip | ToolStripItemDesignerAvailability.StatusStrip)]
    internal class ToolStripControlAgent : ToolStripControlHost
    {
        public ToolStripControlAgent()
            : base(null)
        {
        }

        public ToolStripControlAgent(Control c) : base(c)
        {
        }
    }

    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip | ToolStripItemDesignerAvailability.StatusStrip)]
    internal class ToolStripControl<T> : ToolStripControlAgent where T : Control
    {
        public ToolStripControl()
            : base(Activator.CreateInstance<T>())
        {
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public T ControlItem
        {
            get { return (T)this.Control; }
        }
    }
}
