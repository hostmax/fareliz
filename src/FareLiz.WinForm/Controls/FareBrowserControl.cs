﻿using log4net;
using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal sealed class FareBrowserControl : Control
    {
        private readonly object _syncObject = new object();
        private readonly FlightDataListView lvFlightData;

        internal bool IsStopping { get; private set; }

        internal int TimeoutInSeconds { get { return DataHandler == null ? 0 : DataHandler.TimeoutInSeconds; } }
        internal IFareDataProvider DataHandler { get; private set; }
        internal DateTime LastDataDate { get; private set; }
        internal DateTime LastRequestInitiatedDate { get; private set; }
        internal DateTime LastRequestStartedDate { get; private set; }
        internal Exception LastException { get; private set; }
        internal ILog Logger { get { return GlobalContext.MonitorEnvironment.Logger; } }

        private TravelRoute _lastRetrievedRoute;
        internal TravelRoute LastRetrievedRoute { get { return _lastRetrievedRoute; } }

        private DataRequestState _requestState = DataRequestState.Pending;
        internal DataRequestState RequestState
        {
            get { return _requestState; }
            private set { _requestState = value; }
        }

        internal event JourneyBrowserDelegate GetJourneyCompleted;

        private FareBrowserControl() : this(null) { }

        internal FareBrowserControl(IFareDataProvider fareDataProvider)
        {
            DataHandler = fareDataProvider;
            lvFlightData = new FlightDataListView { Dock = DockStyle.Fill };
            lvFlightData.SetWatermark("Idle");
            Controls.Add(lvFlightData);
        }

        internal TravelRoute RequestDataAsync(FlightFareRequest request)
        {
            ThreadPool.QueueUserWorkItem(o =>
                {
                    AppUtil.NameCurrentThread(GetType().Name + "-RequestDataAsync");
                    DoRequestData(request);
                });

            return LastRetrievedRoute;
        }

        private void DoRequestData(FlightFareRequest request)
        {
            if (!Monitor.TryEnter(_syncObject))
                return;

            try
            {
                IsStopping = false;
                if (this.IsDestructed() || IsStopping)
                    return;

                // First, reset all data
                Reset();

                lvFlightData.SafeInvoke(new Action(() =>
                    {
                        lvFlightData.Name = StringUtil.GetPeriodString(request.DepartureDate, request.ReturnDate);
                        lvFlightData.SetWatermark("Requesting data... Please wait...");
                    }));

                // Create a thread to get the data
                DataRequestResult dataRequestResult = DataRequestResult.Empty;
                var workResult = WorkerThread.DoWork(() =>
                    {
                        lvFlightData.SetWatermark("Data request was started on " + DateTime.Now + Environment.NewLine + "Please wait while the application retrieves fare data...");
                        RequestState = DataRequestState.Requested;
                        LastRequestStartedDate = DateTime.Now;
                        dataRequestResult = DataHandler.QueryData(request, OnProgressChanged);
                    }, TimeoutInSeconds);

                if (workResult.Succeeded)
                    RequestState = (dataRequestResult.ResultRoute == null ? DataRequestState.NoData : DataRequestState.Ok);
                else
                {
                    RequestState = DataRequestState.Failed;
                    if (workResult.IsTimedout)
                    {
                        string err = "Request timed out after " + TimeoutInSeconds + "s";
                        LastException = new TimeoutException(err);
                        Logger.ErrorFormat(err);
                    }
                    else if (workResult.Exception != null)
                    {
                        LastException = workResult.Exception;
                        Logger.Error("Failed to request journey data: " + workResult.Exception.Message);
                    }
                }

                RequestState = dataRequestResult.RequestState;
                _lastRetrievedRoute = dataRequestResult.ResultRoute;
                if (RequestState > DataRequestState.Requested)
                {
                    LastDataDate = DateTime.Now;
                    if (_lastRetrievedRoute != null && _lastRetrievedRoute.Journeys.Count > 0)
                    {
                        foreach (var j in _lastRetrievedRoute.Journeys)
                            foreach (var d in j.Data)
                                if (d.DataDate.IsUndefined())
                                    d.DataDate = LastDataDate;
                    }
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                RequestState = DataRequestState.Failed;
                Logger.Error("Failed to request data: " + ex);
            }
            finally
            {
                Monitor.Exit(_syncObject);  // Release the lock at the end
                OnGetJourneyCompleted(new JourneyEventArgs(LastRetrievedRoute, RequestState, LastRequestInitiatedDate));
                IsStopping = false;
            }
        }

        internal void Reset()
        {
            LastDataDate = LastRequestStartedDate = DateTime.MinValue;
            _lastRetrievedRoute = null;
            RequestState = DataRequestState.Pending;
        }

        internal void Stop()
        {
            IsStopping = true;
        }

        private void OnProgressChanged(object sender, JourneyProgressChangedEventArgs e)
        {
            if (e.ResultRoute != null)
                lvFlightData.SafeInvoke(new Action(() =>
                {
                    BindData(e.ResultRoute);
                    lvFlightData.Enabled = e.ProgressPercentage < 100;
                }));
        }

        private void OnGetJourneyCompleted(JourneyEventArgs e)
        {
            if (this.IsDestructed() || LastRequestInitiatedDate != e.RequestInitiatedDate)
                return;

            BindData(e.ResultRoute);
            lvFlightData.Enabled = true;

            if (!IsStopping && GetJourneyCompleted != null)
                GetJourneyCompleted(this, e);
        }

        private void BindData(TravelRoute route)
        {
            if (route != null && route.Journeys.Count > 0 && route.Journeys[0].Data.Count > 0)
            {
                DateTime dataDate = DateTime.Now;
                var flights = new List<Flight>();
                var jData = route.Journeys[0].Data;
                foreach (var d in jData)
                {
                    d.DataDate = dataDate;
                    if (d.Flights != null && d.Flights.Count > 0)
                        flights.AddRange(d.Flights);
                }

                lvFlightData.SetDataSourceAsync(flights, true);
                lvFlightData.SetWatermark(null);
            }
            else if (RequestState == DataRequestState.NoData)
                lvFlightData.SetWatermark("There is no data for selected journey. Probably there is no flight on that date or there is no flight between two destinations!");
            else if (LastException != null)
                lvFlightData.SetWatermark("Failed to retrieve data: " + LastException.Message);
            else
                lvFlightData.SetWatermark("The request was not properly handled or aborted");
        }
    }

    internal delegate void JourneyBrowserDelegate(FareBrowserControl sender, JourneyEventArgs e);
    internal class JourneyEventArgs : EventArgs
    {
        internal TravelRoute ResultRoute { get; private set; }
        internal DataRequestState RequestState { get; private set; }
        internal DateTime RequestInitiatedDate { get; private set; }

        internal JourneyEventArgs(TravelRoute route, DataRequestState requestState, DateTime initiatedDate)
        {
            ResultRoute = route;
            RequestState = requestState;
            RequestInitiatedDate = initiatedDate;
        }

        internal static readonly new JourneyEventArgs Empty = new JourneyEventArgs(null, DataRequestState.Pending, DateTime.MinValue);
    }
}

