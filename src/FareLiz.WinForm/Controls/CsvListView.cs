﻿using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal enum CsvDataType { CsvFile, CsvString }
    internal class CsvListView : EnhancedListView
    {
        private readonly List<string[]> _dataLines = new List<string[]>();
        private readonly List<string[]> _filteredLines = new List<string[]>();
        private readonly CsvListViewColumnSorter _sorter = new CsvListViewColumnSorter();

        public CsvListView()
        {
            ListViewItemSorter = _sorter;
            VirtualMode = true;
            RetrieveVirtualItem += CsvListView_RetrieveVirtualItem;
            VirtualModeFilter = CsvListViewVirtualModeFilter;
            VirtualModeSort = CsvListVIewVirtualModeSort;
            View = View.Details;
        }

        private void CsvListVIewVirtualModeSort(EnhancedListView listView)
        {
            _dataLines.Sort(_sorter);
        }

        private void CsvListViewVirtualModeFilter(EnhancedListView listView, List<ListViewFilter> filters)
        {
            if (DoFilter<string[]>(_dataLines, _filteredLines, filters, FilterLine))
            {
                VirtualListSize = _dataLines.Count;
            }
        }

        private bool FilterLine(string[] data, ListViewFilter filter)
        {
            int index = filter.Column;
            return data[index].IsMatch(filter.FilterString);
        }

        public void BindData(string data, CsvDataType type)
        {
            if (String.IsNullOrEmpty(data) || (type == CsvDataType.CsvFile && !File.Exists(data)))
                throw new ArgumentException("Invalid input data for CsvListView. Check that the data is not empty or the file exists");

            Clear();
            _dataLines.Clear();
            _filteredLines.Clear();
            VirtualListSize = 0;

            TextReader stream = null;
            if (type == CsvDataType.CsvFile)
                stream = new StreamReader(data);
            else
                stream = new StringReader(data);

            using (stream)
            {
                using (var reader = new CsvReader(stream, true) { DefaultParseErrorAction = ParseErrorAction.AdvanceToNextLine })
                {
                    var headers = reader.GetFieldHeaders();
                    int fieldCount = headers.Length;
                    var colWidth = ClientSize.Width / fieldCount;

                    foreach (var header in headers)
                    {
                        var newCol = Columns.Add(header);
                        newCol.Width = colWidth;
                    }

                    while (reader.ReadNextRecord())
                    {
                        var item = new string[fieldCount];
                        for (int i = 0; i < fieldCount; i++)
                            item[i] = reader[i];
                        _dataLines.Add(item);
                    }
                }
            }

            VirtualListSize = _dataLines.Count;
            Invalidate();
        }

        private void CsvListView_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            var itemIdx = e.ItemIndex;
            e.Item = new ListViewItem(_dataLines[itemIdx]);
        }

        private class CsvListViewColumnSorter : ListViewTextColumnSorter, IComparer<string[]>
        {
            public int Compare(string[] x, string[] y)
            {
                string xText = x[SortInfo.SortColumn],
                       yText = y[SortInfo.SortColumn];
                int result = StringLogicalComparer.Compare(xText, yText);
                return SortInfo.SortAscending ? result : -result;
            }
        }
    }
}
