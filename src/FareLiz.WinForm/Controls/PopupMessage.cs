﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public class PopupMessage : ToolTip
    {
        private Font _font = SystemFonts.MessageBoxFont;
        public Font Font { get { return _font; } set { _font = value; } }

        private Padding _padding = new Padding(5);
        public Padding Padding { get { return _padding; } set { _padding = value; } }

        public PopupMessage()
        {
            OwnerDraw = true;
            BackColor = Color.DimGray;
            ForeColor = Color.White;
            Popup += new PopupEventHandler(this.OnPopup);
            Draw += new DrawToolTipEventHandler(this.OnDraw);
        }

        public void Show(string text, Control target, Point arrowLocation)
        {
            if (target == null)
                throw new ArgumentException("Target control cannot be null");

            var activeScreen = Screen.FromControl(target).WorkingArea;

            var contentSize = GetSize(text);
            var abLoc = target.PointToScreen(arrowLocation);
            abLoc.X -= contentSize.Width;
            abLoc.Y -= contentSize.Height / 2;

            int diffToScreenBottom = activeScreen.Bottom - (abLoc.Y + contentSize.Height);
            if (diffToScreenBottom < 0)
                abLoc.Y += diffToScreenBottom;

            var ttLoc = target.PointToClient(abLoc);
            base.Show(text, target, ttLoc);
        }

        private int ArrowWidth { get { return (int)(0.8 * Font.Height); } }

        private void OnPopup(object sender, PopupEventArgs e)
        {
            var text = GetToolTip(e.AssociatedControl);
            var ttSize = GetSize(text);
            e.ToolTipSize = ttSize;
        }

        /// <summary>
        /// Get the total size of the popup message (including the arrow)
        /// </summary>
        private Size GetSize(string text)
        {
            var txtSize = TextRenderer.MeasureText(text, Font, Size.Empty, TextFormatFlags.Internal);
            var ttSize = new Size(txtSize.Width + Padding.Left + Padding.Right + 10, txtSize.Height + Padding.Top + Padding.Bottom);
            return ttSize;
        }

        private void OnDraw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBorder();
            e.DrawBackground();
            using (var brush = new SolidBrush(ForeColor))
            {
                e.Graphics.DrawString(e.ToolTipText, Font, brush, e.Bounds.X + Padding.Left, e.Bounds.Y + Padding.Top);
            }
        }
    }
}
