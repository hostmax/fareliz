﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal partial class AutoCompleteTextbox<T> : TextBox
    {
        // string to remember a former input
        private bool _suppressAutoSuggest = false, _suppressSelIndexChanged = false;
        private object _prevSelItem = null;
        private int _mouseIndex = -1;
        private int _prevSelIndex = -1;
        public event EventHandler SelectedItemChanged;

        #region Properties
        // the list for our suggestions database
        private IList<T> _autoCompletedList;
        public virtual IList<T> AutoCompleteList
        {
            get { return _autoCompletedList; }
            set { _autoCompletedList = value; }
        }

        public bool AlwaysShowSuggest { get; set; }

        // case sensitivity
        private bool _caseSensitive = false;
        public bool CaseSensitive { get { return _caseSensitive; } set { _caseSensitive = value; } }

        // minimum characters to be typed before suggestions are displayed
        private int _minTypedCharacters = 0;
        public int MinTypedCharacters
        {
            get { return _minTypedCharacters; }
            set { _minTypedCharacters = value > 0 ? value : 1; }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public int SelectedIndex
        {
            get { return listBox.SelectedIndex; }
            set { listBox.SelectedIndex = value; }
        }

        public new string Text
        {
            get { return base.Text; }
            set
            {
                _suppressAutoSuggest = true;
                base.Text = value;
                listBox.Items.Clear();
                SelectItem();
                _suppressAutoSuggest = false;
            }
        }

        private int _visibleItems = 10;
        public int VisibleSuggestItems { get { return _visibleItems; } set { _visibleItems = value > 3 ? value : 3; } }

        // the actual list of currently displayed suggestions
        private ListBox.ObjectCollection CurrentAutoCompleteList;

        // the parent Form of this control
        private Form ParentForm { get { return Parent.FindForm(); } }
        #endregion Properties

        // the constructor
        public AutoCompleteTextbox()
            : base()
        {
            InitializeComponent();
        }

        #region Methods
        public void HideSuggestionListBox()
        {
            if ((ParentForm != null))
            {
                panel.Hide();   // hiding the panel also hides the listbox
                if (ParentForm.Controls.Contains(panel))
                    ParentForm.Controls.Remove(panel);
            }
        }

        protected override void OnKeyDown(KeyEventArgs args)
        {
            args.Handled = true;
            if ((args.KeyCode == Keys.Up))
                MoveSelectionInListBox((SelectedIndex - 1));    // Up: move the selection in listbox one up
            else if ((args.KeyCode == Keys.Down))
                MoveSelectionInListBox((SelectedIndex + 1));    // Down: move the selection in listbox one down
            else if ((args.KeyCode == Keys.PageUp))
                MoveSelectionInListBox((SelectedIndex - 10));   // Page Up: move 10 up
            else if ((args.KeyCode == Keys.PageDown))
                MoveSelectionInListBox((SelectedIndex + 10));   // Pade Down: move 10 down
            else if ((args.KeyCode == Keys.Enter))
                SelectItem();                                   // Enter: select the item in the ListBox
            else if (args.KeyCode == Keys.Escape)
                HideSuggestionListBox();                        // Escape: Hide suggestion box
            else if (args.Control && args.KeyCode == Keys.A)
                SelectAll(); // Crtl+A: select all
            else
            {
                base.OnKeyDown(args); // work is not done, maybe the base class will process the event, so call it...            
                args.Handled = false;
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            if (AlwaysShowSuggest && !panel.Visible && listBox.Items.Count > 0)
                ShowSuggests();
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            if (!(ContainsFocus || panel.ContainsFocus))
                SelectItem();   // then hide the stuff and select the last valid item
            base.OnLostFocus(e);
        }

        protected override void OnTextChanged(EventArgs args)
        {
            if (!(DesignMode || _suppressAutoSuggest))
                ShowSuggests();
            base.OnTextChanged(args);
        }

        private void listBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectItem();
                e.Handled = true;
            }
        }

        private void listBox_MouseClick(object sender, MouseEventArgs e)
        {
            SelectItem();
        }

        private void MoveSelectionInListBox(int index)
        {
            if (listBox.Items.Count < 1)
                return;

            if (index <= -1)    // beginning of list
                listBox.SelectedIndex = 0;
            else
                if (index > (listBox.Items.Count - 1))  // end of list
                    listBox.SelectedIndex = (listBox.Items.Count - 1);
                else
                    listBox.SelectedIndex = index; // somewhere in the middle

            if (!panel.Visible && listBox.Items.Count > 0)
                ShowSuggests();
        }

        // selects the current item
        private bool SelectItem()
        {
            if (AutoCompleteList == null || AutoCompleteList.Count < 1)
                return false;

            _suppressSelIndexChanged = _suppressAutoSuggest = true;

            // if the ListBox is not empty
            if (listBox.Items.Count > 0)
            {
                if (_mouseIndex > 0 && _mouseIndex < listBox.Items.Count)
                    SelectedIndex = _mouseIndex;

                if (SelectedIndex > -1)
                {
                    base.Text = listBox.GetItemText(listBox.SelectedItem); // set the Text of the TextBox to the selected item of the ListBox
                    if (ContainsFocus)
                        SelectAll();
                }
                else
                {
                    for (int i = 0; i < listBox.Items.Count; i++)
                    {
                        var itemText = listBox.GetItemText(listBox.Items[i]);
                        if (base.Text == itemText)
                        {
                            SelectedIndex = i;
                            break;
                        }
                    }

                    if (SelectedIndex < 0)
                    {
                        listBox.SelectedItem = _prevSelItem;
                        if (SelectedIndex < 0)
                            SelectedIndex = 0;
                        base.Text = listBox.GetItemText(listBox.SelectedItem);
                    }
                }

                // and hide the ListBox
                HideSuggestionListBox();
            }
            else
            {
                // The listbox was empty
                object newSelItem = null;
                // ListBox is empty: Try to match the item with the data source
                if (!String.IsNullOrEmpty(base.Text))
                    foreach (var item in AutoCompleteList)
                    {
                        var itemText = listBox.GetItemText(item);
                        if (base.Text == itemText)
                        {
                            newSelItem = item;
                            break;
                        }
                    }

                if (newSelItem == null)
                {
                    if (_prevSelItem == null)
                        newSelItem = AutoCompleteList[0];
                    else
                        newSelItem = _prevSelItem;
                }

                if (newSelItem != null)
                {
                    listBox.Items.Add(newSelItem);
                    base.Text = listBox.GetItemText(newSelItem);
                }

                listBox.SelectedItem = newSelItem;
            }

            var selItem = listBox.SelectedItem;
            if (_prevSelItem != selItem)
            {
                if (SelectedItemChanged != null)
                    SelectedItemChanged(this, EventArgs.Empty);
                _prevSelItem = selItem;
            }

            _suppressSelIndexChanged = _suppressAutoSuggest = false;
            return true;
        }

        // shows the suggestions in ListBox beneath the TextBox
        // and fitting it into the ParentForm
        private void ShowSuggests()
        {
            // show only if MinTypedCharacters have been typed
            if (base.Text.Length >= MinTypedCharacters)
            {
                // prevent overlapping problems with other controls
                // while loading data there is nothing to draw, so suspend layout
                panel.SuspendLayout();
                UpdateCurrentAutoCompleteListAndPanel();

                if (CurrentAutoCompleteList != null && CurrentAutoCompleteList.Count > 0)
                {
                    // finally show Panel and ListBox
                    // (but after refresh to prevent drawing empty rectangles)
                    listBox.SelectedIndex = 0;
                    panel.Show();
                    // at the top of all controls
                    panel.BringToFront();
                    // then give the focuse back to the TextBox (this control)
                    Focus();
                }
                // or hide if no results
                else
                {
                    HideSuggestionListBox();
                }
                // prevent overlapping problems with other controls
                panel.ResumeLayout(true);
            }
            // hide if typed chars <= MinCharsTyped
            else
            {
                HideSuggestionListBox();
            }
        }

        // This is a timecritical part
        // Fills/ refreshed the CurrentAutoCompleteList with appropreate candidates
        private void UpdateCurrentAutoCompleteListAndPanel()
        {
            var parentForm = ParentForm;
            // if there is a ParentForm
            if ((parentForm != null))
            {
                // Update auto complete data source
                // the list of suggestions has to be refreshed so clear it
                CurrentAutoCompleteList = new ListBox.ObjectCollection(listBox);
                int curIndex = 0;
                string txt = base.Text;

                // and find appropreate candidates for the new CurrentAutoCompleteList in AutoCompleteList                
                foreach (object obj in AutoCompleteList)
                {
                    string str = obj.ToString();
                    bool isMatch = (MinTypedCharacters == 0 && String.IsNullOrEmpty(txt)) ||
                                   str.IndexOf(txt, CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase) > -1;
                    if (isMatch)
                    {
                        CurrentAutoCompleteList.Add(obj);   // Add candidates to new CurrentAutoCompleteList                        
                        if (++curIndex == VisibleSuggestItems)
                            break;
                    }
                }

                listBox.Items.Clear();
                listBox.Items.AddRange(CurrentAutoCompleteList);

                // Visual rendering
                // get its width
                listBox.Width = Width;
                // calculate the remeining height beneath the TextBox

                var itemsHeight = 5 + (listBox.DrawMode == DrawMode.Normal
                    ? TextRenderer.MeasureText(new string('\n', CurrentAutoCompleteList.Count), listBox.Font).Height
                    : CurrentAutoCompleteList.Count * listBox.ItemHeight);

                var borderWidth = 2 * SystemInformation.BorderSize.Width;
                var screenLoc = PointToScreen(Point.Empty);
                var relLocation = parentForm.PointToClient(screenLoc);
                int maxHeight = parentForm.ClientSize.Height - Height - relLocation.Y;
                listBox.Height = (itemsHeight > maxHeight ? maxHeight : itemsHeight);
                // and the Location to use
                panel.Location = new Point(relLocation.X - borderWidth, relLocation.Y + Height - borderWidth);
                panel.Size = listBox.Size;
                if (!parentForm.Controls.Contains(panel))
                {
                    parentForm.Controls.Add(panel);
                }
            }
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suppressSelIndexChanged)
                return;

            RefreshItem(_prevSelIndex);
            _prevSelIndex = listBox.SelectedIndex;
        }

        private void listBox_MouseLeave(object sender, EventArgs e)
        {
            RefreshItem(_mouseIndex);
            RefreshItem(_prevSelIndex);
            _mouseIndex = -1;
        }

        private void listBox_MouseMove(object sender, MouseEventArgs e)
        {
            int index = listBox.IndexFromPoint(e.Location);
            if (index != _mouseIndex)
            {
                RefreshItem(_prevSelIndex);
                RefreshItem(_mouseIndex);
                RefreshItem(index);
                _mouseIndex = index;
            }
        }

        private void RefreshItem(int index)
        {
            if (index > -1 && index < listBox.Items.Count)
            {
                var itemRect = listBox.GetItemRectangle(index);
                listBox.Invalidate(itemRect);
            }
        }

        private void ListBoxOnDrawItem(object sender, DrawItemEventArgs e)
        {
            if (listBox.Items.Count < 0 || e.Index < 0)
                return;

            // Draw the background of the ListBox control for each item.
            var isHighlighted = (e.Index == _mouseIndex || (_mouseIndex < 0 && e.Index == SelectedIndex));
            Brush backBrush;
            var bounds = e.Bounds;

            if (isHighlighted)
                backBrush = SystemBrushes.Highlight;
            else
                backBrush = SystemBrushes.Window;

            e.Graphics.FillRectangle(backBrush, bounds);

            string text = listBox.Items[e.Index].ToString();

            using (var txtBrush = new SolidBrush(isHighlighted ? SystemColors.HighlightText : listBox.ForeColor))
            {
                var textSize = TextRenderer.MeasureText(text, listBox.Font, new Size(bounds.Width, int.MaxValue), TextFormatFlags.WordBreak);
                int y = bounds.Y + (bounds.Height - textSize.Height) / 2;
                e.Graphics.DrawString(text, e.Font, txtBrush, new Rectangle(2, y, bounds.Width, textSize.Height), StringFormat.GenericDefault);
            }

            e.Graphics.DrawLine(Pens.Gainsboro, 0, bounds.Y, bounds.X + bounds.Width, bounds.Y);

            // If the ListBox has focus, draw a focus rectangle around the selected item.
            e.DrawFocusRectangle();
        }
        #endregion Methods
    }

}
