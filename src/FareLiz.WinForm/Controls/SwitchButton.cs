﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal class SwitchButton : ImageButton
    {
        public event CancelEventHandler StateChanging;

        private bool _autoSwitchStateOnClick = true;
        public bool AutoSwitchStateOnClick { get { return _autoSwitchStateOnClick; } set { _autoSwitchStateOnClick = value; } }

        private bool _isSecondState = false;
        public bool IsSecondState
        {
            get { return _isSecondState; }
            set
            {
                if (StateChanging != null)
                {
                    var arg = new CancelEventArgs();
                    StateChanging(this, arg);
                    if (arg.Cancel)
                        return;
                }
                _isSecondState = value;
                UpdateState();
            }
        }

        private Image _firstStateImage;
        [Localizable(true)]
        public Image FirstStateImage
        {
            get { return _firstStateImage; }
            set
            {
                _firstStateImage = value;
                if (!IsSecondState)
                    Image = value;
            }
        }

        private Image _secondStateImage;
        [Localizable(true)]
        public Image SecondStateImage
        {
            get { return _secondStateImage; }
            set
            {
                _secondStateImage = value;
                if (IsSecondState)
                    Image = value;
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new Image Image { get { return base.Image; } private set { base.Image = value; } }

        private string _firstStateText;
        [Localizable(true)]
        public string FirstStateText
        {
            get { return _firstStateText; }
            set
            {
                _firstStateText = value;
                if (!IsSecondState)
                    Text = value;
            }
        }

        private string _secondStateText;
        [Localizable(true)]
        public string SecondStateText
        {
            get { return _secondStateText; }
            set
            {
                _secondStateText = value;
                if (IsSecondState)
                    Text = value;
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string Text { get { return base.Text; } private set { base.Text = value; } }

        /// <summary>
        /// The next mouse click won't change the button state
        /// </summary>
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool SuppressStateSwitch { get; set; }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            if (SuppressStateSwitch)
            {
                SuppressStateSwitch = false;
                return;
            }

            if (AutoSwitchStateOnClick)
                IsSecondState = !IsSecondState;
        }

        private void UpdateState()
        {
            Image = IsSecondState ? SecondStateImage : FirstStateImage;
            Text = IsSecondState ? SecondStateText : FirstStateText;
        }
    }
}
