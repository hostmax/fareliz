using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace SkyDean.FareLiz.Core.Presentation
{
    public delegate void OnFormatText(RichTextBox target);

    /// <summary>
    /// List of the different popup animation status
    /// </summary>
    public enum NotiferState
    {
        Hidden = 0,
        Appearing = 1,
        Visible = 2,
        Disappearing = 3
    }

    /// <summary>
    /// TaskbarNotifier allows to display styled popups
    /// </summary>
    public partial class TaskbarNotifierBase : SmartForm
    {
        private readonly Timer _timer = new Timer();
        private NotiferState _state = NotiferState.Hidden;
        private int _stayDuration;
        private bool _bIsMouseOverPopup;

        private double _opacityStep = 0.1;
        private const int TIMER_STEP = 50;

        public int FadeTime
        {
            get { return (int)(TIMER_STEP / _opacityStep); }
            set { _opacityStep = TIMER_STEP / value; }
        }

        protected virtual void InitializeContentPanel(Panel contentPanel) { }
        protected virtual Size MeasureContentSize(int maxWidth, int maxHeight)
        {
            return Size.Empty;
        }

        /// <summary>
        /// The Constructor for TaskbarNotifier
        /// </summary>
        protected TaskbarNotifierBase()
        {
            InitializeComponent();
            InitializeContentPanel(pnlMiddle);
            CreateHandle(); // Allocate the form on the thread where it was created
            AssignMouseHandler(this);
            _timer.Tick += OnTimer;
        }

        public void WaitTillHidden()
        {
            while (Visible)
                Thread.Sleep(500);
        }

        private void AssignMouseHandler(Control targetControl)
        {
            targetControl.MouseEnter += OnMouseEnter;
            targetControl.MouseLeave += OnMouseLeave;
            foreach (Control control in targetControl.Controls)
            {                
                AssignMouseHandler(control);
            }
        }

        private void OnTimer(Object obj, EventArgs ea)
        {
            switch (_state)
            {
                case NotiferState.Appearing:
                    if (Opacity < 1)
                        Opacity += _opacityStep;
                    else
                    {
                        _timer.Stop();
                        _timer.Interval = _stayDuration;
                        _state = NotiferState.Visible;
                        _timer.Start();
                    }
                    break;

                case NotiferState.Visible:
                    _timer.Stop();
                    _timer.Interval = TIMER_STEP;
                    if (!_bIsMouseOverPopup)
                    {
                        _state = NotiferState.Disappearing;
                    }
                    _timer.Start();
                    break;

                case NotiferState.Disappearing:
                    if (_bIsMouseOverPopup)
                    {
                        _state = NotiferState.Appearing;
                    }
                    else
                    {
                        if (Opacity > 0)
                            Opacity -= _opacityStep;
                        else
                            Hide();
                    }
                    break;

                case NotiferState.Hidden:
                    _timer.Enabled = false;
                    break;
            }

        }

        /// <summary>
        /// Displays the popup for a certain amount of time
        /// </summary>
        /// <param name="timeToStay">Time to stay in milliseconds</param>
        /// <param name="type">Notification type</param>
        protected void Display(int timeToStay, NotificationType type)
        {
            _stayDuration = timeToStay;
            SetWindowMetrics();
            ApplyTheme(type);
            _timer.Enabled = true;
            _timer.Interval = 100;
            pnlMiddle.Invalidate();

            switch (_state)
            {
                case NotiferState.Hidden:
                case NotiferState.Disappearing:
                    _timer.Stop();
                    _state = NotiferState.Appearing;
                    NativeMethods.ShowWindow(Handle, 4); // We Show the popup without stealing focus
                    _timer.Interval = TIMER_STEP;
                    _timer.Start();
                    break;

                case NotiferState.Appearing:
                    if (!_timer.Enabled)
                    {
                        _timer.Interval = TIMER_STEP;
                        _timer.Start();
                    }
                    break;

                case NotiferState.Visible:
                    _timer.Stop();
                    _timer.Interval = _stayDuration;
                    _timer.Start();
                    break;
            }
        }

        protected void SetTitle(string title)
        {
            lblTitle.Text = title;
        }

        private void SetWindowMetrics()
        {
            int screenWidth = Screen.PrimaryScreen.WorkingArea.Width;
            int screenHeight = Screen.PrimaryScreen.WorkingArea.Height;

            int maxWidth = screenWidth / 3;
            int maxHeight = screenHeight / 3;
            Size szContent = MeasureContentSize(maxWidth, maxHeight);

            Size szTitle = TextRenderer.MeasureText(lblTitle.Text, lblTitle.Font, szContent, TextFormatFlags.SingleLine);
            int height = szContent.Height + pnlTop.Height + pnlBottom.Height + pnlMiddle.Padding.Top + pnlMiddle.Padding.Bottom + Padding.Top + Padding.Bottom,
                contentWidth = szContent.Width + pnlMiddle.Padding.Left + pnlMiddle.Padding.Right,
                titleWidth = szTitle.Width + pnlTopRight.Width + pnlTopLeft.Width + lblTitle.Padding.Left + lblTitle.Padding.Right,
                width = ((contentWidth > titleWidth) ? contentWidth : titleWidth) + Padding.Left + Padding.Right;

            if (height > maxHeight)
                height = maxHeight;
            if (width > maxWidth)
                width = maxWidth;

            int left = screenWidth - width,
                top = screenHeight - height;

            NativeMethods.SetWindowPos(Handle.ToInt32(), -1, left, top, width, height, 0x0010);
        }

        /// <summary>
        /// Hides the popup
        /// </summary>
        /// <returns>Nothing</returns>
        public new void Hide()
        {
            if (_state != NotiferState.Hidden)
            {
                _timer.Stop();
                _state = NotiferState.Hidden;
                imgClose.Visible = false;
                base.Hide();
            }
        }

        private void OnMouseEnter(object sender, EventArgs ea)
        {
            _bIsMouseOverPopup = imgClose.Visible = true;
            Opacity = 1;
            _state = NotiferState.Visible;
        }

        private void OnMouseLeave(object sender, EventArgs ea)
        {
            DetectMouseLeave();
        }

        private void DetectMouseLeave()
        {
            if (Cursor.Position.X < Location.X
                || Cursor.Position.Y < Location.Y
                || Cursor.Position.X > Location.X + Width
                || Cursor.Position.Y > Location.Y + Height)
            {
                _bIsMouseOverPopup = imgClose.Visible = false;
                _timer.Stop();
                _timer.Interval = _stayDuration;
                _timer.Start();
            }
        }

        private void imgClose_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
