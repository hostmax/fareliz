﻿using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public partial class FlightDataListView : UserControl
    {
        /// <summary>
        /// The minimum amount of items count before VirtualMode is used
        /// </summary>
        public int VirtualModeThreshold { get; set; }

        public event ContextMenuStripEventHandler OnContextMenuStripOpening;

        private readonly object _syncObject = new object();
        private readonly FlightDataListViewItemAdaptor _lvFlightItemAdaptor = new FlightDataListViewItemAdaptor();
        private readonly FlightDataColumnSorter _sorter = new FlightDataColumnSorter();
        private volatile bool _lvChangeRequested = false;
        private DateTime _lvChangeRequestDate = DateTime.MinValue;

        private List<Flight> _flightsData = new List<Flight>();
        private readonly List<Flight> _filteredFlightsData = new List<Flight>();
        public List<Flight> CurrentData { get { return _flightsData; } }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public ICurrencyProvider CurrencyProvider
        {
            get { return GlobalContext.MonitorEnvironment.CurrencyProvider; }
        }

        public string ActiveCurrency { get; set; }

        public Flight SelectedFlight
        {
            get
            {
                if (lvFlightData.SelectedIndices.Count == 1)
                {
                    var selItem = lvFlightData.Items[lvFlightData.SelectedIndices[0]];
                    return selItem.Tag as Flight;
                }
                return null;
            }
        }

        public FlightDataListView()
        {
            InitializeComponent();
            VirtualModeThreshold = 3000;
            lvFlightData.ListViewItemSorter = _sorter;
            lvFlightData.VirtualModeSort = FlightDataVirtualModeSort;
            lvFlightData.VirtualModeFilter = FlightDataVirtualModeFilter;
            lvFlightData.AttachMenuStrip(lvFlightDataContextMenuStrip);
            lvFlightData.SetGroupColumn(colPeriod);
        }

        public void AppendContextMenuStrip(params ToolStripItem[] items)
        {
            if (items != null)
                lvFlightDataContextMenuStrip.Items.AddRange(items);
        }

        public void ClearData()
        {
            lvFlightData.VirtualListSize = 0;
            lvFlightData.Items.Clear();
            lvFlightData.Groups.Clear();
        }

        /// <summary>
        /// Thread-safe setting water mark for the listview
        /// </summary>
        /// <param name="text"></param>
        public void SetWatermark(string text)
        {
            if (lblWaterMark.InvokeRequired)
                lblWaterMark.BeginSafeInvoke(new Action(() => SetWatermark(text)));
            else
            {
                lblWaterMark.Visible = !String.IsNullOrEmpty(text);
                lblWaterMark.Text = text;
            }
        }

        /// <summary>
        /// Set the data source for the list view and render the items. This method returns immediately
        /// </summary>
        public void SetDataSourceAsync(List<Flight> flights, bool autoResize)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(o =>
                {
                    AppUtil.NameCurrentThread((ParentForm == null ? String.Empty : ParentForm.GetType().Name + "-") + "FlightLV-SetDataSourceAsync");
                    SetDataSource(flights, autoResize);
                }));
        }

        public void SetDataSource(List<Flight> flights, bool autoResize)
        {
            DateTime requestDate = _lvChangeRequestDate = DateTime.Now;

            Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] Entering lock for FlightDataListView [{1}]", Thread.CurrentThread.ManagedThreadId, Name));
            if (!Monitor.TryEnter(_syncObject))
            {
                Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] Could not enter lock for FlightDataListView [{1}]... Try again", Thread.CurrentThread.ManagedThreadId, Name));
                _lvChangeRequested = true;
                Monitor.Enter(_syncObject);
            }
            Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] Succesfully obtain lock for FlightDataListView [{1}]", Thread.CurrentThread.ManagedThreadId, Name));
            _lvChangeRequested = false;

            try
            {
                if (requestDate < _lvChangeRequestDate || this.IsUnusable()) // Another request has come in: Let the later one passes through, or return if control is disposed
                    return;

                var totalFlightsCount = flights == null ? 0 : flights.Count;
                bool needVirtualMode = (totalFlightsCount > VirtualModeThreshold);

                ListViewItem[] lvItems = null;
                if (!needVirtualMode)
                {
                    lvItems = new ListViewItem[totalFlightsCount];
                    for (int i = 0; i < totalFlightsCount; i++)
                    {
                        var f = flights[i];
                        lvItems[i] = _lvFlightItemAdaptor.GetListViewItem(f, ActiveCurrency);
                    }
                }

                if (this.IsUnusable() || _lvChangeRequested)
                    return;

                // Render the view
                this.BeginSafeInvoke((Action)delegate
                    {
                        ClearData();

                        if (totalFlightsCount > 0)
                        {
                            lvFlightData.VirtualMode = needVirtualMode;
                            if (needVirtualMode)
                                lvFlightData.VirtualListSize = _flightsData.Count;

                            if (!needVirtualMode)
                            {
                                lvFlightData.BeginUpdate();
                                try
                                {
                                    lvFlightData.AddRange(lvItems);
                                    if (this.IsUnusable() || _lvChangeRequested)
                                        return;

                                    if (lvFlightData.GroupColumnIndex > -1)
                                        lvFlightData.AutoGroup();
                                }
                                finally
                                {
                                    lvFlightData.EndUpdate();
                                }
                            }

                            if (autoResize && _flightsData != null && _flightsData.Count > 0)
                                lvFlightData.AutoResize();
                        }
                    });

                _flightsData = flights;
            }
            catch (Exception ex)
            {
                if (this.IsUnusable() || _lvChangeRequested)
                    return;

                Console.Error.WriteLine(ex.ToString());
                throw;
            }
            finally
            {
                Monitor.Exit(_syncObject);
                Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "[{0}] Released lock for FlightDataListView [{1}]", Thread.CurrentThread.ManagedThreadId, Name));
            }
        }

        private void FlightDataVirtualModeSort(EnhancedListView listView)
        {
            _flightsData.Sort(_sorter);
        }

        private void FlightDataVirtualModeFilter(EnhancedListView listView, List<ListViewFilter> filters)
        {
            if (EnhancedListView.DoFilter<Flight>(_flightsData, _filteredFlightsData, filters, FilterFlight))
            {
                listView.VirtualListSize = _flightsData.Count;
                listView.Sort();
            }
        }

        private bool FilterFlight(Flight flight, ListViewFilter filter)
        {
            var data = _lvFlightItemAdaptor.GetPresentationStrings(flight, ActiveCurrency);
            if (!data[filter.Column].IsMatch(filter.FilterString))
                return false;
            return true;
        }

        public void ToCsv()
        {
            var dlg = new SaveFileDialog
            {
                Filter = "CSV Document|*.csv|Text Document|*.txt|All files|*",
                Title = "Export Flight Data",
                FileName = "FareData"
            };

            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                lvFlightData.ToCsv(dlg.FileName);
            }
        }

        private void mnuRefreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void lvFlightData_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            if (e.ItemIndex < _flightsData.Count)
                e.Item = _lvFlightItemAdaptor.GetListViewItem(_flightsData[e.ItemIndex], ActiveCurrency);
        }

        private void lvFlightDataContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            var selFlight = SelectedFlight;
            mnuBookTrip.Available = CanBuy(selFlight);

            if (mnuBookTrip.Available)
                mnuBookTrip.ToolTipText = selFlight.SummaryString;

            if (OnContextMenuStripOpening != null)
            {
                var args = new MenuBuilderEventArgs(selFlight);
                OnContextMenuStripOpening(this, args);
                e.Cancel = args.Cancel;
            }
        }

        private void mnuChangeCurrency_DropDownOpening(object sender, EventArgs e)
        {
            UpdateCurrencyMenuStrip();
            mnuCurrencySeparator.Available = mnuChangeCurrency.DropDownItems.Count > 2;
        }

        public void UpdateCurrencyMenuStrip()
        {
            if (mnuChangeCurrency.DropDownItems.Count > 2)
                for (int i = 2; i < mnuChangeCurrency.DropDownItems.Count; i++)
                    mnuChangeCurrency.DropDownItems.RemoveAt(i--);

            var currencyProvider = CurrencyProvider;
            if (currencyProvider == null)
                return;

            var allCurrencies = currencyProvider.GetCurrencies();
            var sel = currencyProvider.AllowedCurrencies;
            RadioToolStripMenuItem selCurrency = mnuUseDataCurrency;

            foreach (var c in allCurrencies)
            {
                if (sel == null || sel.Contains(c.Key))
                {
                    var newItem = new RadioToolStripMenuItem(String.Format(CultureInfo.InvariantCulture, "{0} - {1} {2}", c.Key, c.Value.FullName, c.Value.Symbol), c.Key);
                    newItem.Click += changeCurrency_Click;
                    mnuChangeCurrency.DropDownItems.Add(newItem);
                    if (c.Key == ActiveCurrency)
                        selCurrency = newItem;
                }
            }

            selCurrency.Checked = true;
        }

        private void changeCurrency_Click(object sender, EventArgs e)
        {
            var mnuItem = sender as ToolStripMenuItem;
            if (mnuItem != null && mnuItem.Checked)
            {
                var selected = (string)mnuItem.Tag;
                if (ActiveCurrency != selected)
                {
                    ActiveCurrency = selected;
                    RefreshData();
                }
            }
        }

        public void RefreshData()
        {
            SetDataSourceAsync(_flightsData, true);
        }

        private void BuyTicket_Click(object sender, EventArgs e)
        {
            BuySelectedFlight();
        }

        private bool CanBuy(Flight flight)
        {
            var result = (flight != null && flight.TravelAgency != null && !String.IsNullOrEmpty(flight.TravelAgency.Url)
                          && !(flight.OutboundLeg != null && flight.OutboundLeg.Departure.Date <= DateTime.Now.Date));
            return result;
        }

        private void BuySelectedFlight()
        {
            var selFlight = SelectedFlight;
            if (!CanBuy(selFlight))
                return;

            var url = selFlight.TravelAgency.Url;
            if (!String.IsNullOrEmpty(url))
            {
                try
                {
                    BrowserUtils.Open(url);
                }
                catch (Exception ex)
                {
                    string error = "Failed to launch web browser for ticket purchase: " + ex.Message;
                    GlobalContext.Logger.Error(error);
                    MessageBox.Show(error, "Ticket Purchase", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
