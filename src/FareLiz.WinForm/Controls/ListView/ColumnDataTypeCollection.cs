﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// This is an indexer to the LVDataType values for each column.
    /// The data is stored in the lParam value of the column.
    /// </summary>
    public class ColumnDataTypeCollection
    {
        private readonly ListViewColumnHeader _header; // owning header control
        private HDITEM _hdItem; // HDITEM instance

        /// <summary>
        /// Constructor this must be given the header instance for access to the Handle property so that messages can be sent to it.
        /// </summary>
        /// <param name="header">HeaderControl</param>
        public ColumnDataTypeCollection(ListViewColumnHeader header)
        {
            _header = header;
        }

        /// <summary>
        /// Indexer method to get/set the LVDataType for the column.
        /// </summary>
        public FilterDataType this[int index]
        {
            get
            {
                // the lparam of the column header contains the datatype
                _hdItem.mask = W32_HDI.HDI_LPARAM;
                _hdItem.lParam = (int)FilterDataType.String;

                // if it is valid the lparam is updated and then returned
                NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMW, index, ref _hdItem);
                return (FilterDataType)_hdItem.lParam;
            }
            set
            {
                // ensure that this is a valid column
                if (index < Count)
                {
                    // simply set the new LVDataType in the lparam and pass it on
                    _hdItem.mask = W32_HDI.HDI_LPARAM;
                    _hdItem.lParam = (int)value;
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_SETITEMW, index, ref _hdItem);
                }
            }
        }

        /// <summary>
        /// Return the number of columns in the header.
        /// </summary>
        public int Count
        {
            get { return NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMCOUNT, 0, IntPtr.Zero); }
        }
    }
}
