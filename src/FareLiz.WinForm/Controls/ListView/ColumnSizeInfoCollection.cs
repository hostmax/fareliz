﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// This is an indexer to the READONLY Size values for a column.
    /// NOTE: The Size really contains the Width and Left position,
    /// the Left is sorted in the Height property of the Size class
    /// We do this because a Rectangle is not really necessary.
    /// </summary>
    public class ColumnSizeInfoCollection
    {
        private readonly ListViewColumnHeader _header; // owning header control
        private RECT _colRectangle; // HDITEM instance

        /// <summary>
        /// Constructor this must be given the header instance for access to the Handle property so that messages can be sent to it.
        /// </summary>
        /// <param name="header">HeaderControl</param>
        public ColumnSizeInfoCollection(ListViewColumnHeader header)
        {
            _header = header;
        }

        /// <summary>
        /// Indexer method to get/set the Size for the column.
        /// </summary>
        public Size this[int index]
        {
            get
            {
                // if the column is valid get the rectangle
                if (index < Count)
                {
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMRECT,
                                index, ref _colRectangle);
                    return new Size((_colRectangle.right - _colRectangle.left),
                                    _colRectangle.left);
                }

                    // return null size
                else return new Size(0, 0);
            }
        }

        /// <summary>
        /// Return the number of columns in the header.
        /// </summary>
        public int Count
        {
            get { return NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMCOUNT, 0, IntPtr.Zero); }
        }
    }
}
