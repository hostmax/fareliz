﻿using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public partial class EnhancedListView
    {
        public event ListViewItemsDelegate ItemsAdded;
        public event ListViewItemDelegate ItemRemoved;

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == (int)W32_WM.WM_NOTIFY)  // notify messages can come from the header and are used for column sorting and item filtering if wanted
            {
                var h1 = (NMHEADER)m.GetLParam(typeof(NMHEADER));   // get the notify message header from this message LParam
                if ((_header != null) && (h1.hdr.hwndFrom == _header.Handle)) // process messages ONLY from our header control
                    NotifyHeaderMessage(h1);
            }
            else if (m.Msg == (int)W32_WM.WM_LBUTTONUP)
                base.DefWndProc(ref m);

            base.WndProc(ref m);
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (_header != null)
                _header.ReleaseHandle();
            base.OnHandleDestroyed(e);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            if (_header != null)
            {
                _header.ReleaseHandle();
                _header.Attach(this);   // Subclass for the header control
            }

            base.OnHandleCreated(e);
        }

        /// <summary>
        /// When user click on a Filter menu item
        /// </summary>
        private void FilterMenuItem_Click(object sender, EventArgs e)
        {
            var mnu = sender as ToolStripMenuItem;
            if (mnu == null)
                return;

            var index = (int)mnu.Tag;
            if (index == -1)
                _header.ClearAllFilters();
            else
            {
                ListViewItem selItem = FirstSelectedItem;
                if (selItem != null)
                {
                    _header.Filters[index] = selItem.SubItems[index].Text;
                }
            }
        }

        /// <summary>
        /// Populate the filter context menu strip (depending on the available columns and selected item)
        /// </summary>
        private void FilterDataMenuStrip_DropDownOpening(object sender, EventArgs eventArgs)
        {
            var mnu = sender as ToolStripDropDownItem;
            if (mnu != null)
            {
                mnu.DropDownItems.Clear();
                var mnuClearAllFilters = new RadioToolStripMenuItem("Clear all filters", -1);
                mnuClearAllFilters.Click += FilterMenuItem_Click;
                mnu.DropDownItems.Add(mnuClearAllFilters);

                var selItem = FirstSelectedItem;
                if (selItem != null)
                {
                    mnu.DropDownItems.Add(new ToolStripSeparator());
                    for (int i = 0; i < selItem.SubItems.Count; i++)
                    {
                        var subItem = selItem.SubItems[i];
                        var newItem = new RadioToolStripMenuItem(Columns[i].Text + " = " + subItem.Text, i);
                        newItem.Click += FilterMenuItem_Click;
                        mnu.DropDownItems.Add(newItem);
                    }
                }
            }
        }

        /// <summary>
        /// When user click on a Group By menu item
        /// </summary>
        void GroupByMenuItem_Click(object sender, EventArgs e)
        {
            var mnu = sender as ToolStripMenuItem;
            if (mnu == null)
                return;

            var index = (int)mnu.Tag;
            GroupColumnIndex = index;
            if (base.ShowGroups = (index > -1))
                AutoGroup();
        }

        private void NotifyHeaderMessage(NMHEADER h)
        {
            // process only specific header notification messages
            switch (h.hdr.code)
            {
                // a header column was clicked, do the sort
                case (int)W32_HDN.HDN_ITEMCLICKA:
                case (int)W32_HDN.HDN_ITEMCLICKW:
                    int col = h.iItem;
                    if (_header.SortInfo.SortColumn == col)
                    {
                        // Sort on same column, just the opposite direction.
                        _header.SortInfo.SortAscending = !_header.SortInfo.SortAscending;
                    }
                    else
                    {
                        _header.SortInfo.SortAscending = true;
                        _header.SortInfo.SortColumn = col;
                    }
                    Sort();
                    break;

                // a filter button was clicked display the popup menu
                // to handle setting filter options for the column
                case (int)W32_HDN.HDN_FILTERBTNCLICK:
                    _filterButtonColumn = h.iItem;
                    mnuFilterButton.Show(this, PointToClient(MousePosition));
                    break;

                // a filter content changed, update the items collection
                case (int)W32_HDN.HDN_FILTERCHANGE:

                    // if this is for item -1 then this is a clear all filters
                    if (h.iItem < 0)
                        _activeFilters.Clear();

                        // if we are filtered this is a real filter data change
                    else if (Filtered)
                        BuildFilter(h.iItem);

                    // update the items array with new filters applied
                    ApplyFilters();
                    if (GroupColumnIndex > -1)
                        AutoGroup();

                    break;
            }
        }

        /// <summary>
        /// Set the sort icon for the column depending on the sort type
        /// </summary>
        void SetSortIcon(int columnIndex, bool ascending)
        {
            IntPtr columnHeader = NativeMethods.SendMessage(Handle, (int)W32_LVM.LVM_GETHEADER, IntPtr.Zero, IntPtr.Zero);
            for (int columnNumber = 0; columnNumber <= Columns.Count - 1; columnNumber++)
            {
                var columnPtr = new IntPtr(columnNumber);
                var item = new HDITEM
                {
                    mask = W32_HDI.HDI_FORMAT
                };

                if (NativeMethods.SendMessage(columnHeader, (int)W32_HDM.HDM_GETITEMW, columnPtr, ref item) == IntPtr.Zero)
                    throw new Win32Exception();

                if (columnNumber == columnIndex)
                {
                    if (ascending)
                    {
                        item.fmt &= ~W32_HDF.HDF_SORTDOWN;
                        item.fmt |= W32_HDF.HDF_SORTUP;
                    }
                    else
                    {
                        item.fmt &= ~W32_HDF.HDF_SORTUP;
                        item.fmt |= W32_HDF.HDF_SORTDOWN;
                    }
                }
                else
                {
                    item.fmt &= ~W32_HDF.HDF_SORTDOWN & ~W32_HDF.HDF_SORTUP;
                }

                if (NativeMethods.SendMessage(columnHeader, (int)W32_HDM.HDM_SETITEMW, columnPtr, ref item) == IntPtr.Zero)
                    throw new Win32Exception();
            }
        }

        /// <summary>
        /// Update the menu when users click on the Filter button on the column header
        /// </summary>
        void FilterButtonMenu_Popup(object sender, System.EventArgs e)
        {
            // also set the current alignment
            switch (_header.Alignment[_filterButtonColumn])
            {
                case HorizontalAlignment.Center:
                    mnuAlignLeft.Checked = mnuAlignRight.Checked = !(mnuAlignCenter.Checked = true);
                    break;
                case HorizontalAlignment.Right:
                    mnuAlignLeft.Checked = mnuAlignCenter.Checked = !(mnuAlignRight.Checked = true);
                    break;
                default:
                    mnuAlignCenter.Checked = mnuAlignRight.Checked = !(mnuAlignLeft.Checked = true);
                    break;
            }

            mnuIgnoreCase.Checked = _isFilterIgnoreCase;
        }

        /// <summary>
        ///     MenuItem click event.  Perform the requested menu action
        ///     and always force a filter rebuild since all these change
        ///     the filtering properties in some manner.  The sender
        ///     identifies the specific menuitem clicked.
        /// </summary>
        /// <param name="sender">MenuItem</param>
        /// <param name="e">Event</param>
        private void FilterButtonMenuItem_Click(object sender, EventArgs e)
        {
            // 'Clear filter', set the Header.Filter for the column
            if (sender == mnuClearFilter)
                _header.Filters[_filterButtonColumn] = "";

            else if (sender == mnuClearAllFilter)
                _header.ClearAllFilters();

                // 'Ignore case', toggle the flag
            else if (sender == mnuIgnoreCase)
                FilterIgnoreCase = !FilterIgnoreCase;

                // 'Left', set the alignment
            else if (sender == mnuAlignLeft)
                _header.Alignment[_filterButtonColumn] = HorizontalAlignment.Left;

                // 'Right', set the alignment
            else if (sender == mnuAlignRight)
                _header.Alignment[_filterButtonColumn] = HorizontalAlignment.Right;

                // 'Center', set the alignment
            else if (sender == mnuAlignCenter)
                _header.Alignment[_filterButtonColumn] = HorizontalAlignment.Center;

                // unknown, ignore this type
            else return;

            // force a filter build on the specific column
            BuildFilter(_filterButtonColumn);

            // follow with a filter update
            ApplyFilters();

            // set focus to ourself after menu action otherwise
            // focus is left in the filter edit itself...
            Focus();

            // if this was an alignment change then we need to invalidate
            if (((MenuItem)sender).Parent == mnuAlignment)
                Invalidate();
        }
    }
}