﻿namespace SkyDean.FareLiz.WinForm.Controls
{
    public class SortInfo
    {
        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>        
        public int SortColumn
        {
            set { _sortColumn = value; }
            get { return _sortColumn; }
        }
        private int _sortColumn = -1;

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>        
        public bool SortAscending
        {
            set { _sortAscending = value; }
            get { return _sortAscending; }
        }
        private bool _sortAscending;

        public SortInfo(int column, bool ascending)
        {
            SortColumn = column;
            SortAscending = ascending;
        }
    }
}
