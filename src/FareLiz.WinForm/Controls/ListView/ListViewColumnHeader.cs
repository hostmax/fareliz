﻿using System;
using System.Collections;
using System.Windows.Forms;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public class ListViewColumnHeader : NativeWindow
    {
        private EnhancedListView _ownerListView; // owning listview control        
        private HDITEM hdr_hditem; // instance container

        /// <summary>
        /// Indexer to the alignment by column.
        /// </summary>
        public readonly ColumnAlignmentCollection Alignment;

        /// <summary>
        /// Indexer to the data types by column.
        /// </summary>
        public readonly ColumnDataTypeCollection DataType;

        /// <summary>
        /// Indexer to the column filter text.
        /// </summary>
        public readonly ColumnFilterCollection Filters;

        /// <summary>
        /// Indexer to the column name text.
        /// </summary>
        public readonly ColumnNamesCollection Names;

        /// <summary>
        /// Indexer to the column size (Width and Left as Height).
        /// </summary>
        public readonly ColumnSizeInfoCollection SizeInfo;

        private bool hdr_filter; // show filterbar in header
        /// <summary>
        /// When the Filtered property changes update the window style.
        /// </summary>
        public bool Filtered
        {
            get { return hdr_filter; }
            set
            {
                if (hdr_filter != value)
                {
                    hdr_filter = value;
                    ChangeFiltered();
                }
            }
        }

        private readonly SortInfo _sortInfo = new SortInfo(-1, true);
        public SortInfo SortInfo
        {
            get { return _sortInfo; }
        }

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnHeader(bool filtered)
        {
            // create the collection properties
            DataType = new ColumnDataTypeCollection(this);
            Filters = new ColumnFilterCollection(this);
            Names = new ColumnNamesCollection(this);
            SizeInfo = new ColumnSizeInfoCollection(this);
            Alignment = new ColumnAlignmentCollection(this);
            Filtered = filtered;
        }

        public void Attach(EnhancedListView listView)
        {
            _ownerListView = listView;
            var headerHandle = NativeMethods.GetDlgItem(_ownerListView.Handle, 0);

            if ((int)headerHandle != 0)
            {
                if ((int)Handle != 0)
                    ReleaseHandle();
                AssignHandle(headerHandle); // set the handle to this control.  the first dialog item for a listview is this header control...
            }
        }

        public void ClearAllFilters()
        {
            for (int i = 0; i < Filters.Count; i++)
                Filters[i] = "";
        }

        /// <summary>
        ///     Set the window style to reflect the value of the Filtered
        ///     property.  This is done when the Handle or property change.
        /// </summary>
        private void ChangeFiltered()
        {
            // we need to set a new style value for this control to turn
            // on/off the HDS_FILTERBAR flag.  First get the style itself.
            const int HDS_FILTR = (int)W32_HDS.HDS_FILTERBAR;
            int style = NativeMethods.GetWindowLong(Handle, W32_GWL.GWL_STYLE);

            // now that we have the flag see if it is not what is desired
            if (((style & HDS_FILTR) != 0) != hdr_filter)
            {
                // set/reset the flag for the filterbar
                if (hdr_filter) style |= HDS_FILTR;
                else style ^= HDS_FILTR;
                NativeMethods.SetWindowLong(Handle, W32_GWL.GWL_STYLE, style);

                // now we have to resize this control.  we do this by sending
                // a set item message to column 0 to change it's size.  this
                // is a kludge but the invalidate and others just don't work.
                hdr_hditem.mask = W32_HDI.HDI_HEIGHT;
                NativeMethods.SendMessage(Handle, W32_HDM.HDM_GETITEMW, 0, ref hdr_hditem);
                hdr_hditem.cxy += (hdr_filter) ? 1 : -1;
                NativeMethods.SendMessage(Handle, W32_HDM.HDM_SETITEMW, 0, ref hdr_hditem);
            }

            // it can't hurt to set the filter timeout limit to .5 seconds
            //NativeMethods.SendMessage(Handle, W32_HDM.HDM_SETFILTERCHANGETIMEOUT, 0, 500);

            // it is necessary to clear all filters which will cause a 
            // notification be sent to the ListView with -1 column.
            NativeMethods.SendMessage(Handle, W32_HDM.HDM_CLEARFILTER, -1, IntPtr.Zero);
        }

        protected override void OnHandleChange()
        {
            base.OnHandleChange();
            // reset the filter settings if there is a handle
            if (Handle != (IntPtr)0)
                ChangeFiltered();

        }
    }
}
