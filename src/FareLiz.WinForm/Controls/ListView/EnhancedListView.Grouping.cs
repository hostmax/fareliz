﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public partial class EnhancedListView
    {
        private delegate void CallBackSetGroupState(ListViewGroup lstvwgrp, ListViewGroupState state);
        private delegate void CallbackSetGroupString(ListViewGroup lstvwgrp, string value);

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref LVGROUP lParam);

        //Only Vista and forward allows collaps of ListViewGroups
        public static readonly bool SupportCollapsibleGroups = Environment.OSVersion.Version.Major > 5;

        /// <summary>
        /// Set selected column as the base column for grouping. This method does not refresh the view (Call AutoGroup() to refresh the view with the new group)
        /// </summary>
        /// <param name="col"></param>
        public void SetGroupColumn(ColumnHeader col)
        {
            var colIndex = GetColumn(col);

            if (colIndex > -1)
                SetGroupColumn(colIndex);
            else
                throw new ArgumentException("Could not set group column since the column does not belong to the ListView");
        }

        /// <summary>
        /// Set the column to be used for grouping
        /// </summary>
        public void SetGroupColumn(int index)
        {
            GroupColumnIndex = index;
            AutoGroup();
            RadioToolStripMenuItem item;
            if (_groupStrip.TryGetValue(index, out item))
                item.Checked = true;
        }

        /// <summary>
        /// Group the items according to the GroupIndexColumn
        /// </summary>
        public void AutoGroup()
        {
            if (GroupColumnIndex < 0)
                throw new ArgumentException("GroupColumnIndex was not set properly");

            if (VirtualMode)
                throw new InvalidOperationException("Could not group items in Virtual Mode");

            if (Items.Count > 0)
            {
                BeginUpdate();
                try
                {
                    Groups.Clear();

                    for (int i = 0; i < Items.Count; i++)
                    {
                        var item = Items[i];
                        string groupName = item.SubItems[GroupColumnIndex].Text;
                        ListViewGroup group = null;
                        foreach (ListViewGroup g in Groups)
                        {
                            if (g.Header == groupName || g.Name == groupName)
                            {
                                group = g;
                                break;
                            }
                        }

                        if (group == null)
                        {
                            group = new ListViewGroup(groupName);
                            Groups.Add(group);
                            SetGroupState(group, ListViewGroupState.Collapsible | ListViewGroupState.Normal);
                        }

                        item.Group = group;
                    }
                }
                finally
                {
                    EndUpdate();
                }
            }
        }

        private void SetGroupState(ListViewGroupState state)
        {
            if (!SupportCollapsibleGroups)
                return;

            foreach (ListViewGroup lvg in Groups)
                SetGroupState(lvg, state);
        }

        private void SetGroupState(ListViewGroup lstvwgrp, ListViewGroupState state)
        {
            if (!SupportCollapsibleGroups)
                return;

            if (lstvwgrp == null)
                return;

            int? grpId = GetGroupID(lstvwgrp);
            int grpIdParam = grpId ?? lstvwgrp.ListView.Groups.IndexOf(lstvwgrp);

            LVGROUP group = new LVGROUP()
            {
                CbSize = Marshal.SizeOf(typeof(LVGROUP)),
                State = state,
                Mask = ListViewGroupMask.State,
                IGroupId = grpIdParam
            };

            SendMessage(Handle, (int)W32_LVM.LVM_SETGROUPINFO, grpIdParam, ref group);
        }

        private static readonly PropertyInfo groupIdProp = typeof(ListViewGroup).GetProperty("ID", BindingFlags.NonPublic | BindingFlags.Instance);
        private static int? GetGroupID(ListViewGroup lstvwgrp)
        {
            int? rtnval = null;
            if (groupIdProp != null)
            {
                object tmprtnval = groupIdProp.GetValue(lstvwgrp, null);
                if (tmprtnval != null)
                    rtnval = tmprtnval as int?;
            }
            return rtnval;
        }

        public void SetGroupFooter(ListViewGroup lstvwgrp, string footer)
        {
            if (!SupportCollapsibleGroups)
                return;

            if (lstvwgrp == null)
                return;

            int? grpId = GetGroupID(lstvwgrp);
            int grpIdParam = grpId ?? Groups.IndexOf(lstvwgrp);

            LVGROUP group = new LVGROUP()
            {
                CbSize = Marshal.SizeOf(typeof(LVGROUP)),
                PszFooter = footer,
                Mask = ListViewGroupMask.Footer,
                IGroupId = grpIdParam
            };

            SendMessage(lstvwgrp.ListView.Handle, (int)W32_LVM.LVM_SETGROUPINFO, grpIdParam, ref group);
        }
    }
}