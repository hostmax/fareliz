﻿using SkyDean.FareLiz.Core.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public delegate void ContextMenuStripEventHandler(FlightDataListView sender, MenuBuilderEventArgs e);
    public class MenuBuilderEventArgs : CancelEventArgs
    {
        public Flight SelectedFlight { get; private set; }

        private readonly List<ToolStripItem> _newItems = new List<ToolStripItem>();

        public void AddItem(ToolStripItem item)
        {
            if (item != null)
                _newItems.Add(item);
        }

        public ToolStripItem[] NewItems { get { return _newItems.ToArray(); } }

        public MenuBuilderEventArgs(Flight selectedFlight)
        {
            SelectedFlight = selectedFlight;
        }
    }
}
