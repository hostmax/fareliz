﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// This is an indexer to the text values for each column.
    /// </summary>
    public class ColumnNamesCollection
    {
        private readonly ListViewColumnHeader _header; // owning header control
        private HDITEM _hdItem; // HDITEM instance

        /// <summary>
        /// Constructor this must be given the header instance for access
        /// to the Handle property so that messages can be sent to it.
        /// </summary>
        /// <param name="header">HeaderControl</param>
        public ColumnNamesCollection(ListViewColumnHeader header)
        {
            _header = header;
        }

        /// <summary>
        /// Indexer method to get/set the text of the column.
        /// </summary>
        public string this[int index]
        {
            get
            {
                // set up to retrive the column header text
                _hdItem.mask = W32_HDI.HDI_TEXT;
                _hdItem.pszText = new string(new char[64]);
                _hdItem.cchTextMax = _hdItem.pszText.Length;

                // if successful the text has been retrieved and returned
                NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMW, index, ref _hdItem);
                return _hdItem.pszText;
            }
            set
            {
                // this must be a valid column index
                if (index < Count)
                {
                    // simply set the text and size in the structure and pass it on
                    _hdItem.mask = W32_HDI.HDI_TEXT;
                    _hdItem.pszText = value;
                    _hdItem.cchTextMax = _hdItem.pszText.Length;
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_SETITEMA, index, ref _hdItem);
                }
            }
        }

        /// <summary>
        /// Return the number of columns in the header.
        /// </summary>
        public int Count
        {
            get { return NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMCOUNT, 0, IntPtr.Zero); }
        }
    }
}
