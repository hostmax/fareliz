﻿using System;
using System.Runtime.InteropServices;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// This is an indexer to the filter values for each column.  This
    /// is the most complex access to the HDITEM data since the filter
    /// data must be get/set via the HDTEXTFILTER structure referenced
    /// in the HDITEM structure.  It is simple to Marshall this, but
    /// figuring that out in the first place took alot of effort.
    /// </summary>
    public class ColumnFilterCollection
    {
        private readonly ListViewColumnHeader _header; // owning header control
        private HDITEM _hditem; // HDITEM instance
        private HDTEXTFILTER _hdTextFilter; // HDTEXTFILTER instance

        /// <summary>
        /// Constructor this must be given the header instance for access to the Handle property so that messages can be sent to it.
        /// </summary>
        /// <param name="header">HeaderControl</param>
        public ColumnFilterCollection(ListViewColumnHeader header)
        {
            _header = header;
        }

        /// <summary>
        /// Indexer method to get/set the filter text for the column.
        /// </summary>
        public string this[int index]
        {
            get
            {
                // if the column is invalid return nothing
                if (index >= Count || index < 0) return "";

                // this is tricky since it involves marshalling pointers
                // to structures that are used as a reference in another
                // structure.  first initialize the receiving HDTEXTFILTER
                _hdTextFilter.pszText = new string(new char[64]);
                _hdTextFilter.cchTextMax = _hdTextFilter.pszText.Length;

                // set the HDITEM up to request the current filter content
                _hditem.mask = W32_HDI.HDI_FILTER;
                _hditem.type = (uint)W32_HDFT.HDFT_ISSTRING;

                // marshall memory big enough to contain a HDTEXTFILTER 
                _hditem.pvFilter = Marshal.AllocCoTaskMem(
                    Marshal.SizeOf(_hdTextFilter));

                // now copy the HDTEXTFILTER structure to the marshalled memory
                Marshal.StructureToPtr(_hdTextFilter, _hditem.pvFilter, false);

                NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMW, index, ref _hditem);    // First check if the header fileter is empty or not
                _hdTextFilter = (HDTEXTFILTER)Marshal.PtrToStructure(_hditem.pvFilter, typeof(HDTEXTFILTER));  // un-marshall the memory back into the HDTEXTFILTER structure                

                if (!String.IsNullOrEmpty(_hdTextFilter.pszText))
                {
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMA, index, ref _hditem);
                    _hdTextFilter = (HDTEXTFILTER)Marshal.PtrToStructure(_hditem.pvFilter, typeof(HDTEXTFILTER));
                }

                Marshal.FreeCoTaskMem(_hditem.pvFilter);    // remember to free the marshalled IntPtr memory...

                // return the string in the text filter area
                return _hdTextFilter.pszText;
            }
            set
            {
                // ensure that the column exists before attempting this
                if (index < Count)
                {
                    // this is just like the get{} except we don't have to
                    // return anything and the message is HDM_SETITEMA. we
                    // use the non-unicode methods for both the get and set.
                    // reference the get{} method for marshalling details.
                    _hdTextFilter.pszText = value;
                    _hdTextFilter.cchTextMax = 64;
                    _hditem.mask = W32_HDI.HDI_FILTER;
                    _hditem.type = (uint)W32_HDFT.HDFT_ISSTRING;
                    _hditem.pvFilter = Marshal.AllocCoTaskMem(Marshal.SizeOf(_hdTextFilter));
                    Marshal.StructureToPtr(_hdTextFilter, _hditem.pvFilter, false);
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_SETITEMA, index, ref _hditem);
                    Marshal.FreeCoTaskMem(_hditem.pvFilter);
                }
            }
        }

        /// <summary>
        /// Return the number of columns in the header.
        /// </summary>
        public int Count
        {
            get { return NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMCOUNT, 0, IntPtr.Zero); }
        }
    }
}
