﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SkyDean.FareLiz.Core.Utils.WinNative;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public class ColumnAlignmentCollection
    {
        private readonly ListViewColumnHeader _header; // owning header control
        private HDITEM _hdItem; // HDITEM instance

        /// <summary>
        /// Constructor this must be given the header instance for access
        /// to the Handle property so that messages can be sent to it.
        /// </summary>
        /// <param name="header">HeaderControl</param>
        public ColumnAlignmentCollection(ListViewColumnHeader header)
        {
            _header = header;
        }

        /// <summary>
        /// Indexer method to get/set the Alignment for the column.
        /// </summary>
        public HorizontalAlignment this[int index]
        {
            get
            {
                // ensure that this is a valid column
                if (index >= Count) return HorizontalAlignment.Left;

                // get the current format for the column
                _hdItem.mask = W32_HDI.HDI_FORMAT;
                NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMW, index, ref _hdItem);

                // return the current setting
                if ((_hdItem.fmt & W32_HDF.HDF_CENTER) != 0)
                    return HorizontalAlignment.Center;
                else if ((_hdItem.fmt & W32_HDF.HDF_RIGHT) != 0)
                    return HorizontalAlignment.Right;
                else return HorizontalAlignment.Left;
            }
            set
            {
                // ensure that this is a valid column
                if (index < Count)
                {
                    // get the current format for the column
                    _hdItem.mask = W32_HDI.HDI_FORMAT;
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMW, index, ref _hdItem);

                    // turn off any existing alignment values
                    _hdItem.fmt &= W32_HDF.HDF_NOJUSTIFY;

                    // turn on the correct alignment
                    switch (value)
                    {
                        case HorizontalAlignment.Center:
                            _hdItem.fmt |= W32_HDF.HDF_CENTER;
                            break;
                        case HorizontalAlignment.Right:
                            _hdItem.fmt |= W32_HDF.HDF_RIGHT;
                            break;
                        default:
                            _hdItem.fmt |= W32_HDF.HDF_LEFT;
                            break;
                    }

                    // now update the column format
                    NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_SETITEMW, index, ref _hdItem);
                }
            }
        }

        /// <summary>
        /// Return the number of columns in the header.
        /// </summary>
        public int Count
        {
            get { return NativeMethods.SendMessage(_header.Handle, W32_HDM.HDM_GETITEMCOUNT, 0, IntPtr.Zero); }
        }
    }
}
