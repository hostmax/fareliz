﻿using SkyDean.FareLiz.Core.Presentation;
using SkyDean.FareLiz.WinForm.Presentation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public sealed class TaskbarFlightNotifier : TaskbarNotifierBase
    {
        private FlightItemsPanel _mainPanel = new FlightItemsPanel() { Dock = DockStyle.Fill };

        public void Show(string title, string header, IList<FlightMonitorItem> data, int timeToStay, NotificationType type, bool waitTillHidden)
        {
            if (waitTillHidden)
                WaitTillHidden();

            if (InvokeRequired)
                this.SafeInvoke(new Action(() => Show(title, header, data, timeToStay, type, false)));
            else
            {
                SetTitle(title);
                _mainPanel.Bind(header, data);
                Display(timeToStay, type);
            }
        }

        protected override void InitializeContentPanel(Panel contentPanel)
        {
            contentPanel.Controls.Add(_mainPanel);
        }

        protected override Size MeasureContentSize(int maxWidth, int maxHeight)
        {
            var expectedSize = _mainPanel.AutoResize(maxHeight);
            return expectedSize;
        }
    }
}
