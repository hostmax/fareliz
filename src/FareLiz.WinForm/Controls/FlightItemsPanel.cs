﻿using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.WinForm.Presentation;
using SkyDean.FareLiz.WinForm.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    public partial class FlightItemsPanel : UserControl
    {
        private readonly Pen _borderPen = new Pen(Brushes.DarkSlateGray) { DashStyle = DashStyle.Dot };
        private readonly Brush _newPriceBrush = Brushes.DeepSkyBlue;
        private readonly Brush _incPriceBrush = Brushes.DeepPink;
        private readonly Brush _decPriceBrush = Brushes.LimeGreen;
        private readonly Color _decPriceBackColor = Color.FromArgb(241, 255, 242);
        private readonly Color _incPriceBackColor = Color.FromArgb(255, 245, 242);
        private readonly Color _newPriceBackColor = Color.FromArgb(255, 255, 242);
        private readonly CultureInfo _numberCulture = new CultureInfo(CultureInfo.CurrentCulture.Name) { NumberFormat = new NumberFormatInfo { NumberDecimalSeparator = ".", NumberGroupSeparator = "," } };
        private readonly Font _priceFont;

        private int _hoverRow = -1;

        public int ExpectedHeight
        {
            get
            {
                int labelHeight = lblHeader.Height + lblHeader.Padding.Top + lblHeader.Padding.Bottom;
                int flightHeight = gvFlightItems.Rows.GetRowsHeight(DataGridViewElementStates.Visible);
                return labelHeight + flightHeight;
            }
        }

        public FlightItemsPanel()
        {
            InitializeComponent();
            colFirstPrice.DefaultCellStyle.Font = colSecondPrice.DefaultCellStyle.Font = _priceFont = new Font(Font, FontStyle.Bold);
            gvFlightItems.BackgroundColor = BackColor;
            gvFlightItems.CreateControl();
        }

        internal Size AutoResize(int maxHeight)
        {
            gvFlightItems.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            colIcon.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            colIcon.Width = colIcon.MinimumWidth;

            int targetHeight = ExpectedHeight;
            int borderWidth = 2 * SystemInformation.BorderSize.Width;

            int flightWidth = gvFlightItems.Columns.GetColumnsWidth(DataGridViewElementStates.Visible) + borderWidth;
            if (targetHeight >= maxHeight)
                flightWidth += SystemInformation.VerticalScrollBarWidth;
            int titleWidth = (lblHeader.Width + lblHeader.Padding.Left + lblHeader.Padding.Right);

            int targetWidth = (titleWidth > flightWidth ? titleWidth : flightWidth);

            var result = new Size(targetWidth, targetHeight);
            colIcon.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            return result;
        }

        public void Bind(string header, IList<FlightMonitorItem> data)
        {
            lblHeader.Text = header;
            lblHeader.AutoSize = !(String.IsNullOrEmpty(header));
            if (!lblHeader.AutoSize)
                lblHeader.Size = Size.Empty;

            gvFlightItems.SuspendLayout();

            try
            {
                gvFlightItems.Rows.Clear();
                DataGridViewRow[] rows = GetDataRows(data);
                if (rows != null)
                    gvFlightItems.Rows.AddRange(rows);
            }
            finally
            {
                gvFlightItems.ResumeLayout();
            }
        }

        private DataGridViewRow[] GetDataRows(IList<FlightMonitorItem> data)
        {
            int dataCount = (data == null ? 0 : data.Count);
            if (dataCount == 0)
                return null;

            var currency = data[0].FlightData.JourneyData.Currency;
            var currencySymbol = GlobalContext.MonitorEnvironment.CurrencyProvider.GetCurrencyInfo(currency).Symbol;
            var result = new DataGridViewRow[data.Count];
            string decimalSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

            for (int i = 0; i < dataCount; i++)
            {
                var item = data[i];
                var flightData = item.FlightData;

                var iconCell = new DataGridViewImageCell();
                var opCell = new DataGridViewTextBoxCell { Value = flightData.Operator };
                if (flightData.CanBePurchased)
                    opCell.Style.ForeColor = Color.SteelBlue;
                var durationCell = new DataGridViewTextBoxCell { Value = flightData.Duration.ToHourMinuteString() };

                double firstPrice = item.FlightStatus == FlightStatus.New ? flightData.Price : item.OldPrice;
                double roundedFirstPrice = Math.Round(firstPrice);
                var firstPriceCell = new DataGridViewTextBoxCell { Value = roundedFirstPrice };
                var firstPriceCurrencyCell = new DataGridViewTextBoxCell { Value = currencySymbol };

                var newRow = new DataGridViewRow { Tag = item };
                newRow.Cells.AddRange(iconCell, opCell, durationCell, firstPriceCell, firstPriceCurrencyCell);

                if (item.FlightStatus == FlightStatus.New)
                    newRow.DefaultCellStyle.BackColor = _newPriceBackColor;
                else
                {
                    newRow.DefaultCellStyle.BackColor = (item.FlightStatus == FlightStatus.PriceDecreased ? _decPriceBackColor : _incPriceBackColor);

                    // Price status
                    var img = (item.FlightStatus == FlightStatus.PriceDecreased ? Properties.Resources.DownArrow : Properties.Resources.UpArrow);
                    var statusCell = new DataGridViewImageCell { Value = img };

                    // New price
                    double roundedSecondPrice = Math.Round(flightData.Price);
                    var secondPriceCell = new DataGridViewTextBoxCell { Value = roundedSecondPrice };
                    var secondPriceCurrencyCell = new DataGridViewTextBoxCell { Value = currencySymbol };

                    newRow.Cells.AddRange(statusCell, secondPriceCell, secondPriceCurrencyCell);
                }
                result[i] = newRow;
            }

            return result;
        }

        private void gvFlightItems_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            var rect = e.CellBounds;
            if (rect.Bottom < 0 || rect.Right < 0)
                return;

            e.PaintBackground(rect, true);

            var cellColumn = gvFlightItems.Columns[e.ColumnIndex];

            // Draw off-target decimal part for the prices
            if (e.FormattedValue != null)
            {
                if (cellColumn == colFirstPrice || cellColumn == colSecondPriceCurrency)
                {
                    string val = e.FormattedValue.ToString();
                    if (val.Length > 0)
                    {
                        var font = e.CellStyle.Font;
                        var txtSize = TextRenderer.MeasureText(val, font);
                        int txtX = rect.X + (cellColumn == colFirstPrice ? 4 : -2);
                        int txtY = rect.Y + ((rect.Height - txtSize.Height) / 2) + 1;
                        e.Graphics.DrawString(val, e.CellStyle.Font, SystemBrushes.ControlText, txtX, txtY);
                    }
                }
                else
                    e.PaintContent(rect);
            }

            rect.Inflate(0, -1 * (int)_borderPen.Width);

            var y = rect.Y + rect.Height;   // Draw dotted bottom border
            e.Graphics.DrawLine(_borderPen, rect.X, y, rect.Right, y);
            if (e.RowIndex == 0)
                e.Graphics.DrawLine(_borderPen, rect.X, rect.Y, rect.Right, rect.Y);

            using (Pen edgePen = (cellColumn == colIcon) ? new Pen(ControlPaint.Dark(e.CellStyle.BackColor), 3) : null)
            {
                // Draw left border for all cells except for Operator cell and price section
                if (cellColumn == colIcon || cellColumn == colDuration || cellColumn == colFirstPrice)
                    e.Graphics.DrawLine(edgePen ?? _borderPen, rect.X, rect.Y, rect.X, rect.Bottom);
                else if (cellColumn == colSecondPriceCurrency)    // Draw right border for the last column
                    e.Graphics.DrawLine(_borderPen, rect.Right - 1, rect.Y, rect.Right - 1, rect.Bottom);
            }

            if (e.ColumnIndex == 0) // Draw arrow on the first column (this is a walk-around for the tooltip)
            {
                var flightItem = gvFlightItems.Rows[e.RowIndex].Tag as FlightMonitorItem;
                if (flightItem != null)
                {
                    int edgeSize = rect.Height / 3;
                    var status = flightItem.FlightStatus;
                    Brush brush = (status == FlightStatus.PriceIncreased ? _incPriceBrush : (status == FlightStatus.PriceDecreased ? _decPriceBrush : _newPriceBrush));

                    if (_hoverRow == e.RowIndex)
                    {
                        int arrowSize = rect.Height / 2;
                        int arrowSpan = (int)(arrowSize * 1.2 / 2);

                        int arrowXLeft = rect.Left - 1;
                        int arrowXMiddle = rect.Left + arrowSize - 1;

                        int arrowYTop = rect.Top + 2;
                        int arrowYMiddle = arrowYTop + arrowSpan;
                        int arrowYBottom = arrowYMiddle + arrowSpan;

                        var arrow = new Point[]
                            {
                                new Point(arrowXLeft, arrowYTop),
                                new Point(arrowXMiddle, arrowYMiddle),
                                new Point(arrowXLeft, arrowYBottom)
                            };

                        using (var arrowBrush = new SolidBrush(toolTip.BackColor))
                        {
                            e.Graphics.FillPolygon(arrowBrush, arrow);
                        }
                    }
                }
            }

            e.Handled = true;
        }

        private void gvFlightItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var flightItem = gvFlightItems.Rows[e.RowIndex].Tag as FlightMonitorItem;
            if (flightItem != null)
            {
                var data = flightItem.FlightData;
                if (data != null && data.CanBePurchased)
                {
                    try
                    {
                        BrowserUtils.Open(data.TravelAgency.Url);
                    }
                    catch (Exception ex)
                    {
                        string error = "Failed to launch web browser for ticket purchase: " + ex.Message;
                        GlobalContext.Logger.Error(error);
                        MessageBox.Show(error, "Ticket Purchase", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void gvFlightItems_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var flightItem = gvFlightItems.Rows[e.RowIndex].Tag as FlightMonitorItem;
            if (flightItem != null)
            {
                var data = flightItem.FlightData;
                DataGridViewRow row = gvFlightItems.Rows[e.RowIndex];
                bool canPurchase = data != null && data.CanBePurchased;

                if (canPurchase)
                {
                    row.Cells[0].Value = Properties.Resources.Purchase;
                    Cursor = Cursors.Hand;
                }
                else
                {
                    row.Cells[0].Value = null;
                    Cursor = Cursors.Default;
                }

                if (_hoverRow != e.RowIndex)
                {
                    var rowRect = gvFlightItems.GetRowDisplayRectangle(e.RowIndex, true);

                    _hoverRow = e.RowIndex;
                    string caption = (canPurchase ? "Double-click to buy this ticket:" : "This ticket cannot be bought at the moment!") + Environment.NewLine + data.SummaryString;

                    var rowScreenRect = gvFlightItems.RectangleToScreen(rowRect);
                    var relRowRect = lblHeader.RectangleToClient(rowScreenRect);
                    Point rowMiddleLeft = new Point(relRowRect.X, relRowRect.Y + (relRowRect.Height / 2));

                    // Show the tooltip on the edge of the panel
                    toolTip.Show(caption, this, rowMiddleLeft);
                }
            }
        }

        private void gvFlightItems_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            gvFlightItems.Rows[e.RowIndex].Cells[0].Value = null;
            Cursor = Cursors.Default;

            if (_hoverRow == e.RowIndex)
            {
                var mouseLoc = gvFlightItems.PointToClient(MousePosition);
                var rowRect = gvFlightItems.GetRowDisplayRectangle(_hoverRow, true);
                if (!rowRect.Contains(mouseLoc))
                {
                    toolTip.Hide(lblHeader);    // Hide tooltip if the mouse has changed to another row
                    _hoverRow = -1;
                }
            }

        }

        private void gvFlightItems_SelectionChanged(object sender, EventArgs e)
        {
            gvFlightItems.ClearSelection();
        }

        private void gvFlightItems_MouseEnter(object sender, EventArgs e)
        {
            gvFlightItems.Focus();
        }

        private void gvFlightItems_MouseLeave(object sender, EventArgs e)
        {
            var mousePos = MousePosition;
            var mouseLoc = gvFlightItems.PointToClient(mousePos);
            if (!gvFlightItems.Bounds.Contains(mouseLoc))   // The tooltip may have stolen the focus, thus this check is needed
            {
                if (_hoverRow > -1)
                {
                    gvFlightItems.InvalidateRow(_hoverRow);
                    _hoverRow = -1;
                }
                toolTip.Hide(lblHeader);
            }
        }
    }
}
