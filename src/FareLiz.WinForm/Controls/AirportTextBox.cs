﻿using SkyDean.FareLiz.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal class AirportTextBox : AutoCompleteTextbox<Airport>
    {
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public override IList<Airport> AutoCompleteList
        {
            get { return base.AutoCompleteList; }
            set { base.AutoCompleteList = value; }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public Airport SelectedAirport
        {
            get { return listBox.SelectedItem as Airport; }
            set
            {
                if (value != null)
                    Text = value.ToString();
                else
                    listBox.SelectedItem = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public string SelectedAirportCode
        {
            get { return SelectedAirport == null ? null : SelectedAirport.IATA; }
            set
            {
                if (value == null)
                    listBox.SelectedItem = null;
                else
                    foreach (var a in AutoCompleteList)
                    {
                        if (String.Equals(a.IATA, value, StringComparison.OrdinalIgnoreCase))
                        {
                            Text = a.ToString();
                            return;
                        }
                    }
            }
        }

        public AirportTextBox()
        {
            listBox.ItemHeight = listBox.Font.Height * 2 + 10; // 2 rows and padding              
            listBox.ValueMember = "IATA";
            listBox.DisplayMember = "";
            InitializeData();
        }

        internal void InitializeData()
        {
            InitializeData(Airport.Data);
        }

        internal void InitializeData(IList<Airport> data)
        {
            AutoCompleteList = data;
        }
    }
}
