using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// Summary description for FlatTabControl.
    /// </summary>
    [ToolboxBitmap(typeof(TabControl))] //,
    internal partial class FlatTabControl : TabControl
    {
        private const int _margin = 5, _overlap = 16, _radius = 16;
        private bool _bUpDown = false; // true when the button UpDown is required        

        #region Properties
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
        public int ActiveIndex
        {
            get { return GetActiveIndex(MousePosition, true); }
        }

        [Category("Appearance")]
        public new TabAlignment Alignment
        {
            get { return base.Alignment; }
            set
            {
                if (value > TabAlignment.Bottom)
                    base.Alignment = TabAlignment.Top;
                else
                    base.Alignment = value;
            }
        }

        [Category("Appearance")]
        public int IndicatorWidth
        {
            get { return _indicatorWidth; }
            set { _indicatorWidth = value; }
        }
        private int _indicatorWidth = 3;

        [Category("Appearance")]
        public new Point Padding
        {
            get { return base.Padding; }
            set
            {
                base.Padding = value;
                //	This line will trigger the handle to recreate, therefore invalidating the control
                if (value.X + (int)(_radius / 2) < -6)
                {
                    base.Padding = new Point(0, value.Y);
                }
                else
                {
                    base.Padding = new Point(value.X + (int)(_radius / 2) + 6, value.Y);
                }
            }
        }

        private bool _showTabBorders = true;
        [Category("Appearance"), DefaultValue(true)]
        public bool ShowTabBorders
        {
            get { return _showTabBorders; }
            set { _showTabBorders = value; }
        }

        private bool _useTabCloser = true;
        [Category("Appearance"), DefaultValue(true)]
        public bool UseTabCloser
        {
            get { return _useTabCloser; }
            set { _useTabCloser = value; }
        }

        private bool _useTabRefresher = true;
        [Category("Appearance"), DefaultValue(true)]
        public bool UseTabReloader
        {
            get { return _useTabRefresher; }
            set { _useTabRefresher = value; }
        }
        #endregion

        public FlatTabControl()
        {
            InitializeComponent();
            // double buffering
            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer
                | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor, true);
        }

        public int FindIndex(TabPage tabPage)
        {
            for (int i = 0; i < TabPages.Count; i++)
            {
                if (TabPages[i] == tabPage)
                    return i;
            }

            return -1;
        }

        public int GetActiveIndex(Point position, bool isRelativeToScreen)
        {
            TCHITTESTINFO hitTestInfo = new TCHITTESTINFO(isRelativeToScreen ? PointToClient(position) : position);
            return NativeMethods.SendMessage(this.Handle, NativeMethods.TCM_HITTEST, IntPtr.Zero, NativeMethods.ToIntPtr(hitTestInfo)).ToInt32();
        }

        private void DrawControl(Graphics g)
        {
            if (!Visible || !IsHandleCreated || IsDisposed || Disposing)
                return;

            g.SmoothingMode = SmoothingMode.HighSpeed;
            var tabControlArea = this.ClientRectangle;
            var tabArea = this.DisplayRectangle;

            //----------------------------
            // fill client area
            using (var br = new SolidBrush(BackColor))
                g.FillRectangle(br, tabControlArea);
            //----------------------------

            //----------------------------
            // draw border
            if (ShowTabBorders)
            {
                int nDelta = SystemInformation.Border3DSize.Width;
                tabArea.Inflate(nDelta, nDelta);
                g.DrawRectangle(SystemPens.ControlDark, tabArea);
            }
            //----------------------------

            //----------------------------
            // clip region for drawing tabs
            Region rSaved = g.Clip;
            int nWidth = tabArea.Width + _margin;
            if (_bUpDown)
            {
                // exclude updown control for painting
                if (NativeMethods.IsWindowVisible(scUpDown.Handle))
                {
                    Rectangle rupdown = new Rectangle();
                    NativeMethods.GetWindowRect(scUpDown.Handle, ref rupdown);
                    Rectangle rupdown2 = RectangleToClient(rupdown);
                    nWidth = rupdown2.X;
                }
            }

            var rReg = new Rectangle(tabArea.Left, tabControlArea.Top, nWidth - _margin, tabControlArea.Height);
            g.SetClip(rReg);

            // draw tabs
            for (int i = TabCount - 1; i >= 0; i--)
                if (i != SelectedIndex)
                    DrawTab(g, this.TabPages[i], i);

            if (SelectedIndex > -1)
                DrawTab(g, TabPages[SelectedIndex], SelectedIndex);

            g.Clip = rSaved;
            //----------------------------

            //----------------------------
            // draw background to cover flat border areas
            if (ShowTabBorders)
            {
                if (this.SelectedTab != null)
                {
                    TabPage tabPage = this.SelectedTab;
                    Color color = tabPage.BackColor;
                    using (Pen border = new Pen(color))
                    {
                        tabArea.Offset(1, 1);
                        tabArea.Width -= 2;
                        tabArea.Height -= 2;

                        g.DrawRectangle(border, tabArea);
                        tabArea.Width -= 1;
                        tabArea.Height -= 1;
                        g.DrawRectangle(border, tabArea);
                    }
                }
            }
            //----------------------------
        }

        internal class TabpageExCollectionEditor : CollectionEditor
        {
            public TabpageExCollectionEditor(Type type)
                : base(type)
            {
            }

            protected override Type CreateCollectionItemType()
            {
                return typeof(TabPage);
            }
        }
    }

    internal delegate void TabPageEventHandler(TabPage sender, EventArgs args);
}
