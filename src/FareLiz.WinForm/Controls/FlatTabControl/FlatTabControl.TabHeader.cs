﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal partial class FlatTabControl
    {
        private int _inactiveTabDiff = 3;

        private void PaintTab(TabPage tabPage, int index, Graphics graphics)
        {
            using (GraphicsPath tabpath = GetTabBorder(index))
            {
                using (Brush fillBrush = GetTabBackgroundBrush(index))
                {
                    //	Paint the background
                    graphics.FillPath(fillBrush, tabpath);

                    //	Paint a focus indication
                    if (Focused)
                    {
                        using (var brush = new SolidBrush(ControlPaint.Dark(tabPage.BackColor)))
                        {
                            DrawTabFocusIndicator(brush, tabpath, index, graphics);
                        }
                    }
                }
            }
        }

        private void DrawTab(Graphics g, TabPage tabPage, int nIndex)
        {
            //----------------------------
            // draw border
            if (ShowTabBorders)
            {
                using (GraphicsPath tabPageBorderPath = GetTabPageBorder(nIndex))
                {
                    using (var br = GetPageBackgroundBrush(nIndex))
                    {
                        g.FillPath(br, tabPageBorderPath);
                    }
                    PaintTab(tabPage, nIndex, g);

                    // Draw tab border
                    using (var borderPen = new Pen(ControlPaint.Dark(tabPage.BackColor)))
                    {
                        g.DrawPath(borderPen, tabPageBorderPath);
                    }
                }
            }

            bool bSelected = (SelectedIndex == nIndex);

            Rectangle recBounds = GetTabRect(nIndex);
            RectangleF tabTextArea = (RectangleF)GetTabRect(nIndex);
            var closerRect = GetCloserRect(nIndex);
            var refresherRect = GetReloaderRect(nIndex);

            if (UseTabCloser)
                tabTextArea.Width -= closerRect.Width;
            if (UseTabReloader)
                tabTextArea.Width -= refresherRect.Width;
            tabTextArea.Width -= (_overlap + _margin);

            //----------------------------
            // draw tab's icon            
            bool hasIcon = (tabPage.ImageIndex >= 0 && ImageList != null && tabPage.ImageIndex < ImageList.Images.Count);
            if (hasIcon)
            {
                var tabImage = ImageList.Images[tabPage.ImageIndex];
                if (tabImage != null)
                {
                    using (tabImage)
                    {
                        int nLeftMargin = 14 + (bSelected ? 2 : 0);
                        const int nRightMargin = 2;
                        int nWidth = tabImage.Width - (bSelected ? 0 : 2);
                        int nHeight = tabImage.Height - (bSelected ? 0 : 2);

                        Rectangle rimage = new Rectangle(recBounds.X + nLeftMargin, recBounds.Y + 1, nWidth, nHeight);

                        // adjust rectangles
                        float nAdj = (float)(nLeftMargin + tabImage.Width + nRightMargin);

                        rimage.Y += (recBounds.Height - tabImage.Height) / 2;
                        tabTextArea.X += nAdj;
                        tabTextArea.Width -= nAdj;

                        // draw icon
                        g.DrawImage(tabImage, rimage);
                    }
                }
            }
            else
                tabTextArea.Width += (UseTabCloser ? closerRect.Width : 0)
                    + (UseTabReloader ? refresherRect.Width : 0);

            //----------------------------

            //----------------------------
            // draw string
            using (var stringFormat = new StringFormat())
            {
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                using (var br = new SolidBrush(tabPage.ForeColor))
                {
                    using (var tabFont = new Font(Font, bSelected ? FontStyle.Bold : FontStyle.Regular))
                    {
                        g.DrawString(tabPage.Text, tabFont, br, tabTextArea, stringFormat);
                    }
                }
            }

            //	Draw the closer
            DrawCloser(nIndex, g);
            DrawRefresher(nIndex, g);
        }

        private Brush GetTabBackgroundBrush(int index)
        {
            var curTab = TabPages[index];

            //	Capture the colours dependant on selection RequestState of the tab
            Color brushColor = Color.Empty;

            if (SelectedIndex == index)
                brushColor = Color.White;
            else if (!curTab.Enabled)
                brushColor = SystemColors.ControlDark;
            else if (HotTrack && index == ActiveIndex)
            {
                var scRect = GetUpDownRect();
                var mousePos = PointToClient(MousePosition);
                bool overUpDown = scRect.Contains(mousePos);
                if (!overUpDown)
                    brushColor = Color.FromArgb(40, curTab.BackColor);
            }
            else
                brushColor = Color.FromArgb(10, curTab.BackColor);

            return new SolidBrush(brushColor);
        }

        private void AddTabBorder(GraphicsPath path, Rectangle tabBounds)
        {
            int spread;
            int eigth;
            int sixth;
            int quarter;

            if (Alignment <= TabAlignment.Bottom)
            {
                spread = (int)Math.Floor((decimal)tabBounds.Height * 2 / 3);
                eigth = (int)Math.Floor((decimal)tabBounds.Height * 1 / 8);
                sixth = (int)Math.Floor((decimal)tabBounds.Height * 1 / 6);
                quarter = (int)Math.Floor((decimal)tabBounds.Height * 1 / 4);
            }
            else
            {
                spread = (int)Math.Floor((decimal)tabBounds.Width * 2 / 3);
                eigth = (int)Math.Floor((decimal)tabBounds.Width * 1 / 8);
                sixth = (int)Math.Floor((decimal)tabBounds.Width * 1 / 6);
                quarter = (int)Math.Floor((decimal)tabBounds.Width * 1 / 4);
            }

            switch (Alignment)
            {
                case TabAlignment.Top:
                    path.AddCurve(new Point[] {  new Point(tabBounds.X, tabBounds.Bottom)
					              		,new Point(tabBounds.X + sixth, tabBounds.Bottom - eigth)
					              		,new Point(tabBounds.X + spread - quarter, tabBounds.Y + eigth)
					              		,new Point(tabBounds.X + spread, tabBounds.Y)});
                    path.AddLine(tabBounds.X + spread, tabBounds.Y, tabBounds.Right - spread, tabBounds.Y);
                    path.AddCurve(new Point[] {  new Point(tabBounds.Right - spread, tabBounds.Y)
					              		,new Point(tabBounds.Right - spread + quarter, tabBounds.Y + eigth)
					              		,new Point(tabBounds.Right - sixth, tabBounds.Bottom - eigth)
					              		,new Point(tabBounds.Right, tabBounds.Bottom)});
                    break;
                case TabAlignment.Bottom:
                    path.AddCurve(new Point[] {  new Point(tabBounds.Right, tabBounds.Y)
					              		,new Point(tabBounds.Right - sixth, tabBounds.Y + eigth)
					              		,new Point(tabBounds.Right - spread + quarter, tabBounds.Bottom - eigth)
					              		,new Point(tabBounds.Right - spread, tabBounds.Bottom)});
                    path.AddLine(tabBounds.Right - spread, tabBounds.Bottom, tabBounds.X + spread, tabBounds.Bottom);
                    path.AddCurve(new Point[] {  new Point(tabBounds.X + spread, tabBounds.Bottom)
					              		,new Point(tabBounds.X + spread - quarter, tabBounds.Bottom - eigth)
					              		,new Point(tabBounds.X + sixth, tabBounds.Y + eigth)
					              		,new Point(tabBounds.X, tabBounds.Y)});
                    break;
                case TabAlignment.Left:
                    path.AddCurve(new Point[] {  new Point(tabBounds.Right, tabBounds.Bottom)
					              		,new Point(tabBounds.Right - eigth, tabBounds.Bottom - sixth)
					              		,new Point(tabBounds.X + eigth, tabBounds.Bottom - spread + quarter)
					              		,new Point(tabBounds.X, tabBounds.Bottom - spread)});
                    path.AddLine(tabBounds.X, tabBounds.Bottom - spread, tabBounds.X, tabBounds.Y + spread);
                    path.AddCurve(new Point[] {  new Point(tabBounds.X, tabBounds.Y + spread)
					              		,new Point(tabBounds.X + eigth, tabBounds.Y + spread - quarter)
					              		,new Point(tabBounds.Right - eigth, tabBounds.Y + sixth)
					              		,new Point(tabBounds.Right, tabBounds.Y)});

                    break;
                case TabAlignment.Right:
                    path.AddCurve(new Point[] {  new Point(tabBounds.X, tabBounds.Y)
					              		,new Point(tabBounds.X + eigth, tabBounds.Y + sixth)
					              		,new Point(tabBounds.Right - eigth, tabBounds.Y + spread - quarter)
					              		,new Point(tabBounds.Right, tabBounds.Y + spread)});
                    path.AddLine(tabBounds.Right, tabBounds.Y + spread, tabBounds.Right, tabBounds.Bottom - spread);
                    path.AddCurve(new Point[] {  new Point(tabBounds.Right, tabBounds.Bottom - spread)
					              		,new Point(tabBounds.Right - eigth, tabBounds.Bottom - spread + quarter)
					              		,new Point(tabBounds.X + eigth, tabBounds.Bottom - sixth)
					              		,new Point(tabBounds.X, tabBounds.Bottom)});
                    break;
            }
        }

        private void DrawTabFocusIndicator(Brush brush, GraphicsPath tabpath, int index, Graphics graphics)
        {
            if (Focused && index == SelectedIndex)
            {
                RectangleF pathRect = tabpath.GetBounds();
                Rectangle focusRect = Rectangle.Empty;
                switch (Alignment)
                {
                    case TabAlignment.Top:
                        focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Y, (int)pathRect.Width, IndicatorWidth);
                        break;
                    case TabAlignment.Bottom:
                        focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Bottom - IndicatorWidth, (int)pathRect.Width, IndicatorWidth);
                        break;
                    case TabAlignment.Left:
                        focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Y, IndicatorWidth, (int)pathRect.Height);
                        break;
                    case TabAlignment.Right:
                        focusRect = new Rectangle((int)pathRect.Right - IndicatorWidth, (int)pathRect.Y, IndicatorWidth, (int)pathRect.Height);
                        break;
                }

                //	Ensure the focus stip does not go outside the tab
                using (var focusRegion = new Region(focusRect))
                {
                    focusRegion.Intersect(tabpath);
                    graphics.FillRegion(brush, focusRegion);
                }
            }
        }

        private GraphicsPath GetTabBorder(int index)
        {
            GraphicsPath path = new GraphicsPath();
            Rectangle tabBounds = GetTabRect(index);

            AddTabBorder(path, tabBounds);

            path.CloseFigure();
            return path;
        }

        public virtual new Rectangle GetTabRect(int index)
        {
            if (index < 0)
                return Rectangle.Empty;

            Rectangle tabBounds = base.GetTabRect(index);
            if (RightToLeftLayout)
                tabBounds.X = Width - tabBounds.Right;

            //	Expand to overlap the tabpage
            switch (Alignment)
            {
                case TabAlignment.Top:
                    tabBounds.Height += 2;
                    break;
                case TabAlignment.Bottom:
                    tabBounds.Height += 2;
                    tabBounds.Y -= 2;
                    break;
                case TabAlignment.Left:
                    tabBounds.Width += 2;
                    break;
                case TabAlignment.Right:
                    tabBounds.X -= 2;
                    tabBounds.Width += 2;
                    break;
            }

            //	Create Overlap unless first tab in the row to align with tabpage
            if (_overlap > 0)
            {
                if (Alignment <= TabAlignment.Bottom)
                {
                    tabBounds.Width += _overlap;
                }
                else
                {
                    tabBounds.Y -= _overlap;
                    tabBounds.Height += _overlap;
                }
            }

            int selIndex = SelectedIndex;
            if (index != selIndex) // Lower unselected tab, but keep the same height so that it does not shrink
                tabBounds.Y += _inactiveTabDiff;

            if (_isDragging)
            {
                if (index == selIndex) // Shift the dragging tab
                    tabBounds.X += _dragOffset;
            }

            return tabBounds;
        }

        private Rectangle GetCloserRect(int index)
        {
            Rectangle closerRect;
            var rect = GetTabRect(index);

            //	Make it shorter or thinner to fit the height or width because of the padding added to the tab for painting
            int closerSize = rect.Height - 12;
            if (Alignment <= TabAlignment.Bottom)   // Top or Bottom
            {
                int offsetX = Padding.X + closerSize;
                if (RightToLeftLayout)
                    closerRect = new Rectangle((int)rect.Left + offsetX, (int)rect.Y + (int)Math.Floor((double)((int)rect.Height - closerSize) / 2), closerSize, closerSize);
                else
                    closerRect = new Rectangle((int)rect.Right - offsetX, (int)rect.Y + (int)Math.Floor((double)((int)rect.Height - closerSize) / 2), closerSize, closerSize);
            }
            else
            {
                int offsetY = Padding.Y + closerSize;
                if (RightToLeftLayout)
                    closerRect = new Rectangle((int)rect.X + (int)Math.Floor((double)((int)rect.Width - closerSize) / 2), (int)rect.Top + offsetY, closerSize, closerSize);
                else
                    closerRect = new Rectangle((int)rect.X + (int)Math.Floor((double)((int)rect.Width - closerSize) / 2), (int)rect.Bottom - offsetY, closerSize, closerSize);
            }

            if (index != SelectedIndex)
            {
                int diff = _inactiveTabDiff / 2;
                closerRect.Height -= diff;
                closerRect.Width -= diff;
                closerRect.Y -= diff / 2;
            }

            return closerRect;
        }

        private static GraphicsPath GetCloserPath(Rectangle closerRect)
        {
            GraphicsPath closerPath = new GraphicsPath();
            int offset = 4;
            var crossRect = new Rectangle(closerRect.X + offset, closerRect.Y + offset, closerRect.Width - 2 * offset, closerRect.Height - 2 * offset);
            closerPath.AddLine(crossRect.X, crossRect.Y, crossRect.Right, crossRect.Bottom);
            closerPath.CloseFigure();
            closerPath.AddLine(crossRect.Right, crossRect.Y, crossRect.X, crossRect.Bottom);
            closerPath.CloseFigure();

            return closerPath;
        }

        private void DrawCloser(int index, Graphics graphics)
        {
            if (!UseTabCloser)
                return;

            Rectangle closerRect = GetCloserRect(index);
            var curMouse = PointToClient(MousePosition);
            bool mouseOverCloser = closerRect.Contains(curMouse);

            using (GraphicsPath closerPath = GetCloserPath(closerRect))
            {
                if (mouseOverCloser)
                {
                    var oldMode = graphics.SmoothingMode;
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.FillEllipse(Brushes.DarkSalmon, closerRect);
                    graphics.SmoothingMode = oldMode;
                }

                Color closerColor = (mouseOverCloser ? Color.White : Color.LightSlateGray);
                using (var closerPen = new Pen(closerColor, 2))
                {
                    graphics.DrawPath(closerPen, closerPath);
                }
            }
        }

        private Rectangle GetReloaderRect(int index)
        {
            var refresherRect = GetCloserRect(index);

            if (UseTabCloser)
                refresherRect.X -= refresherRect.Width;

            if (index != SelectedIndex)
            {
                int diff = _inactiveTabDiff / 2;
                refresherRect.Height -= diff;
                refresherRect.Width -= diff;
                refresherRect.Y -= diff / 2;
            }

            return refresherRect;
        }

        private void DrawRefresher(int index, Graphics graphics)
        {
            if (!UseTabReloader)
                return;

            var refresherRect = GetReloaderRect(index);

            var curMouse = PointToClient(MousePosition);
            bool mouseOverRefresher = refresherRect.Contains(curMouse);
            if (mouseOverRefresher)
            {
                var oldMode = graphics.SmoothingMode;
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.FillEllipse(Brushes.DeepSkyBlue, refresherRect);
                graphics.SmoothingMode = oldMode;
            }

            Image img = (mouseOverRefresher ? Properties.Resources.Refresh_White : Properties.Resources.Refresh);
            // draw icon
            graphics.DrawImage(img, refresherRect);
        }
    }
}
