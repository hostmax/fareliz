﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using SkyDean.FareLiz.Core.Utils.WinNative;
using SkyDean.FareLiz.WinForm.Utils;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal partial class FlatTabControl
    {
        private SubClass scUpDown = null;

        private void FindUpDown()
        {
            bool bFound = false;

            // find the UpDown control
            IntPtr pWnd = NativeMethods.GetWindow(Handle, NativeMethods.GW_CHILD);

            while (pWnd != IntPtr.Zero)
            {
                //----------------------------
                // Get the window class name
                char[] className = new char[33];
                int length = NativeMethods.GetClassName(pWnd, className, 32);
                string s = new string(className, 0, length);
                //----------------------------

                if (s == "msctls_updown32")
                {
                    bFound = true;

                    if (!_bUpDown)
                    {
                        //----------------------------
                        // Subclass it
                        scUpDown = new SubClass(pWnd, true);
                        scUpDown.SubClassedWndProc += new SubClass.SubClassWndProcEventHandler(scUpDown_SubClassedWndProc);
                        //----------------------------

                        _bUpDown = true;
                    }
                    break;
                }

                pWnd = NativeMethods.GetWindow(pWnd, NativeMethods.GW_HWNDNEXT);
            }

            if ((!bFound) && (_bUpDown))
                _bUpDown = false;
        }

        private void UpdateUpDown()
        {
            if (_bUpDown)
            {
                if (NativeMethods.IsWindowVisible(scUpDown.Handle))
                {
                    Rectangle rect = new Rectangle();
                    NativeMethods.GetClientRect(scUpDown.Handle, ref rect);
                    NativeMethods.InvalidateRect(scUpDown.Handle, ref rect, true);
                }
            }
        }

        private Rectangle GetUpDownRect()
        {
            if (!_bUpDown)
                return Rectangle.Empty;

            Rectangle pageBound = GetPageBounds(TabCount - 1);
            Rectangle scRect = new Rectangle();
            NativeMethods.GetClientRect(scUpDown.Handle, ref scRect);
            Rectangle relRect = new Rectangle(ClientRectangle.Width - scRect.Width, pageBound.Y - scRect.Y - scRect.Height - 1, scRect.Width, scRect.Height);
            return relRect;
        }

        private void DrawUpDown(Graphics g)
        {
            if (TabCount < 1 || (leftRightImages == null) || (leftRightImages.Images.Count != 4))
                return;

            //----------------------------
            // calc positions
            Rectangle TabControlArea = ClientRectangle;
            Rectangle scRect = new Rectangle();
            NativeMethods.GetClientRect(scUpDown.Handle, ref scRect);

            g.FillRectangle(SystemBrushes.ControlLight, scRect);
            using (var p = PaintUtils.GetRoundedRectanglePath(scRect, 3, 1))
            {
                g.DrawPath(SystemPens.ControlDark, p);
            }

            int imgWidth = leftRightImages.ImageSize.Width, imgHeight = leftRightImages.ImageSize.Height;
            int nMiddle = (scRect.Width) / 2;
            int nTop = (scRect.Height - leftRightImages.ImageSize.Height) / 2;
            int nLeft = (nMiddle - leftRightImages.ImageSize.Width) / 2;
            //----------------------------

            //----------------------------
            // draw scroll button
            Image img = null;
            Rectangle firstTabRect = GetTabRect(0);
            Rectangle leftButtonRect = new Rectangle(nLeft, nTop, imgWidth, imgHeight);
            if (firstTabRect.Left < TabControlArea.Left || firstTabRect.Width < 0)
                img = leftRightImages.Images[1];  // Left (Enabled) image
            else
                img = leftRightImages.Images[3];

            if (img != null)
                using (img)
                    g.DrawImage(img, leftButtonRect);

            Rectangle lastTabRect = GetTabRect(TabCount - 1);
            Rectangle rightButtonRect = new Rectangle(nMiddle + nLeft, nTop, imgWidth, imgHeight);
            if (lastTabRect.Right > (TabControlArea.Width - scRect.Width))
                img = leftRightImages.Images[0];  // Right (Enabled) image
            else
                img = leftRightImages.Images[2];

            if (img != null)
                using (img)
                    g.DrawImage(img, rightButtonRect);
            //----------------------------
        }

        private int scUpDown_SubClassedWndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case (int)W32_WM.WM_PAINT:
                    {
                        //------------------------
                        // redraw
                        IntPtr hDC = NativeMethods.GetWindowDC(scUpDown.Handle);
                        using (var g = Graphics.FromHdc(hDC))
                        {
                            DrawUpDown(g);
                        }
                        NativeMethods.ReleaseDC(scUpDown.Handle, hDC);
                        //------------------------

                        // return 0 (processed)
                        m.Result = IntPtr.Zero;

                        //------------------------
                        // validate current rect
                        Rectangle rect = new Rectangle();

                        NativeMethods.GetClientRect(scUpDown.Handle, ref rect);
                        NativeMethods.ValidateRect(scUpDown.Handle, ref rect);
                        //------------------------
                    }
                    return 1;
            }

            return 0;
        }
    }
}
