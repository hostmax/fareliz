﻿using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal partial class FlatTabControl
    {
        [Category("Action")]
        public event TabControlCancelEventHandler TabPageClosing;

        [Category("Action")]
        public event TabControlEventHandler TabPageReloading;

        [Category("Action")]
        public event ScrollEventHandler TabScrolling;

        private int _oldScrollValue;
        private int _lastActiveIndex = -1;  // Keep track of last active index in order to reduce redundant poainting        
        private const int MouseDragThreshold = 5;

        private Point _dragStartLocation = Point.Empty;
        private Rectangle _dragTabRect = Rectangle.Empty;
        private int _dragOffset = 0;
        private int _dragCompensate = 0;
        private int _droppingIndex = -1;
        private bool _isDragging = false;

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            FindUpDown();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (!DesignMode)
            {
                if (!_isDragging)
                {
                    int index = ActiveIndex;
                    if (index < 0)
                        return;

                    var mouseLoc = PointToClient(MousePosition);

                    if (UseTabReloader && TabPageReloading != null && GetReloaderRect(index).Contains(mouseLoc))
                    {
                        ReloadTab(index);
                        return;
                    }

                    // Tab Closer should be handled at last since it may remove the tab!
                    if (UseTabCloser && GetCloserRect(index).Contains(mouseLoc))
                    {
                        CloseTab(index);
                        return;
                    }

                    if (e.Button == MouseButtons.Right)
                    {
                        tabPageContextMenu.Tag = index; // Keep track of the affected tab
                        tabPageContextMenu.Show(this, e.Location);
                    }
                }
                //	Fire the base event
                base.OnMouseClick(e);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (AllowDrop && e.Button == MouseButtons.Left)
            {
                _dragStartLocation = e.Location;
                _dragOffset = _dragCompensate = 0;
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (AllowDrop && Alignment <= TabAlignment.Bottom && e.Button == MouseButtons.Left)
            {
                if (SelectedIndex != -1)
                {
                    // Measure how far has the mouse dragged
                    _dragOffset = e.Location.X - _dragStartLocation.X;

                    if (_isDragging)
                    {
                        var mouseLoc = e.Location;
                        bool dragRight = (_dragOffset > 0); // Mouse move to the right
                        Point testPoint = (dragRight
                            ? new Point(_dragTabRect.Right, _dragTabRect.Height / 2)
                            : new Point(_dragTabRect.Left, _dragTabRect.Height / 2));

                        _dragOffset += _dragCompensate;
                        _dragTabRect = GetTabRect(SelectedIndex);
                        // Debug.WriteLine("Dragging tab " + SelectedIndex + " to the " + (dragRight ? "right" : "left") + " by " + _dragOffset + "px at location: " + _dragTabRect);
                        int index = GetActiveIndex(testPoint, false);
                        if (index == SelectedIndex)
                            _droppingIndex = -1;
                        else
                            _droppingIndex = index;

                        if (index != -1)
                        {
                            bool canSwitch = ((dragRight && index > SelectedIndex) || (!dragRight && index < SelectedIndex));
                            if (canSwitch)
                            {
                                var toRect = GetTabRect(index);
                                Rectangle testRect;
                                var minSlideDistance = (int)(toRect.Width * 0.35);

                                if (dragRight)  // Dragging to the right?
                                    testRect = new Rectangle(toRect.X + minSlideDistance, toRect.Y, minSlideDistance, toRect.Height);
                                else
                                    testRect = new Rectangle(toRect.X, toRect.Y, toRect.Width - minSlideDistance, toRect.Height);

                                if (testRect.Contains(testPoint))    // The edge of dragging edge moved fare enough?
                                {
                                    // Debug.WriteLine(String.Format(CultureInfo.InvariantCulture, "Switching tab {0} and {1}", SelectedIndex, index));
                                    // Swap tab
                                    var fromTab = TabPages[SelectedIndex];
                                    var toTab = TabPages[index];
                                    int newOffset = _dragOffset;
                                    _dragOffset = 0;
                                    var fromTabOriginalRect = GetTabRect(SelectedIndex);
                                    int offsetDiff = fromTabOriginalRect.X - toRect.X;
                                    _dragOffset = newOffset + offsetDiff;
                                    SuspendLayout();
                                    TabPages[SelectedIndex] = toTab;
                                    TabPages[index] = fromTab;
                                    SelectedIndex = index;
                                    _dragTabRect = GetTabRect(SelectedIndex);
                                    _droppingIndex = -1;
                                    _dragStartLocation = e.Location;
                                    _dragCompensate = _dragOffset;  // So that the next calculation will persist the same drag offset
                                    ResumeLayout();
                                }
                            }

                            Cursor = Cursors.Default;
                        }
                        else
                            Cursor = Cursors.No;
                    }
                    else
                    {
                        if ((Math.Abs(_dragOffset) >= MouseDragThreshold))
                        {
                            _isDragging = true;
                            _dragTabRect = GetTabRect(SelectedIndex);
                        }
                    }
                }
            }

            bool needRefresh = _isDragging;
            if (!needRefresh)
            {
                var index = ActiveIndex;
                if (_lastActiveIndex == index)
                {
                    var mouseLoc = new Point(e.X, e.Y);
                    // Check if button is being hovered
                    if ((UseTabReloader && GetReloaderRect(index).Contains(mouseLoc))
                        || (UseTabCloser && GetCloserRect(index).Contains(mouseLoc)))
                        needRefresh = true;
                }
                else
                {
                    needRefresh = true;
                    _lastActiveIndex = index;
                }
            }

            if (needRefresh)
                Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            CancelDrag();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            CancelDrag();
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawControl(e.Graphics);
        }

        protected virtual void OnHScroll(ScrollEventArgs e)
        {
            //	repaint the moved tabs
            this.Invalidate();

            //	Raise the event
            if (TabScrolling != null)
                TabScrolling(this, e);

            if (e.Type == ScrollEventType.EndScroll)
                this._oldScrollValue = e.NewValue;
        }

        protected override void OnDeselecting(TabControlCancelEventArgs e)
        {
            var mouseLoc = PointToClient(MousePosition);
            var index = ActiveIndex;
            if (index != e.TabPageIndex)
                if ((UseTabReloader && TabPageReloading != null && GetReloaderRect(index).Contains(mouseLoc))
                    || (UseTabCloser && GetCloserRect(index).Contains(mouseLoc)))
                {
                    e.Cancel = true;
                }

            // Tab Closer should be handled at last since it may remove the tab!
            base.OnDeselecting(e);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == (int)W32_WM.WM_HSCROLL)
            {
                //	Raise the scroll event when the scroller is scrolled
                OnHScroll(new ScrollEventArgs(((ScrollEventType)NativeMethods.LoWord(m.WParam)), _oldScrollValue, NativeMethods.HiWord(m.WParam), ScrollOrientation.HorizontalScroll));
            }
        }

        private void CloseTab(int index)
        {
            //	If we are clicking on a closer then remove the tab instead of raising the standard mouse click event. But raise the tab closing event first
            TabPage tab = TabPages[index];
            if (TabPageClosing != null)
            {
                var args = new TabControlCancelEventArgs(tab, index, false, TabControlAction.Deselecting);
                TabPageClosing(this, args);
                if (args.Cancel)
                    return;
            }
            this.TabPages.Remove(tab);
            tab.Dispose();
        }

        private void FlatTabControl_ControlAdded(object sender, ControlEventArgs e)
        {
            FindUpDown();
            UpdateUpDown();
        }

        private void FlatTabControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            FindUpDown();
            UpdateUpDown();
        }

        private void FlatTabControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F4)  // Close the tab
            {
                var selTabIdx = SelectedIndex;
                if (selTabIdx > -1)
                {
                    CloseTab(selTabIdx);
                }
            }
        }

        private void FlatTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUpDown();
            Invalidate();	// we need to update border and background colors
        }

        private void CancelDrag()
        {
            _dragTabRect = Rectangle.Empty;
            _dragStartLocation = Point.Empty;
            _dragOffset = 0;
            _droppingIndex = -1;
            Cursor = Cursors.Default;
            if (_isDragging)
            {
                _isDragging = false;
                Invalidate();
            }
        }

        private void ReloadTab(int index)
        {
            if (TabPageReloading != null)
            {
                TabPage tab = TabPages[index];
                var args = new TabControlEventArgs(tab, index, TabControlAction.Selecting);
                TabPageReloading(this, args);
            }
        }

        private void TabPageContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            int count = TabPages.Count;
            mnuCloseAllTabs.Available = UseTabCloser && count > 1;
            mnuCloseLeftTabs.Available = UseTabCloser && count > 1 && activeIdx > 0;
            mnuCloseRightTabs.Available = UseTabCloser && count > 1 && activeIdx < TabPages.Count - 1;
            mnuReload.Available = (UseTabReloader && TabPageReloading != null);
        }

        private void mnuCloseTab_Click(object sender, EventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            CloseTab(activeIdx);
        }

        private void mnuCloseAllTabs_Click(object sender, EventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            SuspendLayout();
            try
            {
                int totalCount = TabPages.Count;
                for (int i = 0; i < totalCount; i++)
                    CloseTab(0);
            }
            finally
            {
                ResumeLayout();
            }
        }

        private void mnuCloseLeftTabs_Click(object sender, EventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            SuspendLayout();
            try
            {
                for (int i = 0; i < activeIdx; i++)
                    CloseTab(0);
            }
            finally
            {
                ResumeLayout();
            }
        }

        private void mnuCloseRightTabs_Click(object sender, EventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            SuspendLayout();
            try
            {
                for (int i = activeIdx + 1; i < TabPages.Count; )
                    CloseTab(i);
            }
            finally
            {
                ResumeLayout();
            }
        }

        private void mnuReload_Click(object sender, EventArgs e)
        {
            int activeIdx = (int)tabPageContextMenu.Tag;
            ReloadTab(activeIdx);
        }
    }
}
