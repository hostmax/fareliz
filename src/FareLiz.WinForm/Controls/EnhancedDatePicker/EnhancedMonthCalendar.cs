﻿using SkyDean.FareLiz.Core.Utils.WinNative;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    /// <summary>
    /// A highly customizable and culture dependent month calendar control.
    /// </summary>
    [Designer(typeof(Design.MonthCalendarControlDesigner))]
    [DefaultEvent("DateSelected")]
    [DefaultProperty("ViewStart")]
    [ToolboxBitmap(typeof(System.Windows.Forms.MonthCalendar))]
    public sealed partial class EnhancedMonthCalendar : Control
    {
        #region Fields
        private readonly BoldedDatesCollection _boldDatesCollection = new BoldedDatesCollection();
        private const int SETREDRAW = 11;
        private bool extendSelection = false;
        private bool selectionStarted;

        private Point mouseLocation = Point.Empty;
        private Rectangle currentMoveBounds = Rectangle.Empty;

        private int _dayWidth;
        private int _dayHeight;
        private int _dayNameHeight;
        private int _monthWidth;
        private int _monthHeight;
        private int _weekNumberWidth;
        private int _headerHeight;
        private int _footerHeight;

        private DateTime _lastVisibleDate;
        private DateTime _monthSelected;
        private DateTime _yearSelected;

        private Rectangle _footerRect;
        private Rectangle _leftArrowRect;
        private Rectangle _rightArrowRect;
        private MonthCalendarRenderer _renderer;

        /// <summary>
        /// The selection start range if in week selection mode.
        /// </summary>
        private SelectionRange _selectionStartRange;

        /// <summary>
        /// The selection range for backup purposes.
        /// </summary>
        private SelectionRange _backupRange;

        /// <summary>
        /// Indicates that an menu is currently displayed.
        /// </summary>
        private bool _showingMenu;

        /// <summary>
        /// Indicates whether the control is calculating it's sizes.
        /// </summary>
        private bool _inUpdate;
        #endregion

        #region Properties
        /// <summary>
        /// The months displayed.
        /// </summary>
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public MonthCalendarMonth[] Months { get; private set; }


        public enum CalendarSizeMode { CalendarDimension, CalendarSize }
        CalendarSizeMode sizeMode = CalendarSizeMode.CalendarDimension;
        [Category("Appearance")]
        [Description("Sets how calendar is filled on screen.")]
        public CalendarSizeMode SizeMode
        {
            get { return sizeMode; }
            set
            {
                sizeMode = value;
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the start month and year.
        /// </summary>
        [Category("Appearance")]
        [Description("Sets the first displayed month and year.")]
        public DateTime ViewStart
        {
            get { return viewStart; }

            set
            {
                if (value == viewStart
                   || value < _minDate || value > _maxDate)
                    return;
                SetStartDate(value);
                UpdateMonths();
                Refresh();
            }
        }
        private DateTime viewStart;

        /// <summary>
        /// Gets the last in-month date of the last displayed month.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime ViewEnd
        {
            get
            {
                MonthCalendarDate dt = new MonthCalendarDate(cultureCalendar, viewStart)
                   .GetEndDateOfWeek(_formatProvider).FirstOfMonth.AddMonths(Months != null ? Months.Length - 1 : 1).FirstOfMonth;
                int daysInMonth = dt.DaysInMonth;
                dt = dt.AddDays(daysInMonth - 1);
                return dt.Date;
            }
        }

        /// <summary>
        /// Gets the real start date of the month calendar.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime RealStartDate
        {
            get { return realStart; }
        }
        private DateTime realStart;

        /// <summary>
        /// Gets or sets the lower limit of the visible month and year.
        /// </summary>
        [Category("Behavior")]
        [Description("The viewable minimum month and year.")]
        public DateTime MinDate
        {
            get { return _minDate; }
            set
            {
                if (value < CultureCalendar.MinSupportedDateTime || value > CultureCalendar.MaxSupportedDateTime
                   || value >= _maxDate)
                    return;
                value = GetMinDate(value);
                _minDate = value.Date;
                UpdateMonths();
                int dim1 = Math.Max(1, calendarDimensions.Width * calendarDimensions.Height);
                int dim2 = Months != null ? Months.Length : 1;
                if (dim1 != dim2)
                    SetStartDate(new MonthCalendarDate(CultureCalendar, viewStart).AddMonths(dim2 - dim1).Date);
                Invalidate();
            }
        }
        private DateTime _minDate;

        /// <summary>
        /// Gets or sets the upper limit of the visible month and year.
        /// </summary>
        [Category("Behavior")]
        [Description("The viewable maximum month and year.")]
        public DateTime MaxDate
        {
            get { return _maxDate; }
            set
            {
                if (value < CultureCalendar.MinSupportedDateTime || value > CultureCalendar.MaxSupportedDateTime
                   || value <= _minDate)
                    return;

                value = GetMaxDate(value);
                _maxDate = value.Date;
                UpdateMonths();
                int dim1 = Math.Max(1, calendarDimensions.Width * calendarDimensions.Height);
                int dim2 = Months != null ? Months.Length : 1;
                if (dim1 != dim2)
                    SetStartDate(new MonthCalendarDate(CultureCalendar, viewStart).AddMonths(dim2 - dim1).Date);
                Invalidate();
            }
        }
        private DateTime _maxDate;

        /// <summary>
        /// Gets or sets the calendar dimensions.
        /// </summary>
        [Category("Appearance")]
        [Description("The number of rows and columns of months in the calendar.")]
        [DefaultValue(typeof(Size), "3,1")]
        public Size CalendarDimensions
        {
            get { return calendarDimensions; }
            set
            {
                if (value == calendarDimensions || value.IsEmpty)
                    return;

                value.Width = Math.Max(1, Math.Min(value.Width, 7));    // get number of months in a row
                value.Height = Math.Max(1, Math.Min(value.Height, 7));  // get number of months in a column
                calendarDimensions = value; // set new dimension
                _inUpdate = true;    // update width and height of control
                Width = value.Width * _monthWidth;
                Height = (value.Height * _monthHeight) + (showFooter ? _footerHeight : 0);
                _inUpdate = false;
                scrollChange = Math.Max(0, Math.Min(scrollChange, calendarDimensions.Width * calendarDimensions.Height));   // adjust scroll change
                CalculateSize(false);   // calculate sizes
            }
        }
        private Size calendarDimensions = new Size(3, 1);

        /// <summary>
        /// Gets or sets the header font.
        /// </summary>
        [Category("Appearance")]
        [Description("The font for the header.")]
        public Font HeaderFont
        {
            get { return headerFont; }
            set
            {
                if (value == headerFont || value == null)
                    return;
                BeginUpdate();
                if (headerFont != null)
                    headerFont.Dispose();
                headerFont = value;
                CalculateSize(false);
                EndUpdate();
            }
        }
        private Font headerFont = new Font(SystemFonts.CaptionFont, FontStyle.Bold);

        /// <summary>
        /// Gets or sets the footer font.
        /// </summary>
        [Category("Appearance")]
        [Description("The font for the footer.")]
        public Font FooterFont
        {
            get { return footerFont; }
            set
            {
                if (value == footerFont || value == null)
                    return;
                BeginUpdate();
                if (footerFont != null)
                    footerFont.Dispose();
                footerFont = value;
                CalculateSize(false);
                EndUpdate();
            }
        }
        private Font footerFont = new Font(SystemFonts.DefaultFont, FontStyle.Bold);

        /// <summary>
        /// Gets or sets the font for the day header.
        /// </summary>
        [Category("Appearance")]
        [Description("The font for the day header.")]
        public Font DayHeaderFont
        {
            get { return dayHeaderFont; }
            set
            {
                if (value == dayHeaderFont || value == null)
                    return;

                BeginUpdate();
                if (dayHeaderFont != null)
                    dayHeaderFont.Dispose();
                dayHeaderFont = value;
                CalculateSize(false);
                EndUpdate();
            }
        }
        private Font dayHeaderFont = SystemFonts.DefaultFont;

        /// <summary>
        /// Gets or sets the font used for the week header and days.
        /// </summary>
        public override Font Font
        {
            get { return base.Font; }
            set
            {
                BeginUpdate();
                base.Font = value;
                CalculateSize(false);
                EndUpdate();
            }
        }

        /// <summary>
        /// Gets or sets the size of the control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new Size Size
        {
            get { return base.Size; }
            set { base.Size = value; }
        }

        /// <summary>
        /// Gets or sets the text alignment for the days.
        /// </summary>
        [DefaultValue(typeof(ContentAlignment), "MiddleCenter")]
        [Description("Determines the text alignment for the days.")]
        [Category("Appearance")]
        public ContentAlignment DayTextAlignment
        {
            get { return dayTextAlign; }
            set
            {
                if (value == dayTextAlign)
                    return;
                dayTextAlign = value;
                Invalidate();
            }
        }
        private ContentAlignment dayTextAlign = ContentAlignment.MiddleCenter;

        /// <summary>
        /// Gets or sets a value indicating whether to use a right to left layout.
        /// </summary>
        [DefaultValue(false)]
        [Category("Appearance")]
        [Description("Indicates whether to use the RTL layout.")]
        public bool RightToLeftLayout
        {
            get { return rightToLeftLayout; }
            set
            {
                if (value == rightToLeftLayout)
                    return;
                rightToLeftLayout = value;
                _formatProvider.IsRTLLanguage = UseRTL;
                Size calDim = calendarDimensions;
                UpdateMonths();
                CalendarDimensions = calDim;
                Refresh();
            }
        }
        private bool rightToLeftLayout = false;

        /// <summary>
        /// Gets or sets a value indicating whether to show the footer.
        /// </summary>
        [DefaultValue(true)]
        [Category("Appearance")]
        [Description("Indicates whether to show the footer.")]
        public bool ShowFooter
        {
            get { return showFooter; }
            set
            {
                if (value == showFooter)
                    return;
                showFooter = value;
                Height += value ? _footerHeight : -_footerHeight;
                Invalidate();
            }
        }
        private bool showFooter = true;

        /// <summary>
        /// Gets or sets a value indicating whether to show the week header.
        /// </summary>
        [DefaultValue(true)]
        [Category("Appearance")]
        [Description("Indicates whether to show the week header.")]
        public bool ShowWeekHeader
        {
            get { return showWeekHeader; }
            set
            {
                if (showWeekHeader == value)
                    return;
                showWeekHeader = value;
                CalculateSize(false);
            }
        }
        private bool showWeekHeader = true;

        /// <summary>
        /// Gets or sets a value indicating whether to use the shortest or the abbreviated day names in the day header.
        /// </summary>
        [DefaultValue(false)]
        [Category("Appearance")]
        [Description("Indicates whether to use the shortest or the abbreviated day names in the day header.")]
        public bool UseShortestDayNames
        {
            get { return useShortestDayNames; }
            set
            {
                useShortestDayNames = value;
                CalculateSize(false);
            }
        }
        private bool useShortestDayNames;

        /// <summary>
        /// Gets or sets a value indicating whether to use the native digits in <see cref="NumberFormatInfo.NativeDigits"/>
        /// specified by <see cref="EnhancedMonthCalendar.Culture"/>s <see cref="CultureInfo.NumberFormat"/>
        /// for number display.
        /// </summary>
        [DefaultValue(false)]
        [Category("Appearance")]
        [Description("Indicates whether to use the native digits as specified by the current Culture property.")]
        public bool UseNativeDigits
        {
            get { return useNativeDigits; }
            set
            {
                if (value == useNativeDigits)
                    return;
                useNativeDigits = value;
                Refresh();
            }
        }
        private bool useNativeDigits;

        /// <summary>
        /// Gets or sets the list for bolded dates.
        /// </summary>
        [Description("The bolded dates in the month calendar.")]
        public List<DateTime> BoldedDates
        {
            get { return boldedDates; }
            set { boldedDates = value ?? new List<DateTime>(); }
        }
        private List<DateTime> boldedDates = new List<DateTime>();

        /// <summary>
        /// Gets the bolded dates.
        /// </summary>
        [Description("The bolded dates in the calendar.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public BoldedDatesCollection BoldedDatesCollection
        {
            get { return _boldDatesCollection; }
        }

        /// <summary>
        /// Gets a collection holding the defined categories of bold dates.
        /// </summary>
        [Description("The bold date categories in the calendar.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public BoldedDateCategoryCollection BoldedDateCategoryCollection { get; private set; }

        /// <summary>
        /// Gets or sets the selection start date.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime SelectionStart
        {
            get { return selectionStart; }
            set
            {
                value = value.Date;

                // valid value ?
                if (value < CultureCalendar.MinSupportedDateTime || value > CultureCalendar.MaxSupportedDateTime)
                    return;

                if (value < _minDate)
                    value = _minDate;
                else if (value > _maxDate)
                    value = _maxDate;

                switch (daySelectionMode)
                {
                    case MonthCalendarSelectionMode.Day:
                        selectionStart = value;
                        selectionEnd = value;

                        break;

                    case MonthCalendarSelectionMode.WorkWeek:
                    case MonthCalendarSelectionMode.FullWeek:
                        MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, value).GetFirstDayInWeek(_formatProvider);
                        selectionStart = dt.Date;
                        selectionEnd = dt.GetEndDateOfWeek(_formatProvider).Date;

                        break;

                    case MonthCalendarSelectionMode.Month:
                        dt = new MonthCalendarDate(CultureCalendar, value).FirstOfMonth;
                        selectionStart = dt.Date;
                        selectionEnd = dt.AddMonths(1).AddDays(-1).Date;

                        break;

                    case MonthCalendarSelectionMode.Manual:
                        value = GetSelectionDate(selectionEnd, value);

                        if (value == DateTime.MinValue)
                        {
                            selectionEnd = value;
                            selectionStart = value;
                        }
                        else
                        {
                            selectionStart = value;
                            if (selectionEnd == DateTime.MinValue)
                                selectionEnd = value;
                        }

                        break;
                }

                Refresh();
            }
        }
        private DateTime selectionStart = DateTime.Today;

        /// <summary>
        /// Gets or sets the selection end date.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime SelectionEnd
        {
            get { return selectionEnd; }
            set
            {
                value = value.Date;

                if (value < CultureCalendar.MinSupportedDateTime || value > CultureCalendar.MaxSupportedDateTime
                   || daySelectionMode != MonthCalendarSelectionMode.Manual)
                    return;

                if (value < _minDate)
                    value = _minDate;
                else if (value > _maxDate)
                    value = _maxDate;

                value = GetSelectionDate(selectionStart, value);

                if (value == DateTime.MinValue || selectionStart == DateTime.MinValue)
                {
                    selectionStart = value;
                    selectionEnd = value;
                    Refresh();
                    return;
                }

                selectionEnd = value;
                Refresh();
            }
        }
        private DateTime selectionEnd = DateTime.Today;

        /// <summary>
        /// Gets or sets the selection range of the selected dates.
        /// </summary>
        [Category("Behavior")]
        [Description("The selection range.")]
        public SelectionRange SelectionRange
        {
            get { return new SelectionRange(selectionStart, selectionEnd); }
            set
            {
                if (value == null)
                {
                    this.selectionEnd = DateTime.Today;
                    this.selectionStart = DateTime.Today;

                    this.Refresh();
                    return;
                }

                switch (daySelectionMode)
                {
                    case MonthCalendarSelectionMode.Day:
                    case MonthCalendarSelectionMode.WorkWeek:
                    case MonthCalendarSelectionMode.FullWeek:
                    case MonthCalendarSelectionMode.Month:
                        SelectionStart = selectionStart == value.Start ?
                           value.End : value.Start;

                        break;

                    case MonthCalendarSelectionMode.Manual:
                        selectionStart = value.Start;
                        SelectionEnd = value.End;

                        break;
                }
            }
        }

        /// <summary>
        /// Gets the selection ranges.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<SelectionRange> SelectionRanges
        {
            get { return selectionRanges; }
        }
        private readonly List<SelectionRange> selectionRanges = new List<SelectionRange>();

        /// <summary>
        /// Gets or sets the scroll change if clicked on an arrow button.
        /// </summary>
        [DefaultValue(0)]
        [Category("Behavior")]
        [Description("The number of months the calendar is going for- or backwards if clicked on an arrow."
         + "A value of 0 indicates the last visible month is the first month (forwards) and vice versa.")]
        public int ScrollChange
        {
            get { return scrollChange; }
            set
            {
                if (value == scrollChange)
                    return;
                scrollChange = value;
            }
        }
        private int scrollChange;

        /// <summary>
        /// Gets or sets the maximum selectable days.
        /// </summary>
        [DefaultValue(0)]
        [Category("Behavior")]
        [Description("The maximum selectable days. A value of 0 means no limit.")]
        public int MaxSelectionCount
        {
            get { return maxSelectionCount; }
            set
            {
                if (value == maxSelectionCount)
                    return;
                maxSelectionCount = Math.Max(0, value);
            }
        }
        private int maxSelectionCount;

        /// <summary>
        /// Gets or sets the day selection mode.
        /// </summary>
        [DefaultValue(MonthCalendarSelectionMode.Manual)]
        [Category("Behavior")]
        [Description("Sets the day selection mode.")]
        public MonthCalendarSelectionMode SelectionMode
        {
            get { return daySelectionMode; }
            set
            {
                if (value == daySelectionMode || !Enum.IsDefined(typeof(MonthCalendarSelectionMode), value))
                    return;
                daySelectionMode = value;
                SetSelectionRange(value);
                Refresh();
            }
        }
        private MonthCalendarSelectionMode daySelectionMode = MonthCalendarSelectionMode.Manual;

        /// <summary>
        /// Gets or sets the non working days.
        /// </summary>
        [DefaultValue(CalendarDayOfWeek.Saturday | CalendarDayOfWeek.Sunday)]
        [Category("Behavior")]
        [Description("Sets the non working days.")]
        [Editor(typeof(Design.FlagEnumUIEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public CalendarDayOfWeek NonWorkDays
        {
            get { return nonWorkDays; }
            set
            {
                if (value == nonWorkDays)
                    return;
                nonWorkDays = value;
                if (daySelectionMode == MonthCalendarSelectionMode.WorkWeek)
                    Refresh();
            }
        }
        private CalendarDayOfWeek nonWorkDays = CalendarDayOfWeek.Saturday | CalendarDayOfWeek.Sunday;

        /// <summary>
        /// Gets or sets the used _renderer.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public MonthCalendarRenderer Renderer
        {
            get { return _renderer; }
            set
            {
                if (value == null)
                    return;
                _renderer = value;
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the culture used by the <see cref="EnhancedMonthCalendar"/>.
        /// </summary>
        [Category("Behavior")]
        [Description("The culture used by the Calendar.")]
        [TypeConverter(typeof(Design.CultureInfoCustomTypeConverter))]
        public CultureInfo Culture
        {
            get { return culture; }
            set
            {
                if (value == null || value.IsNeutralCulture)
                    return;

                culture = value;
                _formatProvider.DateTimeFormat = value.DateTimeFormat;
                CultureCalendar = null;
                if (DateMethods.IsRTLCulture(value))
                {
                    RightToLeft = RightToLeft.Yes;
                    RightToLeftLayout = true;
                }
                else
                {
                    RightToLeftLayout = false;
                    RightToLeft = RightToLeft.Inherit;
                }
                _formatProvider.IsRTLLanguage = UseRTL;
            }
        }
        private CultureInfo culture = CultureInfo.CurrentCulture;

        /// <summary>
        /// Gets or sets the used calendar.
        /// </summary>
        [Category("Behavior")]
        [Description("The calendar used by the Calendar.")]
        [Editor(typeof(Design.MonthCalendarCalendarUIEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [TypeConverter(typeof(Design.MonthCalendarCalendarTypeConverter))]
        public Calendar CultureCalendar
        {
            get { return cultureCalendar; }
            set
            {
                if (value == null)
                    value = culture.Calendar;

                cultureCalendar = value;
                _formatProvider.Calendar = value;

                if (value.GetType() == typeof(PersianCalendar) && !value.IsReadOnly)
                    value.TwoDigitYearMax = 1410;

                foreach (Calendar c in culture.OptionalCalendars)
                {
                    if (value.GetType() == c.GetType())
                    {
                        if (value.GetType() == typeof(GregorianCalendar))
                        {
                            GregorianCalendar g1 = (GregorianCalendar)value;
                            GregorianCalendar g2 = (GregorianCalendar)c;

                            if (g1.CalendarType != g2.CalendarType)
                                continue;
                        }

                        culture.DateTimeFormat.Calendar = c;
                        _formatProvider.DateTimeFormat = culture.DateTimeFormat;
                        cultureCalendar = c;

                        value = c;

                        break;
                    }
                }

                _eraRanges = GetEraRanges(value);
                ReAssignSelectionMode();
                _minDate = GetMinDate(value.MinSupportedDateTime.Date);
                _maxDate = GetMaxDate(value.MaxSupportedDateTime.Date);
                SetStartDate(DateTime.Today);
                CalculateSize(false);
            }
        }
        private Calendar cultureCalendar = CultureInfo.CurrentUICulture.DateTimeFormat.Calendar;

        /// <summary>
        /// Gets or sets the interface for day name handling.
        /// </summary>
        [TypeConverter(typeof(Design.MonthCalendarNamesProviderTypeConverter))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Category("Behavior")]
        [Description("Culture dependent settings for month/day names and date formatting.")]
        public ICustomFormatProvider FormatProvider
        {
            get
            {
                if (_formatProvider == null)
                    _formatProvider = new MonthCalendarFormatProvider(culture, null, culture.TextInfo.IsRightToLeft);
                return _formatProvider;
            }
            set { _formatProvider = value; }
        }
        private ICustomFormatProvider _formatProvider;

        /// <summary>
        /// Gets the size of a single <see cref="MonthCalendarMonth"/>.
        /// </summary>
        internal Size MonthSize
        {
            get { return new Size(_monthWidth, _monthHeight); }
        }

        /// <summary>
        /// Gets the size of a single day.
        /// </summary>
        internal Size DaySize
        {
            get { return new Size(_dayWidth, _dayHeight); }
        }

        /// <summary>
        /// Gets the footer size.
        /// </summary>
        internal Size FooterSize
        {
            get { return new Size(Width, _footerHeight); }
        }

        /// <summary>
        /// Gets the header size.
        /// </summary>
        internal Size HeaderSize
        {
            get { return new Size(_monthWidth, _headerHeight); }
        }

        /// <summary>
        /// Gets the size of the day names.
        /// </summary>
        internal Size DayNamesSize
        {
            get { return new Size(_dayWidth * 7, _dayNameHeight); }
        }

        /// <summary>
        /// Gets the size of the week numbers.
        /// </summary>
        internal Size WeekNumberSize
        {
            get { return new Size(_weekNumberWidth, _dayHeight * 7); }
        }

        /// <summary>
        /// Gets the date for the current day the mouse is over.
        /// </summary>
        internal DateTime MouseOverDay
        {
            get { return mouseMoveFlags.Day; }
        }
        private MonthCalendarMouseMoveFlags mouseMoveFlags = new MonthCalendarMouseMoveFlags();

        /// <summary>
        /// Gets a value indicating whether the control is using the RTL layout.
        /// </summary>
        internal bool UseRTL
        {
            get { return RightToLeft == RightToLeft.Yes && rightToLeftLayout; }
        }

        /// <summary>
        /// Gets the current left button RequestState.
        /// </summary>
        internal ButtonState LeftButtonState
        {
            get { return mouseMoveFlags.LeftArrow ? ButtonState.Pushed : ButtonState.Normal; }
        }

        /// <summary>
        /// Gets the current right button RequestState.
        /// </summary>
        internal ButtonState RightButtonState
        {
            get { return mouseMoveFlags.RightArrow ? ButtonState.Pushed : ButtonState.Normal; }
        }

        /// <summary>
        /// Gets the current hit type result.
        /// </summary>
        internal MonthCalendarHitType CurrentHitType
        {
            get { return currentHitType; }
        }
        private MonthCalendarHitType currentHitType = MonthCalendarHitType.None;

        /// <summary>
        /// Gets the month menu.
        /// </summary>
        internal ContextMenuStrip MonthMenu
        {
            get { return monthMenu; }
        }

        /// <summary>
        /// Gets the year menu.
        /// </summary>
        internal ContextMenuStrip YearMenu
        {
            get { return yearMenu; }
        }

        /// <summary>
        /// Gets the era date ranges for the current calendar.
        /// </summary>
        internal MonthCalendarEraRange[] EraRanges
        {
            get { return _eraRanges; }
        }
        private MonthCalendarEraRange[] _eraRanges;
        #endregion

        public EnhancedMonthCalendar()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.Selectable
                | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor,
                  true);

            InitializeComponent();

            _eraRanges = GetEraRanges(cultureCalendar);
            _formatProvider = new MonthCalendarFormatProvider(culture, null, culture.TextInfo.IsRightToLeft) { EnhancedMonthCalendar = this };
            _minDate = cultureCalendar.MinSupportedDateTime.Date < new DateTime(1900, 1, 1)
                ? new DateTime(1900, 1, 1)
                : cultureCalendar.MinSupportedDateTime.Date;
            _maxDate = cultureCalendar.MaxSupportedDateTime.Date > new DateTime(9998, 12, 31)
                ? new DateTime(9998, 12, 31)
                : cultureCalendar.MaxSupportedDateTime.Date;
            _renderer = new MonthCalendarRenderer(this);

            UpdateYearMenu(DateTime.Today.Year);    // update year menu
            SetStartDate(new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1));   // set start date
            CalculateSize(SizeMode == CalendarSizeMode.CalendarSize);
        }

        #region methods
        /// <summary>
        /// Prevents a drawing of the control until <see cref="EndUpdate"/> is called.
        /// </summary>
        public void BeginUpdate()
        {
            NativeMethods.SendMessage(Handle, SETREDRAW, false, 0);
        }

        /// <summary>
        /// Ends the updating process and the control can draw itself again.
        /// </summary>
        public void EndUpdate()
        {
            NativeMethods.SendMessage(Handle, SETREDRAW, true, 0);
            Refresh();
        }

        /// <summary>
        /// Performs a hit test with the specified <paramref name="location"/> as mouse location.
        /// </summary>
        /// <param name="location">The mouse location.</param>
        /// <returns>A <see cref="MonthCalendarHitTest"/> object.</returns>
        public MonthCalendarHitTest HitTest(Point location)
        {
            if (!ClientRectangle.Contains(location))
                return MonthCalendarHitTest.Empty;

            if (_rightArrowRect.Contains(location))
                return new MonthCalendarHitTest(GetNewScrollDate(false), MonthCalendarHitType.Arrow, _rightArrowRect);

            if (_leftArrowRect.Contains(location))
                return new MonthCalendarHitTest(GetNewScrollDate(true), MonthCalendarHitType.Arrow, _leftArrowRect);

            if (showFooter && _footerRect.Contains(location))
                return new MonthCalendarHitTest(DateTime.Today, MonthCalendarHitType.Footer, _footerRect);

            foreach (MonthCalendarMonth month in Months)
            {
                MonthCalendarHitTest hit = month.HitTest(location);

                if (!hit.IsEmpty)
                    return hit;
            }

            return MonthCalendarHitTest.Empty;
        }

        /// <summary>
        /// Gets all bolded dates.
        /// </summary>
        /// <returns>A generic List of type <see cref="DateTime"/> with the bolded dates.</returns>
        internal List<DateTime> GetBoldedDates()
        {
            List<DateTime> dates = new List<DateTime>();

            // remove all duplicate dates
            boldedDates.ForEach(date =>
            {
                if (!dates.Contains(date))
                    dates.Add(date);
            });

            return dates;
        }

        /// <summary>
        /// Determines if the <paramref name="date"/> is selected.
        /// </summary>
        /// <param name="date">The date to determine if checked.</param>
        /// <returns>true if <paramref name="date"/> is selected; false otherwise.</returns>
        internal bool IsSelected(DateTime date)
        {
            // get if date is in first selection range
            bool selected = SelectionRange.Contains(date);

            // get if date is in all other selection ranges (only in WorkWeek day selection mode)
            selectionRanges.ForEach(range =>
            {
                selected |= range.Contains(date);
            });

            // if in WorkWeek day selection mode a date is only selected if a work day
            if (daySelectionMode == MonthCalendarSelectionMode.WorkWeek)
            {
                // get all work days
                List<DayOfWeek> workDays = DateMethods.GetWorkDays(nonWorkDays);

                // return if date is selected
                return workDays.Contains(date.DayOfWeek) && selected;
            }

            // return if date is selected
            return selected;
        }

        /// <summary>
        /// Scrolls to the selection start.
        /// </summary>
        internal void EnsureSeletedDateIsVisible()
        {
            if (RealStartDate < MinDate || RealStartDate > selectionStart || selectionStart > _lastVisibleDate)
            {
                SetStartDate(new DateTime(selectionStart.Year, selectionStart.Month, 1));
                UpdateMonths();
            }
        }

        /// <summary>
        /// Sets the bounds of the left arrow.
        /// </summary>
        /// <param name="rect">The bounds of the left arrow.</param>
        internal void SetLeftArrowRect(Rectangle rect)
        {
            // if in RTL mode
            if (UseRTL)
                // the left arrow bounds are the right ones
                _rightArrowRect = rect;
            else
                _leftArrowRect = rect;
        }

        /// <summary>
        /// Sets the bounds of the right arrow.
        /// </summary>
        /// <param name="rect">The bounds of the right arrow.</param>
        internal void SetRightArrowRect(Rectangle rect)
        {
            // if in RTL mode
            if (UseRTL)
                // the right arrow bounds are the left ones
                _leftArrowRect = rect;
            else
                _rightArrowRect = rect;
        }

        /// <summary>
        /// Processes a dialog key.
        /// </summary>
        /// <param name="keyData">One of the <see cref="Keys"/> value that represents the key to process.</param>
        /// <returns>true if the key was processed by the control; otherwise, false.</returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (daySelectionMode != MonthCalendarSelectionMode.Day)
                return base.ProcessDialogKey(keyData);

            MonthCalendarDate dt = new MonthCalendarDate(cultureCalendar, selectionStart);
            bool retValue = false;

            if (keyData == Keys.Left)
            {
                selectionStart = dt.AddDays(-1).Date;
                retValue = true;
            }
            else if (keyData == Keys.Right)
            {
                selectionStart = dt.AddDays(1).Date;
                retValue = true;
            }
            else if (keyData == Keys.Up)
            {
                selectionStart = dt.AddDays(-7).Date;
                retValue = true;
            }
            else if (keyData == Keys.Down)
            {
                selectionStart = dt.AddDays(7).Date;
                retValue = true;
            }

            if (retValue)
            {
                if (selectionStart < _minDate)
                    selectionStart = _minDate;
                else if (selectionStart > _maxDate)
                    selectionStart = _maxDate;

                SetSelectionRange(daySelectionMode);
                EnsureSeletedDateIsVisible();
                RaiseInternalDateSelected();
                Invalidate();
                return true;
            }

            return base.ProcessDialogKey(keyData);
        }

        /// <summary>
        /// Performs the work of setting the specified bounds of this control.
        /// </summary>
        /// <param name="x">The new <see cref="System.Windows.Forms.Control.Left"/> property value of the control.</param>
        /// <param name="y">The new <see cref="System.Windows.Forms.Control.Top"/> property value of the control.</param>
        /// <param name="width">The new <see cref="System.Windows.Forms.Control.Width"/> property value of the control.</param>
        /// <param name="height">The new <see cref="System.Windows.Forms.Control.Height"/> property value of the control.</param>
        /// <param name="specified">A bitwise combination of the <see cref="System.Windows.Forms.BoundsSpecified"/> values.</param>
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            base.SetBoundsCore(x, y, width, height, specified);
            if (Created || DesignMode)
                CalculateSize(SizeMode == CalendarSizeMode.CalendarSize);
        }

        /// <summary>
        /// Builds an array of <see cref="MonthCalendarEraRange"/> to store the min and max date of the eras of the specified <see cref="System.Globalization.Calendar"/>.
        /// </summary>
        /// <param name="cal">The <see cref="System.Globalization.Calendar"/> to retrieve the era ranges for.</param>
        /// <returns>An array of type <see cref="MonthCalendarEraRange"/>.</returns>
        private static MonthCalendarEraRange[] GetEraRanges(Calendar cal)
        {
            if (cal.Eras.Length == 1)
                return new[] { new MonthCalendarEraRange(cal.Eras[0], cal.MinSupportedDateTime.Date, cal.MaxSupportedDateTime.Date) };

            List<MonthCalendarEraRange> ranges = new List<MonthCalendarEraRange>();
            DateTime date = cal.MinSupportedDateTime.Date;
            int currentEra = -1;
            while (date < cal.MaxSupportedDateTime.Date)
            {
                int era = cal.GetEra(date);

                if (era != currentEra)
                {
                    ranges.Add(new MonthCalendarEraRange(era, date));
                    if (currentEra != -1)
                        ranges[ranges.Count - 2].MaxDate = cal.AddDays(date, -1);
                    currentEra = era;
                }
                date = cal.AddDays(date, 1);
            }
            ranges[ranges.Count - 1].MaxDate = date;
            return ranges.ToArray();
        }


        /// <summary>
        /// Gets the era range for the specified era.
        /// </summary>
        /// <param name="era">The era to get the date range for.</param>
        /// <returns>A <see cref="MonthCalendarEraRange"/> object.</returns>
        private MonthCalendarEraRange GetEraRange(int era)
        {
            foreach (MonthCalendarEraRange e in _eraRanges)
            {
                if (e.Era == era)
                    return e;
            }

            return new MonthCalendarEraRange(CultureCalendar.GetEra(DateTime.Today), CultureCalendar.MinSupportedDateTime.Date, CultureCalendar.MaxSupportedDateTime.Date);
        }

        /// <summary>
        /// Gets the era range for the era the current date is in.
        /// </summary>
        /// <returns>A <see cref="MonthCalendarEraRange"/>.</returns>
        private MonthCalendarEraRange GetEraRange()
        {
            return GetEraRange(CultureCalendar.GetEra(DateTime.Today));
        }

        /// <summary>
        /// Calculates the various sizes of a single month view and the global size of the control.
        /// </summary>
        private void CalculateSize(bool changeDimension)
        {
            // if already calculating - return
            if (_inUpdate)
                return;

            _inUpdate = true;

            using (Graphics g = CreateGraphics())
            {
                // get sizes for different elements of the calendar
                SizeF daySize = g.MeasureString("30", Font);
                SizeF weekNumSize = g.MeasureString("59", Font);

                MonthCalendarDate date = new MonthCalendarDate(CultureCalendar, viewStart);

                SizeF monthNameSize = g.MeasureString(
                   _formatProvider.GetMonthName(date.Year, date.Month),
                   headerFont);
                SizeF yearStringSize = g.MeasureString(
                   viewStart.ToString("yyyy"), headerFont);
                SizeF footerStringSize = g.MeasureString(
                   viewStart.ToShortDateString(), footerFont);

                // calculate the header height
                _headerHeight = Math.Max(
                   (int)Math.Max(monthNameSize.Height + 3, yearStringSize.Height) + 1, 15);

                // calculate the width of a single day
                _dayWidth = Math.Max(12, (int)daySize.Width + 1) + 5;

                // calculate the height of a single day
                _dayHeight = Math.Max(Math.Max(12, (int)weekNumSize.Height + 1), (int)daySize.Height + 1) + 2;

                // calculate the height of the footer
                _footerHeight = Math.Max(20, (int)footerStringSize.Height + 2);

                // calculate the width of the week number header
                _weekNumberWidth = showWeekHeader ? Math.Max(12, (int)weekNumSize.Width + 1) + 2 : 0;

                // set minimal height of the day name header
                _dayNameHeight = dayHeaderFont.Height + 10;

                // loop through all day names
                foreach (string str in DateMethods.GetDayNames(_formatProvider, useShortestDayNames ? 2 : 1))
                {
                    // get the size of the name
                    SizeF dayNameSize = g.MeasureString(str, dayHeaderFont);

                    // adjust the width of the day and the day name header height
                    _dayWidth = Math.Max(_dayWidth, (int)dayNameSize.Width + 1);
                    _dayNameHeight = Math.Max(
                       _dayNameHeight,
                       (int)dayNameSize.Height + 1);
                }

                // calculate the width and height of a MonthCalendarMonth element
                _monthWidth = _weekNumberWidth + (_dayWidth * 7) + 2;
                _monthHeight = _headerHeight + _dayNameHeight + (_dayHeight * 6) + 2;

                if (changeDimension)
                {
                    // calculate the dimension of the control
                    int calWidthDim = Math.Max(1, Width / _monthWidth);
                    int calHeightDim = Math.Max(1, Height / _monthHeight);

                    // set the dimensions
                    CalendarDimensions = new Size(calWidthDim, calHeightDim);
                }

                // set the width and height of the control
                Height = (_monthHeight * calendarDimensions.Height) + (showFooter ? _footerHeight : 0) + Padding.Top + Padding.Bottom;
                Width = _monthWidth * calendarDimensions.Width + Padding.Top + Padding.Bottom;

                // calculate the footer bounds
                _footerRect = new Rectangle(1, Height - _footerHeight - 1, Width - 2, _footerHeight);

                // update the months
                UpdateMonths();
            }

            _inUpdate = false;
            Refresh();
        }

        /// <summary>
        /// Sets the start date.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <returns>true if <paramref name="start"/> is valid; false otherwise.</returns>
        private bool SetStartDate(DateTime start)
        {
            if (start < DateTime.MinValue.Date || start > DateTime.MaxValue.Date)
                return false;

            DayOfWeek firstDayOfWeek = _formatProvider.FirstDayOfWeek;
            MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, _maxDate);

            if (start > _maxDate)
                start = dt.AddMonths(1 - Months.Length).FirstOfMonth.Date;

            if (start < _minDate)
                start = _minDate;

            dt = new MonthCalendarDate(CultureCalendar, start);
            int length = Months != null ? Months.Length - 1 : 0;

            while (dt.Date > _minDate && dt.Day != 1)
            {
                dt = dt.AddDays(-1);
            }

            MonthCalendarDate endDate = dt.AddMonths(length);
            MonthCalendarDate endDateDay = endDate.AddDays(endDate.DaysInMonth - 1 - (endDate.Day - 1));

            if (endDate.Date >= _maxDate || endDateDay.Date >= _maxDate)
                dt = new MonthCalendarDate(CultureCalendar, _maxDate).AddMonths(-length).FirstOfMonth;
            viewStart = dt.Date;

            while (dt.Date > CultureCalendar.MinSupportedDateTime.Date && dt.DayOfWeek != firstDayOfWeek)
            {
                dt = dt.AddDays(-1);
            }

            realStart = dt.Date;
            return true;
        }

        /// <summary>
        /// Gets the index of the <see cref="MonthCalendarMonth"/> in the array of the specified monthYear datetime.
        /// </summary>
        /// <param name="monthYear">The date to search for.</param>
        /// <returns>The index in the array.</returns>
        private int GetIndex(DateTime monthYear)
        {
            return (from month in Months where month != null where month.Date == monthYear select month.Index)
               .FirstOrDefault();
        }

        /// <summary>
        /// Gets the <see cref="MonthCalendarMonth"/> which contains the specified date.
        /// </summary>
        /// <param name="day">The day to search for.</param>
        /// <returns>An <see cref="MonthCalendarMonth"/> if day is valid; otherwise null.</returns>
        private MonthCalendarMonth GetMonth(DateTime day)
        {
            if (day == DateTime.MinValue)
            {
                return null;
            }

            return Months.Where(month => month != null)
               .FirstOrDefault(month => month.ContainsDate(day));
        }

        /// <summary>
        /// Updates the shown months.
        /// </summary>
        public void UpdateMonths()
        {
            int x = Padding.Left, y = Padding.Top, index = 0;
            int calWidthDim = calendarDimensions.Width;
            int calHeightDim = calendarDimensions.Height;

            List<MonthCalendarMonth> monthList = new List<MonthCalendarMonth>();
            MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, viewStart);

            if (dt.GetEndDateOfWeek(_formatProvider).Month != dt.Month)
                dt = dt.GetEndDateOfWeek(_formatProvider).FirstOfMonth;

            if (UseRTL)
            {
                x = _monthWidth * (calWidthDim - 1);

                for (int i = 0; i < calHeightDim; i++)
                {
                    for (int j = calWidthDim - 1; j >= 0; j--)
                    {
                        if (dt.Date >= _maxDate)
                        {
                            break;
                        }

                        monthList.Add(new MonthCalendarMonth(this, dt.Date)
                        {
                            Location = new Point(x, y),
                            Index = index++
                        });

                        x -= _monthWidth;
                        dt = dt.AddMonths(1);
                    }

                    x = _monthWidth * (calWidthDim - 1);
                    y += _monthHeight;
                }
            }
            else
            {
                for (int i = 0; i < calHeightDim; i++)
                {
                    for (int j = 0; j < calWidthDim; j++)
                    {
                        if (dt.Date >= _maxDate)
                            break;

                        monthList.Add(new MonthCalendarMonth(this, dt.Date)
                        {
                            Location = new Point(x, y),
                            Index = index++
                        });

                        x += _monthWidth;
                        dt = dt.AddMonths(1);
                    }

                    x = 0;
                    y += _monthHeight;
                }
            }

            _lastVisibleDate = monthList[monthList.Count - 1].LastVisibleDate;

            Months = monthList.ToArray();
        }

        /// <summary>
        /// Updates the month menu.
        /// </summary>
        /// <param name="year">The year to calculate the months for.</param>
        private void UpdateMonthMenu(int year)
        {
            int i = 1;

            int monthsInYear = CultureCalendar.GetMonthsInYear(year);

            // set month names in menu
            foreach (ToolStripMenuItem item in monthMenu.Items)
            {
                if (i <= monthsInYear)
                {
                    item.Tag = i;
                    item.Text = _formatProvider.GetMonthName(year, i);
                    bool isAvailable = (_monthSelected.Year > MinDate.Year || (_monthSelected.Year == MinDate.Year && i >= MinDate.Month))
                                     && (_monthSelected.Year < MaxDate.Year || (_monthSelected.Year == MaxDate.Year && i <= MaxDate.Month));
                    item.Available = isAvailable;
                    if (isAvailable)
                    {
                        var now = DateTime.Now;
                        bool isCurrentMonth = (i == now.Month && _monthSelected.Year == now.Year);
                        var fontStyle = (isCurrentMonth ? FontStyle.Bold : FontStyle.Regular);

                        bool isSelectedMonth = (i == _monthSelected.Month);
                        if (isSelectedMonth)
                        {
                            fontStyle |= FontStyle.Underline;
                            item.Select();
                        }
                        item.ForeColor = (isCurrentMonth ? Color.DodgerBlue : SystemColors.MenuText);
                        item.Font = new Font(item.Font, fontStyle);
                    }
                    i++;
                }
                else
                {
                    item.Tag = null;
                    item.Text = string.Empty;
                    item.Visible = false;
                }
            }
        }

        /// <summary>
        /// Updates the year menu.
        /// </summary>
        /// <param name="year">The year in the middle to display.</param>
        private void UpdateYearMenu(int year)
        {
            int selYear = year;
            year -= 4;

            int maxYear = CultureCalendar.GetYear(_maxDate);
            int minYear = CultureCalendar.GetYear(_minDate);

            if (year + 8 > maxYear)
                year = maxYear - 8;
            else if (year < minYear)
                year = minYear;

            year = Math.Max(1, year);

            foreach (ToolStripMenuItem item in yearMenu.Items)
            {
                item.Text = DateMethods.GetNumberString(year, UseNativeDigits ? Culture.NumberFormat.NativeDigits : null, false);
                item.Tag = year;

                bool isCurrentYear = (year == CultureCalendar.GetYear(DateTime.Today));
                var fontStyle = (isCurrentYear ? FontStyle.Bold : FontStyle.Regular);
                if (year == selYear)
                {
                    fontStyle |= FontStyle.Underline;
                    item.Select();
                }

                item.Font = new Font(item.Font, fontStyle);
                item.ForeColor = (isCurrentYear ? Color.DodgerBlue : SystemColors.MenuText);
                year++;
            }
        }

        /// <summary>
        /// Handles clicks in the month menu.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event data.</param>
        private void MonthClick(object sender, EventArgs e)
        {
            MonthCalendarDate currentMonthYear = new MonthCalendarDate(CultureCalendar, (DateTime)monthMenu.Tag);

            int monthClicked = (int)((ToolStripMenuItem)sender).Tag;

            if (currentMonthYear.Month != monthClicked)
            {
                MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, new DateTime(currentMonthYear.Year, monthClicked, 1, CultureCalendar));
                DateTime newStartDate = dt.AddMonths(-GetIndex(currentMonthYear.Date)).Date;

                if (SetStartDate(newStartDate))
                {
                    UpdateMonths();
                    RaiseDateChanged();
                    Focus();
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Handles clicks in the year menu.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event data.</param>
        private void YearClick(object sender, EventArgs e)
        {
            DateTime currentMonthYear = (DateTime)yearMenu.Tag;

            int yearClicked = (int)((ToolStripMenuItem)sender).Tag;

            MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, currentMonthYear);

            if (dt.Year != yearClicked)
            {
                MonthCalendarDate newStartDate = new MonthCalendarDate(CultureCalendar, new DateTime(yearClicked, dt.Month, 1, CultureCalendar))
                   .AddMonths(-GetIndex(currentMonthYear));

                if (SetStartDate(newStartDate.Date))
                {
                    UpdateMonths();

                    RaiseDateChanged();

                    Focus();

                    Refresh();
                }
            }
        }

        /// <summary>
        /// Is called when the month menu was closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">A <see cref="ToolStripDropDownClosedEventArgs"/> that contains the event data.</param>
        private void MonthMenuClosed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            _monthSelected = DateTime.MinValue;
            _showingMenu = false;

            Invalidate(Months[GetIndex((DateTime)monthMenu.Tag)].TitleBounds);
        }

        /// <summary>
        /// Is called when the year menu was closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">A <see cref="ToolStripDropDownClosedEventArgs"/> that contains the event data.</param>
        private void YearMenuClosed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            _yearSelected = DateTime.MinValue;
            _showingMenu = false;

            Invalidate(Months[GetIndex((DateTime)yearMenu.Tag)].TitleBounds);
        }

        /// <summary>
        /// Calls <see cref="OnDateSelected"/>.
        /// </summary>
        private void RaiseDateSelected()
        {
            SelectionRange range = SelectionRange;

            DateTime selStart = range.Start;
            DateTime selEnd = range.End;

            if (selStart == DateTime.MinValue)
            {
                selStart = selEnd;
            }

            OnDateSelected(new DateRangeEventArgs(selStart, selEnd));
        }

        /// <summary>
        /// Calls <see cref="OnDateChanged"/>.
        /// </summary>
        private void RaiseDateChanged()
        {
            OnDateChanged(new DateRangeEventArgs(realStart, _lastVisibleDate));
        }

        /// <summary>
        /// Raises the <see cref="SelectionExtendEnd"/> event.
        /// </summary>
        private void RaiseSelectExtendEnd()
        {
            var handler = SelectionExtendEnd;

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the <see cref="InternalDateSelected"/> event.
        /// </summary>
        private void RaiseInternalDateSelected()
        {
            if (InternalDateSelected != null)
                InternalDateSelected(this, new DateEventArgs(selectionStart));
        }

        /// <summary>
        /// Adjusts the currently displayed month by setting a new start date.
        /// </summary>
        /// <param name="up">true for scrolling upwards, false otherwise.</param>
        private void Scroll(bool up)
        {
            if (SetStartDate(GetNewScrollDate(up)))
            {
                UpdateMonths();
                RaiseDateChanged();
                Refresh();
            }
        }

        /// <summary>
        /// Gets the new date for the specified scroll direction.
        /// </summary>
        /// <param name="up">true for scrolling upwards, false otherwise.</param>
        /// <returns>The new start date.</returns>
        private DateTime GetNewScrollDate(bool up)
        {
            if ((_lastVisibleDate == _maxDate && !up) || (Months[0].FirstVisibleDate == _minDate && up))
                return viewStart;

            MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, viewStart);

            int monthsToAdd = (scrollChange == 0 ?
               Math.Max((calendarDimensions.Width * calendarDimensions.Height) - 1, 1)
               : scrollChange) * (up ? -1 : 1);

            int length = Months == null ? Math.Max(1, calendarDimensions.Width * calendarDimensions.Height) : Months.Length;

            MonthCalendarDate newStartMonthDate = dt.AddMonths(monthsToAdd);
            MonthCalendarDate lastMonthDate = newStartMonthDate.AddMonths(length - 1);
            MonthCalendarDate lastMonthEndDate = lastMonthDate.AddDays(lastMonthDate.DaysInMonth - 1 - lastMonthDate.Day);

            if (newStartMonthDate.Date < _minDate)
                newStartMonthDate = new MonthCalendarDate(CultureCalendar, _minDate);
            else if (lastMonthEndDate.Date >= _maxDate || lastMonthDate.Date >= _maxDate)
            {
                MonthCalendarDate maxdt = new MonthCalendarDate(CultureCalendar, _maxDate).FirstOfMonth;
                newStartMonthDate = maxdt.AddMonths(1 - length);
            }

            return newStartMonthDate.Date;
        }

        /// <summary>
        /// Gets the current month header RequestState.
        /// </summary>
        /// <param name="monthDate">The month date.</param>
        /// <returns>A <see cref="MonthCalendarHeaderState"/> value.</returns>
        private MonthCalendarHeaderState GetMonthHeaderState(DateTime monthDate)
        {
            MonthCalendarHeaderState state = MonthCalendarHeaderState.Default;

            if (_monthSelected == monthDate)
                state = MonthCalendarHeaderState.MonthNameSelected;
            else if (_yearSelected == monthDate)
                state = MonthCalendarHeaderState.YearSelected;
            else if (mouseMoveFlags.MonthName == monthDate)
                state = MonthCalendarHeaderState.MonthNameActive;
            else if (mouseMoveFlags.Year == monthDate)
                state = MonthCalendarHeaderState.YearActive;
            else if (mouseMoveFlags.HeaderDate == monthDate)
                state = MonthCalendarHeaderState.Active;

            return state;
        }

        /// <summary>
        /// Invalidates the region taken up by the month specified by the <paramref name="date"/>.
        /// </summary>
        /// <param name="date">The date specifying the <see cref="MonthCalendarMonth"/> to invalidate.</param>
        /// <param name="refreshInvalid">true for refreshing the whole control if invalid <paramref name="date"/> passed; otherwise false.</param>
        private void InvalidateMonth(DateTime date, bool refreshInvalid)
        {
            if (date == DateTime.MinValue)
            {
                if (refreshInvalid)
                    Refresh();
                return;
            }

            MonthCalendarMonth month = GetMonth(date);

            if (month != null)
            {
                Invalidate(month.MonthBounds);
                Update();
            }
            else if (date > _lastVisibleDate)
            {
                Invalidate(Months[Months.Length - 1].Bounds);
                Update();
            }
            else if (refreshInvalid)
                Refresh();
        }

        /// <summary>
        /// Checks if the <paramref name="newSelectionDate"/> is within bounds of the <paramref name="baseDate"/>
        /// and the <see cref="MaxSelectionCount"/>.
        /// </summary>
        /// <param name="baseDate">The base date from where to check.</param>
        /// <param name="newSelectionDate">The new selection date.</param>
        /// <returns>A valid new selection date if valid parameters, otherwise <c>DateTime.MinValue</c>.</returns>
        private DateTime GetSelectionDate(DateTime baseDate, DateTime newSelectionDate)
        {
            if (maxSelectionCount == 0 || baseDate == DateTime.MinValue)
                return newSelectionDate;

            if (baseDate >= CultureCalendar.MinSupportedDateTime && newSelectionDate >= CultureCalendar.MinSupportedDateTime
               && baseDate <= CultureCalendar.MaxSupportedDateTime && newSelectionDate <= CultureCalendar.MaxSupportedDateTime)
            {
                int days = (baseDate - newSelectionDate).Days;

                if (Math.Abs(days) >= maxSelectionCount)
                {
                    newSelectionDate =
                       new MonthCalendarDate(CultureCalendar, baseDate).AddDays(days < 0
                                                                                     ? maxSelectionCount - 1
                                                                                     : 1 - maxSelectionCount).Date;
                }

                return newSelectionDate;
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// Returns the minimum date for the control.
        /// </summary>
        /// <param name="date">The date to set as min date.</param>
        /// <returns>The min date.</returns>
        private DateTime GetMinDate(DateTime date)
        {
            DateTime dt = new DateTime(1900, 1, 1);
            DateTime minEra = GetEraRange().MinDate;

            // bug in JapaneseLunisolarCalendar - JapaneseLunisolarCalendar.GetYear() with date parameter
            // between 1989/1/8 and 1989/2/6 returns 0 therefore make sure, the calendar
            // can display date range if ViewStart set to min date
            if (cultureCalendar.GetType() == typeof(JapaneseLunisolarCalendar))
                minEra = new DateTime(1989, 4, 1);

            DateTime mindate = minEra < dt ? dt : minEra;
            return date < mindate ? mindate : date;
        }

        /// <summary>
        /// Returns the maximum date for the control.
        /// </summary>
        /// <param name="date">The date to set as max date.</param>
        /// <returns>The max date.</returns>
        private DateTime GetMaxDate(DateTime date)
        {
            DateTime dt = new DateTime(9998, 12, 31);
            DateTime maxEra = GetEraRange().MaxDate;
            DateTime maxdate = maxEra > dt ? dt : maxEra;

            return date > maxdate ? maxdate : date;
        }

        /// <summary>
        /// If changing the used calendar, then this method reassigns the selection mode to set the correct selection range.
        /// </summary>
        private void ReAssignSelectionMode()
        {
            SelectionRange = null;
            MonthCalendarSelectionMode selMode = daySelectionMode;
            daySelectionMode = MonthCalendarSelectionMode.Manual;
            SelectionMode = selMode;
        }

        /// <summary>
        /// Sets the selection range for the specified <see cref="MonthCalendarSelectionMode"/>.
        /// </summary>
        /// <param name="selMode">The <see cref="MonthCalendarSelectionMode"/> value to set the selection range for.</param>
        private void SetSelectionRange(MonthCalendarSelectionMode selMode)
        {
            switch (selMode)
            {
                case MonthCalendarSelectionMode.Day:
                    {
                        selectionEnd = selectionStart;
                        break;
                    }

                case MonthCalendarSelectionMode.WorkWeek:
                case MonthCalendarSelectionMode.FullWeek:
                    {
                        MonthCalendarDate dt =
                           new MonthCalendarDate(CultureCalendar, selectionStart).GetFirstDayInWeek(
                              _formatProvider);
                        selectionStart = dt.Date;
                        selectionEnd = dt.AddDays(6).Date;

                        break;
                    }

                case MonthCalendarSelectionMode.Month:
                    {
                        MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, selectionStart).FirstOfMonth;
                        selectionStart = dt.Date;
                        selectionEnd = dt.AddMonths(1).AddDays(-1).Date;

                        break;
                    }
            }
        }
        #endregion
    }
}