﻿using SkyDean.FareLiz.WinForm.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace SkyDean.FareLiz.WinForm.Controls
{
    internal partial class EnhancedDatePicker
    {
        [Category("Action")]
        [Description("Is Raised when the date value changed.")]
        public event EventHandler<CheckDateEventArgs> ValueChanged;

        /// <summary>
        /// Is raised when the mouse is over an date.
        /// </summary>
        [Category("Action")]
        [Description("Is raised when the mouse is over an date.")]
        public event EventHandler<ActiveDateChangedEventArgs> ActiveDateChanged;


        /// <summary>
        /// Paints the control.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rect = ClientRectangle;
            e.Graphics.Clear(Enabled ? BackColor : SystemColors.Window);
            var mouseLoc = PointToClient(MousePosition);
            bool mouseOverControl = rect.Contains(mouseLoc);

            using (var borderPen = new Pen(mouseOverControl ? SystemColors.HotTrack : SystemColors.ActiveBorder))
            {
                e.Graphics.DrawRectangle(borderPen, rect);
            }

            var borderWidth = SystemInformation.BorderSize.Width;
            var borderHeight = SystemInformation.BorderSize.Height;

            rect.X = rect.Right - DropDownButtonWidth + borderWidth;
            rect.Width = DropDownButtonWidth - 2 * borderWidth;
            rect.Y += borderHeight;
            rect.Height = rect.Bottom - 2 * borderHeight;
            _buttonBounds = rect;

            var isMouseOverDropDownButton = rect.Contains(mouseLoc);
            if (isMouseOverDropDownButton)  // Draw background only if the mouse if over
                ButtonRenderer.DrawButton(e.Graphics, rect,
                    _buttonState == ComboButtonState.Pressed ? PushButtonState.Pressed : PushButtonState.Hot);

            e.Graphics.DrawImage(Resources.CalendarToggle, new Point(rect.X + 2, rect.Y + 2));

            var arrowHeight = Height / 6;
            var arrowWidth = arrowHeight * 0.8;
            var arrawLoc = new Point((int)(rect.Right - arrowWidth * 4), (int)(rect.Height / 2));

            //if the width is odd - favor pushing it over one pixel right.

            var arrow = new[]
                {
                    new Point((int)(arrawLoc.X - arrowWidth), arrawLoc.Y - 1),
                    new Point((int)(arrawLoc.X + arrowWidth + 1), arrawLoc.Y - 1),
                    new Point(arrawLoc.X, arrawLoc.Y + arrowHeight)
                };

            if (Enabled)
                e.Graphics.FillPolygon(SystemBrushes.ControlText, arrow);
            else
                e.Graphics.FillPolygon(SystemBrushes.ButtonShadow, arrow);
        }

        /// <summary>
        /// Raises the mouse enter event.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void OnMouseEnter(EventArgs e)
        {
            if (!_isDropped)
            {
                _buttonState = ComboButtonState.Hot;
                Refresh();
            }

            base.OnMouseEnter(e);
        }

        /// <summary>
        /// Raises the mouse leave event.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            if (!_isDropped)
            {
                _buttonState = ComboButtonState.Normal;
                Invalidate();
            }

            base.OnMouseLeave(e);
        }

        /// <summary>
        /// Raises the mouse down event.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            Focus();

            if (e.Button == MouseButtons.Left && _buttonBounds.Contains(e.Location))
            {
                SwitchPickerState();
                Refresh();
            }

            base.OnMouseDown(e);
        }

        /// <summary>
        /// Raises the <see cref="Control.MouseMove"/> event.
        /// </summary>
        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.Button == MouseButtons.None && !_isDropped)
            {
                Controls.EnhancedDatePicker.ComboButtonState st = _buttonState;

                _buttonState = _buttonBounds.Contains(e.Location) ?
                   ComboButtonState.Hot : ComboButtonState.Normal;

                if (st != _buttonState)
                {
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="Control.LostFocus"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnLostFocus(EventArgs e)
        {
            if (!_isDropped)
            {
                _buttonState = ComboButtonState.Normal;

                Invalidate();
            }

            if (!Focused)
            {
                base.OnLostFocus(e);
            }
        }

        /// <summary>
        /// Raises the <see cref="Control.GotFocus"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);

            Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="Control.FontChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            dateTextBox.Font = Font;

            Height = Math.Max(22, MeasureControlSize());
        }

        /// <summary>
        /// Raises the <see cref="Control.ForeColorChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnForeColorChanged(EventArgs e)
        {
            if (dateTextBox != null)
            {
                dateTextBox.ForeColor = ForeColor;
            }

            base.OnForeColorChanged(e);
        }

        /// <summary>
        /// Raises the <see cref="Control.EnabledChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="Control.RightToLeftChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnRightToLeftChanged(EventArgs e)
        {
            base.OnRightToLeftChanged(e);

            dateTextBox.RightToLeft = RightToLeft;
            dateTextBox.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="ValueChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        private void OnValueChanged(CheckDateEventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="ActiveDateChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ActiveDateChangedEventArgs"/> that contains the event data.</param>
        private void OnActiveDateChanged(ActiveDateChangedEventArgs e)
        {
            if (ActiveDateChanged != null)
            {
                ActiveDateChanged(this, e);
            }
        }
    }
}
