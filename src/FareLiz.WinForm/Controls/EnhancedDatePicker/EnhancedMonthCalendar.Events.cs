﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SkyDean.FareLiz.WinForm.Controls
{
    partial class EnhancedMonthCalendar
    {
        /// <summary>
        /// Occurs when the user selects a date or a date range.
        /// </summary>
        [Category("Action")]
        [Description("Is raised, when the user selected a date or date range.")]
        public event EventHandler<DateRangeEventArgs> DateSelected;

        /// <summary>
        /// Occurs when the user changed the month or year.
        /// </summary>
        [Category("Action")]
        [Description("Is raised, when the user changed the month or year.")]
        public event EventHandler<DateRangeEventArgs> DateChanged;

        /// <summary>
        /// Is Raised when the mouse is over an date.
        /// </summary>
        [Category("Action")]
        [Description("Is raised when the mouse is over an date.")]
        public event EventHandler<ActiveDateChangedEventArgs> ActiveDateChanged;

        /// <summary>
        /// Is raised when the selection extension ended.
        /// </summary>
        [Category("Action")]
        [Description("Is raised when the selection extension ended.")]
        public event EventHandler SelectionExtendEnd;

        /// <summary>
        /// Is raised when a date was clicked.
        /// </summary>
        [Category("Action")]
        [Description("Is raised when a date in selection mode 'Day' was clicked.")]
        public event EventHandler<DateEventArgs> DateClicked;

        /// <summary>
        /// Is raises when a date was selected.
        /// </summary>
        internal event EventHandler<DateEventArgs> InternalDateSelected;

        /// <summary>
        /// Raises the <see cref="DateSelected"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DateRangeEventArgs"/> object that contains the event data.</param>
        private void OnDateSelected(DateRangeEventArgs e)
        {
            if (DateSelected != null)
            {
                DateSelected(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="DateClicked"/> event.
        /// </summary>
        /// <param name="e">A <see cref="DateEventArgs"/> that contains the event data.</param>
        private void OnDateClicked(DateEventArgs e)
        {
            if (DateClicked != null)
            {
                DateClicked(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
        /// </summary>
        /// <param name="e">A <see cref="PaintEventArgs"/> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            // return if in update mode of if nothing to draw
            if (calendarDimensions.IsEmpty || _inUpdate)
                return;

            // set text rendering hint
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

            _renderer.DrawCalendarBackground(e.Graphics, Bounds);

            // loop through all months to draw
            foreach (MonthCalendarMonth month in Months)
            {
                // if month is null or not in the clip rectangle - continue
                if (month == null || !e.ClipRectangle.IntersectsWith(month.Bounds))
                    continue;

                MonthCalendarHeaderState state = GetMonthHeaderState(month.Date);

                // draw the month header
                _renderer.DrawMonthHeader(e.Graphics, month, state);

                // draw the day names header
                _renderer.DrawDayHeader(e.Graphics, month.DayNamesBounds);

                // show week header ?
                if (showWeekHeader)
                {
                    // loop through all week header elements
                    foreach (MonthCalendarWeek week in month.Weeks)
                    {
                        // if week not visible continue
                        if (!week.Visible)
                        {
                            continue;
                        }

                        // draw week header element
                        _renderer.DrawWeekHeaderItem(e.Graphics, week);
                    }
                }

                // loop through all days in current month
                foreach (MonthCalendarDay day in month.Days)
                {
                    // if day is not visible continue
                    if (!day.Visible)
                    {
                        continue;
                    }

                    // draw the day
                    _renderer.DrawDay(e.Graphics, day);
                }

                // draw week header separator line
                _renderer.DrawWeekHeaderSeparator(e.Graphics, month.WeekBounds);
            }

            // if footer is shown
            if (showFooter)
            {
                // draw the footer
                _renderer.DrawFooter(e.Graphics, _footerRect, mouseMoveFlags.Footer);
            }

            // draw the border
            Rectangle r = ClientRectangle;
            r.Width--;
            r.Height--;
            e.Graphics.DrawRectangle(MonthCalendarRenderer.BorderPen, r);
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseDown"/> event.
        /// </summary>
        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            Focus();
            Capture = true;

            // reset the selection range where selection started
            _selectionStartRange = null;
            if (e.Button == MouseButtons.Left)
            {
                MonthCalendarHitTest hit = HitTest(e.Location); // perform hit test                
                currentMoveBounds = hit.Bounds; // set current bounds
                currentHitType = hit.Type;  // set current hit type

                switch (hit.Type)
                {
                    case MonthCalendarHitType.Day:
                        {
                            // save old selection range
                            SelectionRange oldRange = SelectionRange;

                            if (!extendSelection || daySelectionMode != MonthCalendarSelectionMode.Manual)
                            {
                                // clear all selection ranges
                                selectionRanges.Clear();
                            }

                            switch (daySelectionMode)
                            {
                                case MonthCalendarSelectionMode.Day:
                                    {
                                        OnDateClicked(new DateEventArgs(hit.Date));
                                        // only single days are selectable
                                        if (selectionStart != hit.Date)
                                        {
                                            SelectionStart = hit.Date;
                                            RaiseDateSelected();
                                        }

                                        break;
                                    }

                                case MonthCalendarSelectionMode.WorkWeek:
                                    {
                                        // only single work week is selectable
                                        // get first day of week
                                        DateTime firstDay = new MonthCalendarDate(CultureCalendar, hit.Date).GetFirstDayInWeek(_formatProvider).Date;

                                        // get work days
                                        List<DayOfWeek> workDays = DateMethods.GetWorkDays(nonWorkDays);

                                        // reset selection start and end
                                        selectionEnd = DateTime.MinValue;
                                        selectionStart = DateTime.MinValue;

                                        // current range
                                        SelectionRange currentRange = null;

                                        // build selection ranges for work days
                                        for (int i = 0; i < 7; i++)
                                        {
                                            DateTime toAdd = firstDay.AddDays(i);

                                            if (workDays.Contains(toAdd.DayOfWeek))
                                            {
                                                if (currentRange == null)
                                                    currentRange = new SelectionRange(DateTime.MinValue, DateTime.MinValue);

                                                if (currentRange.Start == DateTime.MinValue)
                                                    currentRange.Start = toAdd;
                                                else
                                                    currentRange.End = toAdd;
                                            }
                                            else if (currentRange != null)
                                            {
                                                selectionRanges.Add(currentRange);
                                                currentRange = null;
                                            }
                                        }

                                        if (selectionRanges.Count >= 1)
                                        {
                                            // set first selection range
                                            SelectionRange = selectionRanges[0];
                                            selectionRanges.RemoveAt(0);

                                            // if selection range changed, raise event
                                            if (SelectionRange != oldRange)
                                                RaiseDateSelected();
                                        }
                                        else
                                            Refresh();

                                        break;
                                    }

                                case MonthCalendarSelectionMode.FullWeek:
                                    {
                                        // only a full week is selectable
                                        // get selection start and end
                                        MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, hit.Date).GetFirstDayInWeek(_formatProvider);
                                        selectionStart = dt.Date;
                                        selectionEnd = dt.GetEndDateOfWeek(_formatProvider).Date;

                                        // if range changed, raise event
                                        if (SelectionRange != oldRange)
                                        {
                                            RaiseDateSelected();
                                            Refresh();
                                        }

                                        break;
                                    }

                                case MonthCalendarSelectionMode.Month:
                                    {
                                        // only a full month is selectable
                                        MonthCalendarDate dt = new MonthCalendarDate(CultureCalendar, hit.Date).FirstOfMonth;

                                        // get selection start and end
                                        selectionStart = dt.Date;
                                        selectionEnd = dt.AddMonths(1).AddDays(-1).Date;

                                        // if range changed, raise event
                                        if (SelectionRange != oldRange)
                                        {
                                            RaiseDateSelected();
                                            Refresh();
                                        }
                                        break;
                                    }

                                case MonthCalendarSelectionMode.Manual:
                                    {
                                        if (extendSelection)
                                        {
                                            var range = selectionRanges.Find(r => hit.Date >= r.Start && hit.Date <= r.End);
                                            if (range != null)
                                                selectionRanges.Remove(range);
                                        }

                                        // manual mode - selection ends when user is releasing the left mouse button
                                        selectionStarted = true;
                                        _backupRange = SelectionRange;
                                        selectionEnd = DateTime.MinValue;
                                        SelectionStart = hit.Date;
                                        break;
                                    }
                            }
                            break;
                        }

                    case MonthCalendarHitType.Week:
                        {
                            selectionRanges.Clear();

                            if (maxSelectionCount > 6 || maxSelectionCount == 0)
                            {
                                _backupRange = SelectionRange;
                                selectionStarted = true;
                                selectionEnd = new MonthCalendarDate(CultureCalendar, hit.Date).GetEndDateOfWeek(_formatProvider).Date;
                                SelectionStart = hit.Date;
                                _selectionStartRange = SelectionRange;
                            }
                            break;
                        }

                    case MonthCalendarHitType.MonthName:
                        {
                            _monthSelected = hit.Date;
                            mouseMoveFlags.HeaderDate = hit.Date;

                            Invalidate(hit.InvalidateBounds);
                            Update();

                            monthMenu.Tag = hit.Date;
                            UpdateMonthMenu(CultureCalendar.GetYear(hit.Date));

                            _showingMenu = true;

                            // show month menu
                            monthMenu.Show(this, hit.Bounds.Right, e.Location.Y);
                            break;
                        }

                    case MonthCalendarHitType.MonthYear:
                        {
                            _yearSelected = hit.Date;
                            mouseMoveFlags.HeaderDate = hit.Date;

                            Invalidate(hit.InvalidateBounds);
                            Update();

                            UpdateYearMenu(CultureCalendar.GetYear(hit.Date));

                            yearMenu.Tag = hit.Date;

                            _showingMenu = true;

                            // show year menu
                            yearMenu.Show(this, hit.Bounds.Right, e.Location.Y);

                            break;
                        }

                    case MonthCalendarHitType.Arrow:
                        {
                            // an arrow was pressed
                            // set new start date
                            if (SetStartDate(hit.Date))
                            {
                                // update months
                                UpdateMonths();

                                // raise event
                                RaiseDateChanged();

                                mouseMoveFlags.HeaderDate = _leftArrowRect.Contains(e.Location) ?
                                   Months[0].Date : Months[calendarDimensions.Width - 1].Date;

                                Refresh();
                            }

                            break;
                        }

                    case MonthCalendarHitType.Footer:
                        {
                            // footer was pressed
                            selectionRanges.Clear();

                            bool raiseDateChanged = false;

                            SelectionRange range = SelectionRange;

                            // determine if date changed event has to be raised
                            if (DateTime.Today < Months[0].FirstVisibleDate || DateTime.Today > _lastVisibleDate)
                            {
                                // set new start date
                                if (SetStartDate(DateTime.Today))
                                {
                                    // update months
                                    UpdateMonths();

                                    raiseDateChanged = true;
                                }
                                else
                                    break;
                            }

                            // set new selection start and end values
                            selectionStart = DateTime.Today;
                            selectionEnd = DateTime.Today;

                            SetSelectionRange(daySelectionMode);

                            OnDateClicked(new DateEventArgs(DateTime.Today));

                            // raise events if necessary
                            if (range != SelectionRange)
                                RaiseDateSelected();

                            if (raiseDateChanged)
                                RaiseDateChanged();

                            Refresh();
                            break;
                        }

                    case MonthCalendarHitType.Header:
                        {
                            // header was pressed
                            Invalidate(hit.Bounds);
                            Update();
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseMove"/> event.
        /// </summary>
        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.Location == mouseLocation)
                return;

            mouseLocation = e.Location;

            // backup and reset mouse move flags
            mouseMoveFlags.BackupAndReset();

            // perform hit test
            MonthCalendarHitTest hit = HitTest(e.Location);

            if (e.Button == MouseButtons.Left)
            {
                // if selection started - only in manual selection mode
                if (selectionStarted)
                {
                    // if selection started with hit type Day and mouse is over new date
                    if (hit.Type == MonthCalendarHitType.Day
                       && currentHitType == MonthCalendarHitType.Day
                       && currentMoveBounds != hit.Bounds)
                    {
                        currentMoveBounds = hit.Bounds;

                        // set new selection end
                        SelectionEnd = hit.Date;
                    }
                    else if (hit.Type == MonthCalendarHitType.Week
                       && currentHitType == MonthCalendarHitType.Week)
                    {
                        // set indicator that a week header element is selected
                        mouseMoveFlags.WeekHeader = true;

                        // get new end date
                        DateTime endDate = new MonthCalendarDate(CultureCalendar, hit.Date).AddDays(6).Date;

                        // if new week header element
                        if (currentMoveBounds != hit.Bounds)
                        {
                            currentMoveBounds = hit.Bounds;

                            // check if selection is switched
                            if (selectionStart == _selectionStartRange.End)
                            {
                                // are we after the original end date?
                                if (endDate > selectionStart)
                                {
                                    // set original start date
                                    selectionStart = _selectionStartRange.Start;

                                    // set new end date
                                    SelectionEnd = endDate;
                                }
                                else
                                {
                                    // going backwards - set new "end" date - it's now the start date
                                    SelectionEnd = hit.Date;
                                }
                            }
                            else
                            {
                                // we are after the start date
                                if (endDate > selectionStart)
                                {
                                    // set end date
                                    SelectionEnd = endDate;
                                }
                                else
                                {
                                    // switch start and end
                                    selectionStart = _selectionStartRange.End;
                                    SelectionEnd = hit.Date;
                                }
                            }
                        }
                    }
                }
                else
                {
                    switch (hit.Type)
                    {
                        case MonthCalendarHitType.MonthName:
                            {
                                mouseMoveFlags.MonthName = hit.Date;
                                mouseMoveFlags.HeaderDate = hit.Date;
                                Invalidate(hit.InvalidateBounds);
                                break;
                            }

                        case MonthCalendarHitType.MonthYear:
                            {
                                mouseMoveFlags.Year = hit.Date;
                                mouseMoveFlags.HeaderDate = hit.Date;
                                Invalidate(hit.InvalidateBounds);
                                break;
                            }

                        case MonthCalendarHitType.Header:
                            {
                                mouseMoveFlags.HeaderDate = hit.Date;
                                Invalidate(hit.InvalidateBounds);
                                break;
                            }

                        case MonthCalendarHitType.Arrow:
                            {
                                bool useRTL = UseRTL;

                                if (_leftArrowRect.Contains(e.Location))
                                {
                                    mouseMoveFlags.LeftArrow = !useRTL;
                                    mouseMoveFlags.RightArrow = useRTL;
                                    mouseMoveFlags.HeaderDate = Months[0].Date;
                                }
                                else
                                {
                                    mouseMoveFlags.LeftArrow = useRTL;
                                    mouseMoveFlags.RightArrow = !useRTL;
                                    mouseMoveFlags.HeaderDate = Months[calendarDimensions.Width - 1].Date;
                                }
                                Invalidate(hit.InvalidateBounds);
                                break;
                            }

                        case MonthCalendarHitType.Footer:
                            {
                                mouseMoveFlags.Footer = true;
                                Invalidate(hit.InvalidateBounds);
                                break;
                            }

                        default:
                            {
                                Invalidate();
                                break;
                            }
                    }
                }
            }
            else if (e.Button == MouseButtons.None)
            {
                // no mouse button is pressed
                // set flags and invalidate corresponding region
                switch (hit.Type)
                {
                    case MonthCalendarHitType.Day:
                        {
                            mouseMoveFlags.Day = hit.Date;
                            var bold = GetBoldedDates().Contains(hit.Date) || _boldDatesCollection.Exists(d => d.Value.Date == hit.Date.Date);
                            OnActiveDateChanged(new ActiveDateChangedEventArgs(hit.Date, bold));
                            InvalidateMonth(hit.Date, true);
                            break;
                        }

                    case MonthCalendarHitType.Week:
                        {
                            mouseMoveFlags.WeekHeader = true;

                            break;
                        }

                    case MonthCalendarHitType.MonthName:
                        {
                            mouseMoveFlags.MonthName = hit.Date;
                            mouseMoveFlags.HeaderDate = hit.Date;
                            break;
                        }

                    case MonthCalendarHitType.MonthYear:
                        {
                            mouseMoveFlags.Year = hit.Date;
                            mouseMoveFlags.HeaderDate = hit.Date;
                            break;
                        }

                    case MonthCalendarHitType.Header:
                        {
                            mouseMoveFlags.HeaderDate = hit.Date;
                            break;
                        }

                    case MonthCalendarHitType.Arrow:
                        {
                            bool useRTL = UseRTL;

                            if (_leftArrowRect.Contains(e.Location))
                            {
                                mouseMoveFlags.LeftArrow = !useRTL;
                                mouseMoveFlags.RightArrow = useRTL;

                                mouseMoveFlags.HeaderDate = Months[0].Date;
                            }
                            else if (_rightArrowRect.Contains(e.Location))
                            {
                                mouseMoveFlags.LeftArrow = useRTL;
                                mouseMoveFlags.RightArrow = !useRTL;

                                mouseMoveFlags.HeaderDate = Months[calendarDimensions.Width - 1].Date;
                            }
                            break;
                        }

                    case MonthCalendarHitType.Footer:
                        {
                            mouseMoveFlags.Footer = true;
                            break;
                        }
                }

                // if left arrow RequestState changed
                if (mouseMoveFlags.LeftArrowChanged)
                {
                    Invalidate(UseRTL ? _rightArrowRect : _leftArrowRect);

                    Update();
                }

                // if right arrow RequestState changed
                if (mouseMoveFlags.RightArrowChanged)
                {
                    Invalidate(UseRTL ? _leftArrowRect : _rightArrowRect);

                    Update();
                }

                // if header RequestState changed
                if (mouseMoveFlags.HeaderDateChanged)
                {
                    Invalidate();
                }
                else if (mouseMoveFlags.MonthNameChanged || mouseMoveFlags.YearChanged)
                {
                    // if RequestState of month name or year in header changed
                    SelectionRange range1 = new SelectionRange(mouseMoveFlags.MonthName, mouseMoveFlags.Backup.MonthName);

                    SelectionRange range2 = new SelectionRange(mouseMoveFlags.Year, mouseMoveFlags.Backup.Year);

                    Invalidate(Months[GetIndex(range1.End)].TitleBounds);

                    if (range1.End != range2.End)
                    {
                        Invalidate(Months[GetIndex(range2.End)].TitleBounds);
                    }
                }

                // if day RequestState changed
                if (mouseMoveFlags.DayChanged)
                {
                    // invalidate current day
                    InvalidateMonth(mouseMoveFlags.Day, false);

                    // invalidate last day
                    InvalidateMonth(mouseMoveFlags.Backup.Day, false);
                }

                // if footer RequestState changed
                if (mouseMoveFlags.FooterChanged)
                {
                    Invalidate(_footerRect);
                }
            }

            // if mouse is over a week header, change cursor
            if (mouseMoveFlags.WeekHeaderChanged)
            {
                Cursor = mouseMoveFlags.WeekHeader ? Cursors.UpArrow : Cursors.Default;
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseUp"/> event.
        /// </summary>
        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            // if left mouse button is pressed and selection process was started
            if (e.Button == MouseButtons.Left && selectionStarted)
            {
                selectionRanges.Add(new SelectionRange(SelectionRange.Start, SelectionRange.End));

                // reset selection process
                selectionStarted = false;

                Refresh();

                // raise selected event if necessary
                if (_backupRange.Start != SelectionRange.Start
                   || _backupRange.End != SelectionRange.End)
                {
                    // raise date 
                    RaiseDateSelected();
                }
            }

            // reset current hit type
            currentHitType = MonthCalendarHitType.None;

            Capture = false;
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseLeave"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            // reset some of the mouse move flags
            mouseMoveFlags.LeftArrow = false;
            mouseMoveFlags.RightArrow = false;
            mouseMoveFlags.MonthName = DateTime.MinValue;
            mouseMoveFlags.Year = DateTime.MinValue;
            mouseMoveFlags.Footer = false;
            mouseMoveFlags.Day = DateTime.MinValue;

            if (!_showingMenu)
            {
                mouseMoveFlags.HeaderDate = DateTime.MinValue;
            }

            Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseWheel"/> event.
        /// </summary>
        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);

            if (!_showingMenu)
            {
                Scroll(e.Delta > 0);
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.ParentChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);

            if (Parent != null && Created)
            {
                UpdateMonths();

                Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.RightToLeftChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnRightToLeftChanged(EventArgs e)
        {
            base.OnRightToLeftChanged(e);

            _formatProvider.IsRTLLanguage = UseRTL;

            UpdateMonths();

            Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.EnabledChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            Refresh();
        }

        /// <summary>
        /// Raises the <see cref="DateChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DateRangeEventArgs"/> object that contains the event data.</param>
        void OnDateChanged(DateRangeEventArgs e)
        {
            if (DateChanged != null)
            {
                DateChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="ActiveDateChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ActiveDateChangedEventArgs"/> that contains the event data.</param>
        void OnActiveDateChanged(ActiveDateChangedEventArgs e)
        {
            if (ActiveDateChanged != null)
            {
                ActiveDateChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.FontChanged"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            CalculateSize(false);
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.KeyDown"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.KeyEventArgs"/> that contains the event data.</param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                extendSelection = true;
            }

            base.OnKeyDown(e);
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.KeyUp"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.KeyEventArgs"/> that contains the event data.</param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                extendSelection = false;

                RaiseSelectExtendEnd();
            }

            base.OnKeyUp(e);
        }
    }
}
