﻿using log4net;
using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Utils;
using SkyDean.FareLiz.Service.LiveUpdate;
using SkyDean.FareLiz.WinForm.Data;
using System;

namespace SkyDean.FareLiz.WinForm
{
    internal static class GlobalContext
    {
        internal static MonitorEnvironment MonitorEnvironment { get; private set; }
        internal static ILog Logger { get; private set; }
        internal static string Error { get; private set; }

        private static bool _firstStart;
        internal static bool FirstStart
        {
            get { return _firstStart; }
            set
            {
                if (_isFirstStartSet)
                    throw new ApplicationException("This property cannot be set more than once!");
                _isFirstStartSet = true;
                _firstStart = value;
            }
        }
        private static bool _isFirstStartSet = false;

        static GlobalContext()
        {
            Logger = LogUtil.GetLogger();
        }

        public static void SetEnvironment(MonitorEnvironment env)
        {
            Error = null;
            if (MonitorEnvironment != null)
            {
                Logger.Debug("Closing global environment...");
                MonitorEnvironment.Close();
            }

            Logger.Info("Setting new global environment...");
            AddServices(env);
            MonitorEnvironment = env;

            Logger.Debug("Initializing global environment...");
            var result = MonitorEnvironment.Initialize();
            if (!result.Succeeded)
            {
                Error = result.ErrorMessage;
                Logger.Warn("Error initializing environment: " + Error);
            }
        }

        public static void AddServices(MonitorEnvironment env)
        {
            LiveUpdateService liveUpdateService = null;
            OnlineCurrencyProvider currencyProvider = null;

            var activeServices = env.BackgroundServices.GetAll();
            foreach (var svc in activeServices)
            {
                if (svc.GetType() == typeof(LiveUpdateService))
                    liveUpdateService = svc as LiveUpdateService;
                else if (svc.GetType() == typeof(OnlineCurrencyProvider))
                    currencyProvider = svc as OnlineCurrencyProvider;
            }

            if (liveUpdateService == null)
            {
                Logger.Debug("Adding Live Update Service...");
                liveUpdateService = new LiveUpdateService();
                liveUpdateService.Configuration = liveUpdateService.DefaultConfig;
                env.BackgroundServices.Add(liveUpdateService, false);
            }
            liveUpdateService.ProductLogo = Properties.Resources.FareLiz;

            if (currencyProvider == null)
            {
                Logger.Debug("Adding Currency Provider...");
                env.CurrencyProvider = currencyProvider = new OnlineCurrencyProvider();
                currencyProvider.Configuration = currencyProvider.DefaultConfig;
                env.BackgroundServices.Add(currencyProvider, false);
            }

            Logger.Info("Total loaded services: " + env.BackgroundServices.Count);
        }
    }
}