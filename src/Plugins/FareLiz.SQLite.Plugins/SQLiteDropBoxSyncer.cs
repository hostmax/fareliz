﻿using SkyDean.FareLiz.Plugins.DropBox;
using System.ComponentModel;

namespace SkyDean.FareLiz.Plugins.SQLite
{
    [DisplayName("SQLite DropBox Sync")]
    [Description("Sync SQLite database with DropBox")]
    public class SQLiteDropBoxSyncer : DropBoxDbSync<SQLiteFareDatabase>
    {
        public override void Initialize()
        {
            base.Initialize();
            OnValidateData += SQLiteDropBoxSyncer_OnValidateData;
        }

        void SQLiteDropBoxSyncer_OnValidateData(SQLiteFareDatabase sender, Core.SyncEventArgs<SQLiteFareDatabase> e)
        {
            sender.TryOpenDatabase();
        }
    }
}
