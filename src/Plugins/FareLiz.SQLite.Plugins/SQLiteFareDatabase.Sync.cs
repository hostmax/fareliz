﻿using SkyDean.FareLiz.Core;
using SkyDean.FareLiz.Core.Data;
using System;
using System.Collections.Generic;

namespace SkyDean.FareLiz.Plugins.SQLite
{
    public sealed partial class SQLiteFareDatabase
    {
        public bool Synchronize(SyncOperation operation)
        {
            if (DataSynchronizer == null)
                throw new ApplicationException("There is no available file synchronizer. Please make sure that the proper plugin is installed");

            return DataSynchronizer.Synchronize(operation, DataFileName);
        }

        public int ReceivePackages()
        {
            var pkgSyncer = DataSynchronizer as IPackageSyncer<TravelRoute>;
            int newPackageCount = 0;

            if (pkgSyncer != null)
            {
                var importedPkg = GetImportedPackages();
                var newData = pkgSyncer.Receive(importedPkg);
                if (newData != null && newData.Count > 0)
                {
                    AddData(newData);
                    newPackageCount = newData.Count;
                }
            }

            return newPackageCount;
        }

        public string SendData(IList<TravelRoute> data)
        {
            if (data == null || data.Count < 1)
                return null;

            var newPkg = new DataPackage<TravelRoute>(data);
            var pkgSyncer = DataSynchronizer as IPackageSyncer<TravelRoute>;
            if (pkgSyncer != null)
            {
                pkgSyncer.Send(newPkg);
            }

            return newPkg.Id;
        }
    }
}